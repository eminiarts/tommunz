@servers(['web' => 'tommunz@87.230.26.168'])

@task('deploy')
cd httpdocs/tommunz
git pull
php artisan theme:publish --extensions
@endtask

@task('cache')
cd httpdocs/tommunz
php artisan cache:clear
@endtask

@task('update')
cd httpdocs/tommunz
composer update
@endtask
