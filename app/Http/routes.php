<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/api/data', function () {
    $projects = app('tommunz.projects.project')->homepage();
    $about    = [
        'address'   => app('platform.content')->findBySlug('adresse'),
        'content'   => app('platform.content')->findBySlug('content'),
        'impressum' => app('platform.content')->findBySlug('impressum'),
        'werkliste' => Widget::make('platform/media::media.path', [241]),
    ];
    $team = app('tommunz.team.employee')->findAll();

    $data = array(
        'projects' => $projects,
        'about'    => $about,
        'team'     => $team,
    );

    return $data;
});

Route::get('/', function () {
    return view('pages.index');
});

Route::get('/about', function () {
    return view('pages.index');
});

Route::get('/404', function () {
    return view('pages.index');
});

Route::get('/portfolio/{all}', function () {
    return view('pages.index');
});

Route::get('/viewEmail', function () {
    return view('emails.anmeldung');
});

Route::post('/anmeldung', function () {
    list($messages, $invitation) = app('tommunz.support.invitation')->create(request()->all());

    // Do we have any errors?
    if ($messages->isEmpty()) {
        Alert::success('Vielen Dank für die Anmeldung.');

        return redirect()->to('/danke');
    }

    Alert::error('Leider ist ein Fehler aufgetreten. Versuchen Sie es bitte erneut.');

    return redirect()->back()->withInput();
});

// Route::get('{all}', function () {
//     return view('pages.index');
// });
