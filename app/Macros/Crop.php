<?php

namespace App\Macros;

use Cartalyst\Filesystem\File;
use Platform\Media\Macros\Fit;
use Platform\Media\Models\Media;

class Crop extends Fit
{
    /**
     * @param Media $media
     * @param File $file
     * @return null
     */
    public function up(Media $media, File $file)
    {
        if (!$file->isImage()) {
            return;
        }

        $preset = $this->getPreset();

        $path = $this->getPath($file, $media);

        $this->intervention->make($file->getContents())
             ->crop($preset->width, $preset->height)->save($path)
        ;
    }
}
