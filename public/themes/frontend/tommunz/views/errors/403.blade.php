<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>
    403 Kein Zugriff
  </title>

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000000">
  <meta name="theme-color" content="#000000">

  <link rel="stylesheet" href="fonts/TheinhardtRegular-Regular.css">

  <style>

    article,aside,details,figcaption,figure,footer,header,hgroup,main,nav,section,summary{display:block;}
    audio,canvas,progress,video{display:inline-block;}
    html{font-size:100%;}
    address,blockquote,dd,dl,fieldset,form,hr,menu,ol,p,pre,q,table,ul{margin:0 0 1.25em;}
    h1,h2,h3,h4,h5,h6{line-height:1.25;}
    h1{font-size:64px;margin:0 0 .375em;}
    h2{font-size:1.5em;margin:0 0 .5em;}

    header,section {
      text-align: center;
    }

    body{
      background: #000;
      font-family: "TheinhardtRegular", "Helvetica Neue", Helvetica, Arial, sans-serif;
      color: white;
    }
    body, *{
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }
    section{
      padding: 80px 0;
    }

    a, a:hover, a:focus, a:active, a:visited{
      color: white;
    }

  </style>

</head>

<body>

  <section>

    <div class="error">
      <div class="error__message">

        <h1>403</h1>
        <h2 style="margin-bottom: 30px;">Sie haben keinen Zugriff.</h2>
        <a href="http://tommunz.com/">Zurück zu tommunz.com</a>

      </div>
    </div>

  </section>

</body>
</html>
