<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>
      @section('title')
        @setting('platform.app.title')
      @show
    </title>
    <meta name="description" content="@yield('meta-description')">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="base_url" content="{{ URL::to('/') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{-- Queue assets --}}
    

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">


    {{-- Call custom inline styles --}}
    @section('styles')
    @show

  </head>

  <body class="white-body">

    <!--[if lt IE 7]>
    <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
    <![endif]-->


    <!-- alerts -->
    <div class="alerts">
      @include('partials/alerts')
    </div>

    <!-- Page Header-->
    <div class="page__header">
      @yield('header', '')
    </div>

    <!-- Page Content-->
    <div class="page__content container">
      @yield('page')
    </div>



    {{-- Compiled scripts --}}
    @foreach (Asset::getCompiledScripts() as $script)
      <script src="{{ $script }}"></script>
    @endforeach

    {{-- Call custom inline scripts --}}
    @section('scripts')
    @show

  </body>
</html>
