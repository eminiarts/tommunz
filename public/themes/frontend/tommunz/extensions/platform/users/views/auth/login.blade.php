@extends('layouts/blank')

{{-- Page title --}}
@section('title')
{{{ trans('platform/users::auth/form.login.legend') }}} –
@parent
@stop

{{-- Queue Assets --}}
{{ Asset::queue('platform-validate', 'platform/js/validate.js', 'jquery') }}

{{-- Inline Scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')

<div class="row">

	<div class="col-md-6 col-md-offset-3">

		{{-- Form --}}
		<form id="login-form" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

			{{-- Form: CSRF Token --}}
			<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="login">

          <h3>Anmelden</h3>

					{{-- Email Address --}}
					<div class="mt30 form-group{{ Alert::onForm('email', ' has-error') }}">

						<label class="control-label sr-only" for="email">Email-Adresse</label>

						<input type="email" name="email" id="email" value="{{{ Input::old('email') }}}" placeholder="Email-Adresse"
						required
						autofocus
						data-parsley-trigger="change"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.email_error') }}}">

					</div>

					{{-- Password --}}
					<div class="mb30 form-group{{ Alert::onForm('password', ' has-error') }}">

						<label class="control-label sr-only" for="password">Passwort</label>

						<input type="password" name="password" id="password" placeholder="Passwort"
						required
						data-parsley-trigger="change"
						data-parsley-error-message="{{{ trans('platform/users::auth/form.password_error') }}}">

					</div>


          <p class="pull-right">
            <a href="{{ URL::route('user.password_reminder') }}">Passwort vergessen?</a>
          </p>


					<div class="form-group">

            <label for="remember">

              <input type="checkbox" name="remember" id="remember" value="1"{{ Input::old('remember') ? ' checked="checked"' : null }} />

              {{{ trans('platform/users::auth/form.login.remember-me') }}}

            </label>

          </div>


					{{-- Form actions --}}
					<div class="form-group">

						<button class="btn btn-primary btn-block" type="submit">Anmelden</button>

					</div>

				</div>

		</form> 

		{{-- Social Logins --}}
		@if (count($connections) > 0)
		<div class="panel panel-default">

			<div class="panel-heading">{{{ trans('platform/users::auth/form.login-social.legend') }}}</div>

			<div class="panel-body">

				@foreach ($connections as $slug => $connection)

				<a href="{{ url()->route('user.oauth.auth', $slug) }}" class="btn btn-default btn-block">
					<i class="fa fa-{{ $slug }}"></i> {{ $connection['driver'] }}
				</a>

				@endforeach

			</div>

		</div>
		@endif

	</div>

</div>

@stop
