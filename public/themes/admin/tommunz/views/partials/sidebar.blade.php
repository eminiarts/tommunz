<aside class="sidebar" data-sidebar>

	<div class="sidebar__brand">

		<figure>
			<a href="{{ url()->toAdmin('/') }}" class="header__brand header__brand__name v-link-active">
				tom munz
			</a>
		</figure>

	</div>

	<nav class="navigation navigation--sidebar">

		@nav('admin', 0, 'menu menu--sidebar', admin_uri(), 'partials/navigation/sidebar')

	</nav>

	<div class="sidebar__copyright">

		@setting('platform.app.copyright')

	</div>

</aside>
