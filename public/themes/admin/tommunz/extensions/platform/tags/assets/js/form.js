/**
 * Part of the Platform Tags extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Tags extension
 * @version    4.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

	Extension = Extension || {
		Form: {},
	};

	// Initialize functions
	Extension.Form.init = function()
	{
		Extension.Form
			.listeners()
			.selectize()
		;
	};

	// Add Listeners
	Extension.Form.listeners = function()
	{
		return this;
	};

	// Initialize Selectize
	Extension.Form.selectize = function()
	{
		$('select:not([data-selectize-disabled])').selectize({
			create: true, sortField: 'text',
		});

		return this;
	};

	// Job done, lets run.
	Extension.Form.init();

})(window, document, jQuery);
