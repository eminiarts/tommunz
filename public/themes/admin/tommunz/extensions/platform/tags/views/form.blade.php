@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
 {{{ trans("action.{$mode}") }}} {{{ trans('platform/tags::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css', 'styles') }}

{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('form', 'platform/tags::js/form.js', [ 'platform', 'selectize' ]) }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="tag-form" action="{{ request()->fullUrl() }}" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.tags.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $tag->exists ? $tag->name : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($tag->exists)
							<li>
								<a href="{{ route('admin.tag.delete', $tag->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/tags::common.tabs.general') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

							<legend>{{{ trans('platform/tags::model.general.legend') }}}</legend>

							{{-- Name --}}
							<div class="form-group{{ Alert::onForm('name', ' has-error') }}">

								<label for="name" class="control-label">
									<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/tags::model.general.name_help') }}}"></i>
									{{{ trans('platform/tags::model.general.name') }}}
								</label>

								<input type="text" class="form-control" name="name" id="name" data-slugify="#slug" placeholder="{{{ trans('platform/tags::model.general.name') }}}" value="{{{ request()->old('name', $tag->name) }}}" required autofocus data-parsley-trigger="change">

								<span class="help-block">{{{ Alert::onForm('name') }}}</span>

							</div>

							{{-- Slug --}}
							<div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

								<label for="slug" class="control-label">
									<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/tags::model.general.slug_help') }}}"></i>
									{{{ trans('platform/tags::model.general.slug') }}}
								</label>

								<input type="text" class="form-control" name="slug" id="slug" placeholder="{{{ trans('platform/tags::model.general.slug') }}}" value="{{{ request()->old('slug', $tag->slug) }}}" required data-parsley-trigger="change">

								<span class="help-block">{{{ Alert::onForm('slug') }}}</span>

							</div>

							{{-- Namespace --}}
							<div class="form-group{{ Alert::onForm('namespace', ' has-error') }}">

								<label for="namespace" class="control-label">
									<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/tags::model.general.namespace_help') }}}"></i>
									{{{ trans('platform/tags::model.general.namespace') }}}
								</label>

								<select class="form-control" name="namespace" id="namespace" required data-parsley-trigger="change">
									<option value="">Select a namespace...</option>
									@foreach ($namespaces as $namespace)
									<option {{ request()->old('namespace', $tag->exists ? $tag->namespace : request()->get('namespace')) === $namespace ? ' selected="selected"' : null }} value="{{{ $namespace }}}">{{{ $namespace }}}</option>
									@endforeach
								</select>

								<span class="help-block">{{{ Alert::onForm('namespace') }}}</span>

							</div>

						</fieldset>

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
