Manage your application tags.

---

### When should I use it?

When you need to create or manage tags.

---

### How can I use it?

1. Create or edit a tag.
2. Fill out details.
3. Hit save.
