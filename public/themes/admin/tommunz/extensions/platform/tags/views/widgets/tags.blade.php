<div class="form-group{{ Alert::onForm($inputName, ' has-error') }}">

	<label for="menu" class="control-label">
		<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/tags::model.tag.tags_help') }}}"></i>
		{{{ trans('platform/tags::model.tag.tags') }}}
	</label>

	<input type="hidden" name="{{ $inputName }}[]">

	<select class="form-control" name="{{ $inputName }}[]" id="{{ $inputName }}" data-parsley-trigger="change" multiple>
		@foreach ($availableTags as $tag)
		<option value="{{ $tag->slug }}"{{ in_array($tag->slug, $tags) ? ' selected' : null }}>{{ $tag->name }}</option>
		@endforeach
	</select>

	<span class="help-block">{{{ Alert::onForm($inputName) }}}</span>

</div>
