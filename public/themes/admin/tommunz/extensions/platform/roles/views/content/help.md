Maintain and administer your Platform user roles. Powered by Cartalyst Sentinel, the Roles extension allows you to create roles to which your users may then be assigned.

---

### When should I use it?

When you need to manage roles or role permissions.

---

### How can I use it?

1. Create or edit a role.
2. Fill-out details.
3. Set role permissions.
4. Add any additional attribute information, if desired.
5. Hit Save.
6. 
