@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{{ trans('platform/roles::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('form', 'platform/roles::js/form.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="roles-form" action="{{ request()->fullUrl() }}" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.roles.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $role->exists ? $role->name : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($role->exists)
							<li>
								<a href="{{ route('admin.role.delete', $role->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/roles::common.tabs.general') }}}</a></li>
					<li role="presentation"><a href="#permissions-tab" aria-controls="permissions-tab" role="tab" data-toggle="tab">{{{ trans('platform/roles::common.tabs.permissions') }}}</a></li>
					<li role="presentation"><a href="#attributes-tab" aria-controls="attributes-tab" role="tab" data-toggle="tab">{{{ trans('platform/roles::common.tabs.attributes') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

							<legend>{{{ trans('platform/roles::model.general.legend') }}}</legend>

							{{-- Name --}}
							<div class="form-group{{ Alert::onForm('name', ' has-error') }}">

								<label for="name" class="control-label">
									<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/roles::model.general.name_help') }}}"></i>
									{{{ trans('platform/roles::model.general.name') }}}
								</label>

								<input type="text" class="form-control" name="name" id="name" data-slugify="#slug" placeholder="{{{ trans('platform/roles::model.general.name') }}}" value="{{{ request()->old('name', $role->name) }}}" required data-parsley-trigger="change">

								<span class="help-block">{{{ Alert::onForm('name') }}}</span>

							</div>

							{{-- Slug --}}
							<div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

								<label for="slug" class="control-label">
									<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/roles::model.general.slug_help') }}}"></i>
									{{{ trans('platform/roles::model.general.slug') }}}
								</label>

								<input type="text" class="form-control" name="slug" id="slug" placeholder="{{{ trans('platform/roles::model.general.slug') }}}" value="{{{ request()->old('slug', $role->slug) }}}" required data-parsley-trigger="change">

								<span class="help-block">{{{ Alert::onForm('slug') }}}</span>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Permissions --}}
					<div role="tabpanel" class="tab-pane fade" id="permissions-tab">

						<fieldset>

							<legend>{{{ trans('platform/roles::model.permissions.legend') }}}</legend>

							<div class="row">

								<div class="col-md-12">

									@permissions($permissions)

								</div>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Attributes --}}
					<div role="tabpanel" class="tab-pane fade" id="attributes-tab">

						@attributes($role)

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
