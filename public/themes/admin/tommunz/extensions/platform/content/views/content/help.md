Maintain and publish snippets of `Html`, `Text`, or `Markdown`. This content can be retrieved within any content item itself, pages, and/or any view throughout your application.

---

### Blade Calls

`@content('short-name', 'fallback', 'area:theme')`

This blade call will allow you to return both file and database stored content from within any view utilizing the blade templating engine.

	// Returns content from database
	@content('foo.bar')

	// Returns Html content from a file only if the database entry doesn't exist.
	@content('foo.bar', 'path/to/filename.html')

	// Returns Markdown content from a file only if the database entry doesn't exist.
	@content('foo.bar', 'path/to/filename.md')

---

### When should I use it?

Content is part of your content management system. Use database driven content for snippets which require maintainability through the user interface. Note that database driven content currently can only be returned as text or Html.

Filesystem based content can be created as HTML, MARKDOWN, or TXT files. The extension used `.html, .md, .txt` determines how the content is interpreted.

---

### How can I use it?

1. Create a Content item.
2. Fill out the name and slug inputs.
3. Choose storage type.
  - database: Use the text area to create your content.
  - filesystem: Create your .txt, .md, or .html files in your views content folder.
4. Customize your content item using any available options.

Content items must be located in the following location to be selectable.

`public\themes\frontend\default\views\content`

Here you can create a directory/file structure for all your applications content needs. Even better, content items can be located anywhere throughout your application and do not require you to create a database entry to return them, great for studios who wish to maintain their own content filesystem structures.

That's it, once you have created your content item, simply call and return the content using the `@content()` blade call.
