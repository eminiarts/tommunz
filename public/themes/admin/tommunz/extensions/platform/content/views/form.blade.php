@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{{ trans('platform/content::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css', 'styles') }}
{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}
{{ Asset::queue('form', 'platform/content::js/form.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="content-form" action="{{ request()->fullUrl() }}" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.content.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $content->exists ? $content->name : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($content->exists and $mode != 'copy')
							<li>
								<a href="{{ route('admin.content.delete', $content->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>

							<li>
								<a href="{{ route('admin.content.copy', $content->id) }}" data-toggle="tooltip" data-original-title="{{{ trans('action.copy') }}}">
									<i class="fa fa-copy"></i> <span class="visible-xs-inline">{{{ trans('action.copy') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/content::common.tabs.general') }}}</a></li>
					<li role="presentation"><a href="#tags-tab" aria-controls="attributes" role="tabs-tab" data-toggle="tab">{{{ trans('platform/content::common.tabs.tags') }}}</a></li>
					<li role="presentation"><a href="#attributes-tab" aria-controls="attributes" role="attributes-tab" data-toggle="tab">{{{ trans('platform/content::common.tabs.attributes') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

						<legend>{{{ trans('platform/content::model.general.legend') }}}</legend>

							<div class="row">

								<div class="col-md-3">

									{{-- Name --}}
									<div class="form-group{{ Alert::onForm('name', ' has-error') }}">

										<label for="name" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/content::model.general.name_help') }}}"></i>
											{{{ trans('platform/content::model.general.name') }}}
										</label>

										<input type="text" class="form-control" name="name" id="name" data-slugify="#slug" placeholder="{{{ trans('platform/content::model.general.name') }}}" value="{{{ input()->old('name', $content->name) }}}" required autofocus data-parsley-trigger="change">

										<span class="help-block">{{{ Alert::onForm('name') }}}</span>

									</div>

								</div>

								<div class="col-md-3">

									{{-- Slug --}}
									<div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

										<label for="slug" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/content::model.general.slug_help') }}}"></i>
											{{{ trans('platform/content::model.general.slug') }}}
										</label>

										<input type="text" class="form-control" name="slug" id="slug" placeholder="{{{ trans('platform/content::model.general.slug') }}}" value="{{{ input()->old('slug', $content->slug) }}}" required data-parsley-trigger="change">

										<span class="help-block">{{{ Alert::onForm('slug') }}}</span>

									</div>

								</div>

								<div class="col-md-3">

									{{-- Status --}}
									<div class="form-group{{ Alert::onForm('enabled', ' has-error') }}">

										<label for="enabled" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/content::model.general.enabled_help') }}}"></i>
											{{{ trans('platform/content::model.general.enabled') }}}
										</label>

										<select class="form-control" name="enabled" id="enabled" required data-parsley-trigger="change">
											<option value="1"{{ input()->old('enabled', $content->enabled) == 1 ? ' selected="selected"' : null }}>{{{ trans('common.enabled') }}}</option>
											<option value="0"{{ input()->old('enabled', $content->enabled) == 0 ? ' selected="selected"' : null }}>{{{ trans('common.disabled') }}}</option>
										</select>

										<span class="help-block">{{{ Alert::onForm('enabled') }}}</span>

									</div>

								</div>

								<div class="col-md-3">

									{{-- Type --}}
									<div class="form-group{{ Alert::onForm('type', ' has-error') }}">

										<label for="type" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-placement="left" data-content="{{{ trans('platform/content::model.general.type_help') }}}"></i>
											{{{ trans('platform/content::model.general.type') }}}
										</label>

										<select class="form-control selectize" name="type" id="type" required data-parsley-trigger="change">
											<option value="database"{{ input()->old('type', $content->type) == 'database' ? ' selected="selected"' : null }}>{{{ trans('platform/content::model.general.database') }}}</option>
											<option value="filesystem"{{ input()->old('type', $content->type) == 'filesystem' ? ' selected="selected"' : null }}>{{{ trans('platform/content::model.general.filesystem') }}}</option>
										</select>

										<span class="help-block">{{{ Alert::onForm('type') }}}</span>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-md-12">

									{{-- Type : Filesystem --}}
									<div data-type="filesystem" class="{{ input()->old('type', $content->type) == 'database' ? ' hide' : null }}">

										<div class="form-group{{ Alert::onForm('file', ' has-error') }}">

											<label for="file" class="control-label">
												<i class="fa fa-info-circle" data-toggle="popover" data-placement="left" data-content="{{{ trans('platform/content::model.general.file_help') }}}"></i>
												{{{ trans('platform/content::model.general.file') }}}
											</label>

											<select class="form-control" name="file" id="file"{{ input()->old('type', $content->type) == 'filesystem' ? ' required data-parsley-trigger="change"' : null }}>
												@foreach ($files as $value => $name)
												<option value="{{ $value }}"{{ input()->old('file', $content->file) == $value ? ' selected="selected"' : null}}>{{ $name }}</option>
												@endforeach
											</select>

										</div>

										<span class="help-block">
											{{{ Alert::onForm('file') ?: trans('platform/content::model.general.file_help') }}}
										</span>

										<div class="well" data-content-previewer="{{ route('admin.content.previewer') }}"></div>

									</div>

								</div>

							</div>

							<div class="row">

								<div class="col-lg-12">

									{{-- Type : Database --}}
									<div data-type="database" class="{{ input()->old('type', $content->type) == 'filesystem' ? ' hide' : null }}">

										<div class="form-group{{ Alert::onForm('value', ' has-error') }}">

											<label for="value" class="control-label">
												<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/content::model.general.value_help') }}}"></i>
												{{{ trans('platform/content::model.general.value') }}}
											</label>

											<textarea class="form-control redactor" name="value" id="value"{{ input()->old('type', $content->type) == 'database' ? ' required data-parsley-trigger="change"' : null }}>{{{ input()->old('value', $content->value) }}}</textarea>

											<span class="help-block">
												{{{ Alert::onForm('value') ?: trans('platform/content::model.general.value_help') }}}
											</span>

										</div>

									</div>

								</div>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Tags --}}
					<div role="tabpanel" class="tab-pane fade" id="tags-tab">

						<fieldset>

							<legend>{{{ trans('platform/tags::model.tag.legend') }}}</legend>

							<div class="row">

								<div class="col-md-12">
									@tags($content, 'tags')
								</div>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Attributes --}}
					<div role="tabpanel" class="tab-pane fade" id="attributes-tab">

						@attributes($content)

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
