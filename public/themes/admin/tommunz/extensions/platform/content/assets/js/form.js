/**
 * Part of the Platform Content extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Content extension
 * @version    4.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

	Extension = Extension || {
		Form: {},
	};

	// Initialize functions
	Extension.Form.init = function()
	{
		Extension.Form
			.listeners()
			.selectize()
			.previewer()
		;
	};

	// Add Listeners
	Extension.Form.listeners = function()
	{
		Platform.Cache.$body
			.on('change', '#type', Extension.Form.storage)
			.on('change', '#file', Extension.Form.previewer)
		;

		return this;
	};

	// Initialize Selectize
	Extension.Form.selectize = function()
	{
		$('select:not(#tags)').selectize({
			create: false, sortField: 'text',
		});

		$('#tags').selectize({
			create: true, sortField: 'text',
		});

		return this;
	};

	// Content Previewer
	Extension.Form.previewer = function()
	{
		var element = $('[data-content-previewer]');

		var url = element.data('content-previewer');

		var file = $('#file').val();

		$.ajax({
			url  : url,
			data : { file : file }
		}).done(function(data)
		{
			element.html(data);
		});

		return this;
	};

	// Storage Type
	Extension.Form.storage = function()
	{
		var value = $(this).val();

		$('[data-type]').addClass('hide');

		$('[data-type="' + value + '"]').removeClass('hide');

		$((value == 'filesystem' ? '#file' : '#value')).attr('required', true);

		$((value == 'filesystem' ? '#value' : '#file')).removeAttr('required');
	};

	// Job done, lets run.
	Extension.Form.init();

})(window, document, jQuery);
