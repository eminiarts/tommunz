/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    4.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

var Extension;

;(function(window, document, $, undefined)
{

	'use strict';

	Extension = Extension || {
		Form: {},
	};

	// Initialize functions
	Extension.Form.init = function()
	{
		Extension.Form.selectize();
		Extension.Form.listeners();
	};

	// Add Listeners
	Extension.Form.listeners = function()
	{
		Platform.Cache.$body
			.on('keyup', '#name', Extension.Form.Slug)
			.on('change', '#type', Extension.Form.Storage)
		;
	};

	// Slugify
	Extension.Form.Slug = function()
	{
		$('#slug').val(
			$(this).val().slugify()
		);
	};

	// Storage Type
	Extension.Form.Storage = function()
	{
		$('[data-type]').addClass('hide');

		$('[data-type="' + $(this).val() + '"]').removeClass('hide');

		$((value == 'filesystem' ? '#file' : '#value')).attr('required', true);

		$((value == 'filesystem' ? '#value' : '#file')).removeAttr('required');
	};

	// Initialize Bootstrap Popovers
	Extension.Form.selectize = function ()
	{
		$('select').selectize({
			create: false,
			sortField: 'text'
		});
	};

	// Job done, lets run.
	Extension.Form.init();

})(window, document, jQuery);
