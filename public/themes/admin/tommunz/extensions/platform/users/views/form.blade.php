@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
 {{{ trans("action.{$mode}") }}} {{{ trans('platform/users::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css', 'styles') }}
{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}
{{ Asset::queue('form', 'platform/users::js/form.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="users-form" action="{{ request()->fullUrl() }}" role="form" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.users.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $user->first_name }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($user->exists && $user->id !== $currentUser->id)
							<li>
								<a href="{{ route('admin.user.delete', $user->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/users::common.tabs.general') }}}</a></li>
					<li role="presentation"><a href="#permissions-tab" aria-controls="permissions-tab" role="tab" data-toggle="tab">{{{ trans('platform/users::common.tabs.permissions') }}}</a></li>
					<li role="presentation"><a href="#attributes-tab" aria-controls="attributes-tab" role="tab" data-toggle="tab">{{{ trans('platform/users::common.tabs.attributes') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

							<legend>{{{ trans('platform/users::model.general.legend') }}}</legend>

							<div class="row">

								<div class="col-md-4">

									{{-- First name --}}
									<div class="form-group{{ Alert::onForm('first_name', ' has-error') }}">

										<label for="first_name" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.general.first_name_help') }}}"></i>
											{{{ trans('platform/users::model.general.first_name') }}}
										</label>

										<input type="text" class="form-control" name="first_name" id="first_name" placeholder="{{{ trans('platform/users::model.general.first_name') }}}" value="{{{ request()->old('first_name', $user->first_name) }}}">

										<span class="help-block">{{{ Alert::onForm('first_name') }}}</span>

									</div>

								</div>

								<div class="col-md-4">

									{{-- Last name --}}
									<div class="form-group{{ Alert::onForm('last_name', ' has-error') }}">

										<label for="last_name" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.general.last_name_help') }}}"></i>
											{{{ trans('platform/users::model.general.last_name') }}}
										</label>

										<input type="text" class="form-control" name="last_name" id="last_name" placeholder="{{{ trans('platform/users::model.general.last_name') }}}" value="{{{ request()->old('last_name', $user->last_name) }}}">

										<span class="help-block">{{{ Alert::onForm('last_name') }}}</span>

									</div>

								</div>

								<div class="col-md-4">

									{{-- Email --}}
									<div class="form-group{{ Alert::onForm('email', ' has-error') }}">

										<label for="email" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.general.email_help') }}}" required></i>
											{{{ trans('platform/users::model.general.email') }}}
										</label>

										<input type="email" class="form-control" name="email" id="email" placeholder="{{{ trans('platform/users::model.general.email') }}}" value="{{{ request()->old('email', $user->email) }}}" required data-parsley-trigger="change">

										<span class="help-block">{{{ Alert::onForm('email') }}}</span>

									</div>

								</div>

							</div>

						</fieldset>

						<fieldset>

							<legend>{{{ trans('platform/users::model.authorization.legend') }}}</legend>

							<div class="row">

								<div class="col-md-6">

									{{-- Roles --}}
									<div class="form-group{{ Alert::onForm('roles', ' has-error') }}">

										<label for="roles" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.authorization.roles_help') }}}"></i>
											{{{ trans('platform/users::model.authorization.roles') }}}
										</label>

										<select class="form-control" name="roles[]" id="roles[]" multiple>
											@foreach ($roles as $role)
											<option value="{{ $role->id }}"{{ array_key_exists($role->id, $userRoles) ? ' selected' : null }}>{{ $role->name }}</option>
											@endforeach
										</select>

										<span class="help-block">{{{ Alert::onForm('roles') }}}</span>

									</div>

								</div>

								<div class="col-md-6">

									{{-- Activation status --}}
									<div class="form-group{{ Alert::onForm('activated', ' has-error') }}">

										<label for="activated" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.authorization.activated_help') }}}"></i>
											{{{ trans('platform/users::model.authorization.activated') }}}
										</label>

										<select class="form-control" name="activated" id="activated" required>
											<option value="1"{{ request()->old('activated', $user->activated) == '1' ? ' selected="selected"' : null }}>{{ trans('common.yes') }}</option>
											<option value="0"{{ request()->old('activated', $user->activated) == '0' ? ' selected="selected"' : null }}>{{ trans('common.no') }}</option>
										</select>

										<span class="help-block">{{{ Alert::onForm('activated') }}}</span>

									</div>

								</div>

							</div>

						</fieldset>

						<fieldset>

							<legend>{{{ trans('platform/users::model.authentication.legend') }}}</legend>

							<div class="row">

								<div class="col-md-6">

									{{-- Password --}}
									<div class="form-group{{ Alert::onForm('password', ' has-error') }}">

										<label for="password" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans("platform/users::model.authentication.{$mode}.password_help") }}}"></i>
											{{{ trans("platform/users::model.authentication.{$mode}.password") }}}
										</label>

										<input type="password" class="form-control" name="password" id="password" placeholder="{{{ trans("platform/users::model.authentication.{$mode}.password") }}}"{{ ! $user->exists ? ' required' : null }} data-parsley-trigger="change" data-parsley-minlength="6">

										<span class="help-block">{{{ Alert::onForm('password') }}}</span>

									</div>

								</div>

								<div class="col-md-6">

									{{-- Password Confirmation --}}
									<div class="form-group{{ Alert::onForm('password_confirmation', ' has-error') }}">

										<label for="password_confirmation" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/users::model.authentication.password_confirmation_help') }}}"></i>
											{{{ trans("platform/users::model.authentication.password_confirmation") }}}
										</label>

										<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="{{{ trans("platform/users::model.authentication.password_confirmation") }}}"{{ ! $user->exists ? ' required' : null }} data-parsley-trigger="change" data-parsley-equalto="#password">

										<span class="help-block">{{{ Alert::onForm('password_confirmation') }}}</span>

									</div>

								</div>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Permissions --}}
					<div role="tabpanel" class="tab-pane fade" id="permissions-tab">

						<fieldset>

							<legend>{{{ trans('platform/users::model.permissions.legend') }}}</legend>

							<div class="row">

								<div class="col-md-12">

									@permissions($permissions)

								</div>

							</div>

						</fieldset>

					</div>

					{{-- Tab: Attributes --}}
					<div role="tabpanel" class="tab-pane fade" id="attributes-tab">

						@attributes($user)

					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
