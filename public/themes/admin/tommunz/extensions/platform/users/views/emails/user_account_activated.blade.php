<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<title></title>
	</head>
	<body>
		<p>Hello {{{ $user->first_name }}},</p>

		<p>Your account on "{{{ config('platform.app.title') }}}" has been activated by an administrator, you may login now.</p>

		<p>{{{ config('platform.app.title') }}}</p>
	</body>
</html>
