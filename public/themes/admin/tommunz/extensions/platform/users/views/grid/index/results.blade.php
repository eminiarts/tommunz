<script type="text/template" data-grid="main" data-template="results">

	<% _.each(results, function(r) { %>

		<tr data-grid-row <%= (r.id == {{ $currentUser->id }}) ? ' disabled' : '' %>>
			<td>
				<input data-grid-checkbox <%= (r.id == {{ $currentUser->id }}) ? ' disabled' : '' %> type="checkbox" name="row[]" value="<%= r.id %>">
			</td>
			<td>
				<a href="<%= r.edit_uri %>">
					<% if (r.first_name || r.last_name) { %>
					<%= r.first_name %> <%= r.last_name %>
					<% } else { %>
					N/A
					<% } %>
				</a>
			</td>
			<td><a href="mailto:<%= r.email %>"><%= r.email %></a></td>
			<td>
				<% if (r.activated === true) { %>
					{{ trans('common.yes') }}
				<% } else { %>
					{{ trans('common.no') }}
				<% } %>
			</td>
			<td class="hidden-xs"><%= moment(r.created_at).format('MMM DD, YYYY') %></td>
		</tr>

	<% }); %>

</script>
