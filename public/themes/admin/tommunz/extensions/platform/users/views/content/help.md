Maintain and administrate your platform users. Powered by Cartalyst Sentinel the user extensions provides all the necessary features to maintain user information. Platform offers role based access control, a powerful permission system to maintain your applications authorization & authentication.

---

### When should I use it?

When you need to manage users, their roles or user based permissions.

---

### How can I use it?

1. Create or edit a user.
2. Fill out profile details.
3. Select which roles the user belongs to. The user will inherit any permissions assigned to that role.
4. Set user permissions if required. User permissions will override role permissions.
5. Add any additional profile / attribute information if desired.

You may utilize the `$currentUser` object which is shared throughout your application views and controllers to retrieve the logged in user object.

For user management scenarios, make sure you checkout the [Cartalyst Sentinel Manual](https://cartalyst.com/manual/sentinel) for detailed usage information on `Sentinel`.
