The attributes extension allows you to create and manage attributes for entities across the application.
Features include attribute types (input, radio, select etc.)

---

### Blade Calls

`@attributes('entity', 'attributes', 'view')`

This blade call will allow you to return attributes for the given entity.

	// Returns all the attributes.
	@attributes($page)

	// Returns the meta_title attribute.
	@attributes($page, 'meta_title')

	// Returns only the meta_title and meta_description attributes.
	@attributes($page, [ 'meta_title', 'meta_description' ])

---

### When should I use it?

Attributes is part your content management system. Quickly create new attribute fields for your entities using attributes.

---

### How can I use it?

1. Create an Attribute.
2. Fill out name, slug, namespace, description.
3. Choose a type.
4. Fill out the type options. (if applicable)

That's it, once you have created your attribute, simply navigate to the entity (based on the selected namespace) and hit the attributes tab and you can edit your data on there.
