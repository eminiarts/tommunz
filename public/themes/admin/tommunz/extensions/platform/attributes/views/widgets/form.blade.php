<fieldset>

    <legend>Attributes 
        <a href="{{ route('admin.attribute.create') }}?namespace={{ $namespace }}&amp;previous_url={{ request()->path() }}" role="button" data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
            <i class="fa fa-plus-circle"></i>
        </a>
    </legend>

    @if ($form)

        {!! $form !!}

    @endif

</fieldset>
