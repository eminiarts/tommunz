@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans('platform/dashboard::common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('index', 'platform/dashboard::css/index.scss', 'style') }}

{{-- Inline scripts --}}
@section('scripts')
<!-- Latest Sortable -->
  <script src="http://rubaxa.github.io/Sortable/Sortable.js"></script>

<script>
  // Simple list
Sortable.create(simpleList, {
  animation: 150,
  ghostClass: "sortable-ghost",
  chosenClass: "sortable-chosen",
  // Changed sorting within list
    onUpdate: function (/**Event*/evt) {
        var sortArray = [];

        $(evt.to).children().each(function($i){
          sortArray.push($(this).attr('data-id'));
        });

        console.log(sortArray);

        var posting = $.post( "projects/sort", {projectsArray: sortArray});

        posting.done(function( data ) {
          console.log(data);
        });
    },
});


</script>
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')

@widget('tommunz/projects::project')


@stop
