$(document.body).on('change', 'input[name="seeder"]', function(e)
{
	if ($(this).prop('checked'))
	{
		$('#migrations-form input[name="seeder_count"]').prop('disabled', false);
	}
	else
	{
		$('#migrations-form input[name="seeder_count"]').prop('disabled', true);
	}
});

$('#migrations-form').find('input')


$(document.body).on('click', '.add', function(e)
{
	e.preventDefault();

	var count = $(this).parents('.modal').find('tbody tr').length;

	var row = _.template($('#migrationsTemplate').html());
		row = row({index: count});

	$(this).parents('.modal').find('tbody').append(row);

	$(this).parents('.modal').find('tbody select').selectize();
});

$(document.body).on('click', '.remove', function(e)
{
	e.preventDefault();

	$(this).parents('tr:eq(0)').remove();
});


$(document.body).on('submit', '#migrations-form', function(e)
{
	e.preventDefault();

	var data = $('#migrations-form').serialize();

	$('#migrations-form .btn-success').addClass('disabled');

	var seeder = $(this).find('input[name="seeder"]').prop('checked');

	$('#migrations-form .btn-success').text('Create ');

	$('<i>').attr({
		class: 'fa fa-spinner fa-spin',
	}).appendTo($('#migrations-form .btn-success'));

	$.ajax({
		type: 'post',
		url: url+'/migration',
		data: data,
		success: function(response)
		{
			fetchData();

			$('#migrations-form .btn-success').text('Create').find('i').remove();

			$('#modal-migration').modal('hide');
		}
	});

	return false;
});




$(document.body).on('click', '.btn.seed', function(e)
{
	e.preventDefault();

	$(this).addClass('disabled');
	$(this).prev('i').removeClass('hidden fa-check').addClass('fa-spinner fa-spin');

	var seeder_class = $(this).data('class');

	var self = this;

	$.ajax({
		type: 'post',
		url: url+'/seed',
		data: {
			seeder: seeder_class
		},
		success: function()
		{
			$(self).prev('i').removeClass('fa-spinner fa-spin').addClass('fa-check');

			$(self).removeClass('disabled');
		},
		error: function(response)
		{
			$(self).prev('i').removeClass('fa-spinner fa-spin').addClass('fa-times');
			$(self).removeClass('disabled');
		}
	});
});

$(document.body).on('submit', '#seeder-form', function(e)
{
	e.preventDefault();

	var table_name = $('input[name="table_name"]').val();
	var records = $('input[name="records"]').val();

	$('.save-seeder').text('Creating ').addClass('disabled');

	$('<i>').attr({
		class: 'fa fa-spinner fa-spin',
	}).appendTo('.save-seeder');

	$.ajax({
		type: 'post',
		url: url+'/seeder',
		data: {
			slug: '{{ $extension->getSlug() }}',
			table: table_name,
			records: records
		},
		success: function()
		{
			fetchData();

			$('.save-seeder').text('Create').removeClass('disabled');

			$('#modal-seeder').find('input').val('');

			$('#modal-seeder').modal('hide');
		},
		error: function(response)
		{
			$('#seeder-form').find('.form-group').addClass('has-error');
			$('#seeder-form').find('.help-block').text(response.responseJSON.error.message);
			$('.save-seeder').removeClass('disabled');
			$('.save-seeder').find('i').removeClass('fa-spin fa-spinner').addClass('fa-times');
		}
	});
});


$(document.body).on('submit', '#scaffold-form', function(e)
{
	e.preventDefault();

	var table_name      = $('input[name="table_name"]').val();
	var records         = $('input[name="records"]').val();
	var data            = $('#scaffold-form').serialize();
	var datagrid        = $('#scaffold-form').find('input[name="datagrid"]').prop('checked');
	var form            = $('#scaffold-form').find('input[name="form"]').prop('checked');
	var adminController = $('#scaffold-form').find('input[name="admin_controller"]').prop('checked');
	var seeder          = $('#scaffold-form').find('input[name="seeder"]').prop('checked');
	var name            = $('#scaffold-form').find('input[name="name"]').val();

	$('.save-scaffold').text('Creating ').addClass('disabled');

	$('<i>').attr({
		class: 'fa fa-spinner fa-spin',
	}).appendTo('.save-scaffold');

	$.ajax({
		type: 'post',
		url: url+'/scaffold',
		data: data,
		success: function(response)
		{
			fetchData();

			$('#modal-scaffold').modal('hide');

			if (response.migration)
			{
				$('#modal-migration').modal('show');

				$('#migrations-form').find('input[name="table"]').val(response.table);
				$('#migrations-form').find('input[name="table"]').prop('readonly', true);

				$('<input>').attr({
					type: 'hidden',
					name: 'model',
					value: response.lower_model,
				}).appendTo('#migrations-form');

				$('<input>').attr({
					type: 'hidden',
					name: 'option_admin_controller',
					value: adminController,
				}).appendTo('#migrations-form');

				$('<input>').attr({
					type: 'hidden',
					name: 'option_datagrid',
					value: datagrid,
				}).appendTo('#migrations-form');

				$('<input>').attr({
					type: 'hidden',
					name: 'option_form',
					value: form,
				}).appendTo('#migrations-form');
			}
		}
	});
});



