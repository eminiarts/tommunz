/**
 * Part of the Platform Operations extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Operations extension
 * @version    4.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

 var Extension;

 ;(function(window, document, $, undefined)
 {
 	'use strict';

 	Extension = Extension || {
 		Manage: {},
 	};

 	Extension.Manage.route = window.location.href.substring(0,window.location.href.lastIndexOf("/"));

	// Initialize functions
	Extension.Manage.init = function()
	{
		Extension.Manage
		.listeners()
		.fetchComponents()
		;
	};

	// Add Listeners
	Extension.Manage.listeners = function()
	{
		Platform.Cache.$body
		.on('click', '[data-publish]', Extension.Manage.publishTheme)
		.on('click', '[data-dump]', Extension.Manage.dumpAutoload)
		.on('click', '[data-component="scaffold"] .checkAll', Extension.Manage.checkAll)
		.on('click', '[data-table-action="add"]', Extension.Manage.addColumn)
		.on('click', '[data-table-action="remove"]', Extension.Manage.removeColumn)
		.on('click', '[data-seed]', Extension.Manage.seedMigration)
		.on('show.bs.modal', Extension.Manage.createComponent)
		.on('submit', '#component-form', Extension.Manage.buildComponent)
		.on('change', 'input[name="seeder"]', Extension.Manage.seederStatus)
		;

		return this;
	}

	// Check all components
	Extension.Manage.checkAll = function(event)
	{
		event.stopPropagation();

		$('[data-component="scaffold"]').find('input[type="checkbox"]').not($(this)).prop('checked', $(this).prop('checked'));
	};

	// Add row to migration
	Extension.Manage.addColumn = function(event)
	{
		event.preventDefault();

		var count = $(this).parents('.modal').find('tbody tr').length;

		var row = _.template($('#migrationsTemplate').html());
			row = row({index: count});

		$(this).parents('.modal').find('tbody').append(row);
	};

	// Remove row from migration
	Extension.Manage.removeColumn = function(event)
	{
		event.stopPropagation();

		$(this).parents('tr:eq(0)').remove();
	};

	// Publish Themes
	Extension.Manage.publishTheme = function(event)
	{
		event.stopPropagation();

		var self = this;
		var icon = $(this).find('.fa').attr('class');

		$(this).find('.fa').attr({
			class: 'fa fa-gear fa-spin'
		});

		$.ajax({
			url: Extension.Manage.route+'/publish',
			type: 'post',
			data: {
				slug: $(this).data('slug')
			},
			success: function()
			{
				$(self).find('.fa').removeClass('fa-gear fa-spin').addClass(icon);
			},
			error: function()
			{
				$(self).find('i').removeClass('fa-gear fa-spin').addClass('fa-warning');
			}
		});
	};

	// Check all components
	Extension.Manage.dumpAutoload = function(event)
	{
		event.stopPropagation();

		var self = this;
		var icon = $(this).find('.fa').attr('class');

		$(this).find('.fa').attr({
			class: 'fa fa-gear fa-spin'
		});

		$.ajax({
			url: Extension.Manage.route+'/autoloads',
			type: 'post',
			data: {
				slug: $(this).data('slug')
			},
			success: function()
			{
				$(self).find('.fa').removeClass('fa-gear fa-spin').addClass('fa-truck');
			},
			error: function()
			{
				$(self).find('i').removeClass('fa-gear fa-spin').addClass('fa-warning');
			}
		});
	};

	// Scaffold Component
	Extension.Manage.createComponent = function(event)
	{
		event.stopPropagation();

		var li       = $(event.relatedTarget);
		var size     = li.attr('data-modal-size');
		var template = _.template($(li.attr('data-component-template')).html());
		var modal    = $('#modal-component');

		modal.find('.modal-content').html(template);

		var component = $('#component-form').attr('data-component');

		modal.find('select.regular').selectize();
		modal.find('select.create').selectize({
			create: true
		});

		if (size) {
			modal.find('.modal-dialog').addClass(size);
		} else {
			modal.find('.modal-dialog').removeClass(size);
		}
	};

	// Scaffold Component
	Extension.Manage.buildComponent = function(event)
	{
		event.preventDefault();

		// Form being scaffold.
		var component = $(this).attr('data-component');
		var extension = $(this).attr('data-extension');

		// Common Variables.
		var data = $(this).serialize()

		$(this).find('.scaffold').addClass('hide');
		$(this).find(':submit').addClass('disabled');
		$(this).find('.scaffolding').removeClass('hide');

		$.ajax({
			type: 'post',
			url: Extension.Manage.route+'/'+component,
			data: data,
			success: function(response)
			{
				Extension.Manage.fetchComponents(function()
				{
					if (component == 'scaffold' && response.migration)
					{
						$('#modal-component').on('hidden.bs.modal', function(e)
						{
							$('[data-component-template="#createMigrationTemplate"]').trigger('click');

							$('[data-component="migration"]').find('input[name="table_name"]').val(response.table_name);
							$('[data-component="migration"]').find('input[name="table_name"]').prop('readonly', true);

							$('<input>').attr({
								type: 'hidden',
								name: 'model',
								value: response.lower_model,
							}).appendTo('[data-component="migration"]');

							$('<input>').attr({
								type: 'hidden',
								name: 'scaffold',
								value: true,
							}).appendTo('[data-component="migration"]');

							$('<input>').attr({
								type: 'hidden',
								name: 'option_admin_controller',
								value: response.admin_controller,
							}).appendTo('[data-component="migration"]');

							$('<input>').attr({
								type: 'hidden',
								name: 'option_datagrid',
								value: response.datagrid,
							}).appendTo('[data-component="migration"]');

							$('<input>').attr({
								type: 'hidden',
								name: 'option_form',
								value: response.form,
							}).appendTo('[data-component="migration"]');

							$(this).unbind('hidden.bs.modal');
						});
					}

					$('#modal-component').modal('hide');
				});
			}
		});

		return false;
	};

	// Publish Themes
	Extension.Manage.seedMigration = function(event)
	{
		event.preventDefault();

		$(this).addClass('disabled').prev('i').removeClass('hidden fa-check fa-warning').addClass('fa-spin fa-gear');

		var seeder_class = $(this).data('class');

		var self = this;

		$.ajax({
			type: 'post',
			url: Extension.Manage.route+'/seed',
			data: {
				seeder: seeder_class
			},
			success: function()
			{
				$(self).removeClass('disabled').prev('i').removeClass('fa-gear fa-spin').addClass('fa-check');
			},
			error: function()
			{
				$(self).removeClass('disabled').prev('i').removeClass('fa-gear fa-spin').addClass('fa-warning');
			}
		});
	};

	// Scaffold Component
	Extension.Manage.fetchComponents = function(callback)
	{
		$.ajax({
			url: Extension.Manage.route+'/resources',
			success: function(response)
			{
				var migrations   = _.template($('#listTemplate').html());
					migrations   = migrations({items: response.migrations});
				var seeders      = _.template($('#listSeederTemplate').html());
					seeders      = seeders({items: response.seeders});
				var controllers  = _.template($('#listLabelTemplate').html());
					controllers  = controllers({items: response.controllers});
				var models       = _.template($('#listTemplate').html());
					models       = models({items: response.models});
				var widgets      = _.template($('#listTemplate').html());
					widgets      = widgets({items: response.widgets});
				var datagrids    = _.template($('#listLabelTemplate').html());
					datagrids    = datagrids({items: response.datagrids});
				var repositories = _.template($('#listTemplate').html());
					repositories = repositories({items: response.repositories});

				$('.list-group.migrations').html(migrations);
				$('.list-group.seeders').html(seeders);
				$('.list-group.controllers').html(controllers);
				$('.list-group.models').html(models);
				$('.list-group.widgets').html(widgets);
				$('.list-group.datagrids').html(datagrids);
				$('.list-group.repositories').html(repositories);

				callback ? callback() : null;
			}
		});
	};

	// Enable/disable seeder count field
	Extension.Manage.seederStatus = function(event)
	{
		if ($(this).prop('checked'))
		{
			$('input[name="seeder_count"]').prop('disabled', false);
		}
		else
		{
			$('input[name="seeder_count"]').prop('disabled', true);
		}
	};

	// Job done, lets run.
	Extension.Manage.init();

})(window, document, jQuery);
