/**
 * Part of the Platform Content extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Operations extension
 * @version    4.0.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2016, Cartalyst LLC
 * @link       http://cartalyst.com
 */

var Extension;

;(function(window, document, $, undefined)
{
	'use strict';

	Extension = Extension || {
		Index: {},
	};

	// Initialize functions
	Extension.Index.init = function()
	{
		Extension.Index
			.listeners()
			.selectize()
		;
	};

	// Add Listeners
	Extension.Index.listeners = function()
	{
		Platform.Cache.$body
			.on('focusout', '#vendor, #name', Extension.Index.slugify)
		;

		return this;
	};

	// Slugify
	Extension.Index.slugify = function()
	{
		var slug = $(this).val().slugify();

		$(this).val(slug);
	};

	// Initialize Selectize
	Extension.Index.selectize = function()
	{
		$('#require').selectize({
			create: true,
		});

		return this;
	};

	// Job done, lets run.
	Extension.Index.init();

})(window, document, jQuery);
