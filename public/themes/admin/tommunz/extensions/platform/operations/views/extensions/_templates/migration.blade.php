<script type="text/template" id="migrationsTemplate">

<tr>
	<td><input name="rows[<%= index %>][field]" type="text" class="form-control input-sm" required></td>
	<td>
		<select name="rows[<%= index %>][type]" id="" class="form-control input-sm" required>
			<option value="" disabled selected>Select</option>
			<option value="increments">increments</option>
			<option value="bigIncrements">bigIncrements</option>
			<option value="char">char</option>
			<option value="string">string</option>
			<option value="text">text</option>
			<option value="mediumText">mediumText</option>
			<option value="longText">longText</option>
			<option value="integer">integer</option>
			<option value="bigInteger">bigInteger</option>
			<option value="mediumInteger">mediumInteger</option>
			<option value="tinyInteger">tinyInteger</option>
			<option value="smallInteger">smallInteger</option>
			<option value="unsignedInteger">unsignedInteger</option>
			<option value="unsignedBigInteger">unsignedBigInteger</option>
			<option value="float">float</option>
			<option value="double">double</option>
			<option value="decimal">decimal</option>
			<option value="boolean">boolean</option>
			<option value="date">date</option>
			<option value="dateTime">dateTime</option>
			<option value="time">time</option>
			<option value="timestamp">timestamp</option>
		</select>
	</td>
	<td><input name="rows[<%= index %>][default]" type="type" class="form-control input-sm"></td>
	<td><label class="btn btn-sm btn-default"><input name="rows[<%= index %>][nullable]" type="checkbox" value="nullable"></label></td>
	<td><label class="btn btn-sm btn-default"><input name="rows[<%= index %>][unsigned]" type="checkbox" value="unsigned"></label></td>
	<td><label class="btn btn-sm btn-default"><input name="rows[<%= index %>][unique]" type="checkbox" value="unique"></label></td>
	<td><button class="btn btn-default" type="button" data-table-action="remove"><i class="fa fa-trash-o"></i></button></td>
</tr>

</script>
