<script type="text/template" id="createMigrationTemplate">
	<form id="component-form" data-component="migration" data-extension="{{ $extension->getSlug() }}" action="{{ request()->fullUrl() }}" method="post" accept-char="UTF-8" autocomplete="off" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.migration.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.migration.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-8">

					<div class="form-group">

						<label for="name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.migration.table_name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.migration.table_name') }}}
						</label>

						<input type="text" name="table_name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.migration.table_name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

				<div class="col-md-4">

					<div class="form-group">

						<label for="seeder_count" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.migration.seeder_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.migration.seeder') }}}
						</label>

						<div class="input-group">
							<span class="input-group-addon">
								<input type="checkbox" name="seeder">
							</span>

							<input type="text" name="seeder_count" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.migration.seeder_count_placeholder') }}}" disabled>

						</div>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="table-schema col-md-12">

					<table class="table table-bordered text-center">
						<thead>
							<tr>
								<th class="col-md-3">Field</th>
								<th class="col-md-3">Type</th>
								<th class="col-md-2">Default</th>
								<th class="col-md-1">Nullable</th>
								<th class="col-md-1">Unsigned</th>
								<th class="col-md-1">Unique</th>
								<th class="col-md-1 text-center">Action</th>
							</tr>
						</thead>
						<tbody>

							<tr>
								<td><input name="rows[0][field]" type="text" class="form-control input-sm" required></td>
								<td>
									<select name="rows[0][type]" id="" class="form-control input-sm" required>
										<option value="" disabled selected>Select</option>
										<option value="increments">increments</option>
										<option value="bigIncrements">bigIncrements</option>
										<option value="char">char</option>
										<option value="string">string</option>
										<option value="text">text</option>
										<option value="mediumText">mediumText</option>
										<option value="longText">longText</option>
										<option value="integer">integer</option>
										<option value="bigInteger">bigInteger</option>
										<option value="mediumInteger">mediumInteger</option>
										<option value="tinyInteger">tinyInteger</option>
										<option value="smallInteger">smallInteger</option>
										<option value="float">float</option>
										<option value="double">double</option>
										<option value="decimal">decimal</option>
										<option value="boolean">boolean</option>
										<option value="date">date</option>
										<option value="dateTime">dateTime</option>
										<option value="time">time</option>
										<option value="timestamp">timestamp</option>
									</select>
								</td>

								<td>
									<input name="rows[0][default]" type="type" class="form-control input-sm">
								</td>

								<td>
									<label class="btn btn-sm btn-default">
										<input name="rows[0][nullable]" type="checkbox" value="nullable">
									</label>
								</td>

								<td>
									<label class="btn btn-sm btn-default">
										<input name="rows[0][unsigned]" type="checkbox" value="unsigned">
									</label>
								</td>

								<td>
									<label class="btn btn-sm btn-default">
										<input name="rows[0][unique]" type="checkbox" value="unique">
									</label>
								</td>

								<td>
									<button class="btn btn-default" type="button" data-table-action="remove"><i class="fa fa-trash-o"></i></button>
								</td>
							</tr>

						</tbody>

					</table>

				</div>

			</div>

			<div class="row">

				<div class="col-md-12 text-right">

					<div class="btn-group">

						<label for="timestamps" class="btn btn-default" data-toggle="popover" data-placement="top" data-content="{{{ trans('platform/operations::extensions/model.migration.timestamps_help') }}}">
							<small><i class="fa fa-clock-o"></i> {{{ trans('platform/operations::extensions/model.migration.timestamps') }}}</small>
						</label>

						<label class="btn btn-default">
							<input id="timestamps" type="checkbox" name="timestamps" checked>
						</label>

					</div>

					<div class="btn-group">

						<label for="increments" class="btn btn-default" data-toggle="popover" data-placement="top" data-content="{{{ trans('platform/operations::extensions/model.migration.auto_increment_help') }}}">
							<small><i class="fa fa-sort-numeric-asc"></i> {{{ trans('platform/operations::extensions/model.migration.auto_increment') }}}</small>
						</label>

						<label class="btn btn-default">
							<input id="increments" type="checkbox" name="increments" checked>
						</label>

					</div>

					<button class="btn btn-default" type="button" data-table-action="add">Add Field</button>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>
