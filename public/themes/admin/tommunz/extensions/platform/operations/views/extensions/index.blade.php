@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
 {{{ trans('platform/operations::extensions/common.title') }}}
@stop

{{ Asset::queue('moment', 'moment/js/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'underscore/js/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'platform/operations::js/extensions/index.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')

{{-- Grid --}}
<section class="panel panel-default panel-grid">

	{{-- Grid: Header --}}
	<header class="panel-heading">

		<nav class="navbar navbar-default navbar-actions">

			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<span class="navbar-brand">{{{ trans('platform/operations::extensions/common.title') }}}</span>

				</div>

				{{-- Grid: Actions --}}
				<div class="collapse navbar-collapse" id="actions">

					<ul class="nav navbar-nav navbar-left">

						<li class="primary">
							<a href="{{ route('admin.operations.workshop') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
								<i class="fa fa-plus"></i>  <span class="visible-xs-inline">{{{ trans('action.create') }}}</span>
							</a>
						</li>

					</ul>

					{{-- Grid: Filters --}}
					<form class="navbar-form navbar-right" method="post" accept-charset="utf-8" data-search data-grid="main" role="form">

						<div class="input-group">

							<span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
									{{{ trans('common.filters') }}}
								</button>

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>

								<ul class="dropdown-menu" role="menu">

									<li>
										<a data-grid="main" data-filter="enabled:1" data-label="enabled::{{{ trans('common.all_enabled') }}}" data-reset>
											<i class="fa fa-eye"></i> {{{ trans('common.show_enabled') }}}
										</a>
									</li>

									<li>
										<a data-toggle="tooltip" data-placement="top" data-original-title="" data-grid="main" data-filter="enabled:0" data-label="enabled::{{{ trans('common.all_disabled') }}}" data-reset>
											<i class="fa fa-eye-slash"></i> {{{ trans('common.show_disabled') }}}
										</a>
									</li>

									<li class="divider"></li>

									<li>
										<a data-grid="main" data-filter="installed:1" data-label="installed::{{{ trans('platform/operations::extensions/common.all_installed') }}}" data-reset>
											<i class="fa fa-eye"></i> {{{ trans('platform/operations::extensions/common.show_installed') }}}
										</a>
									</li>

									<li>
										<a data-toggle="tooltip" data-placement="top" data-original-title="" data-grid="main" data-filter="installed:0" data-label="installed::{{{ trans('platform/operations::extensions/common.all_uninstalled') }}}" data-reset>
											<i class="fa fa-eye-slash"></i> {{{ trans('platform/operations::extensions/common.show_uninstalled') }}}
										</a>
									</li>

								</ul>

							</span>

							<input class="form-control" name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit">
									<span class="fa fa-search"></span>
								</button>

								<button class="btn btn-default" data-grid="main" data-reset>
									<i class="fa fa-refresh fa-sm"></i>
								</button>

							</span>

						</div>

					</form>

				</div>

			</div>

		</nav>

	</header>

	<div class="panel-body">

		{{-- Grid: Applied Filters --}}
		<div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

			<div id="data-grid_applied" class="btn-group" data-grid="main"></div>

		</div>

	</div>

	{{-- Grid: Table --}}
	<div class="table-responsive">

		<table id="data-grid" class="table table-hover" data-source="{{ route('admin.operations.extensions.grid') }}" data-grid="main">
			<thead>
				<tr>
					<th data-sort="vendor" class="col-md-2 sortable">{{{ trans('platform/operations::extensions/model.general.vendor') }}}</th>
					<th data-sort="name" class="col-md-3 sortable">{{{ trans('platform/operations::extensions/model.general.name') }}}</th>
					<th data-sort="slug" class="col-md-4 sortable">{{{ trans('platform/operations::extensions/model.general.author') }}}</th>
					<th data-sort="enabled" class="col-md-1 sortable">{{{ trans('platform/operations::extensions/model.general.enabled') }}}</th>
					<th data-sort="installed" class="col-md-1 sortable">{{{ trans('platform/operations::extensions/model.general.installed') }}}</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>

	</div>

	<footer class="panel-footer clearfix">

		{{-- Grid: Pagination --}}
		<div id="data-grid_pagination" data-grid="main"></div>

	</footer>

	{{-- Grid: templates --}}
	@include('platform/operations::extensions/grid/index/results')
	@include('platform/operations::extensions/grid/index/pagination')
	@include('platform/operations::extensions/grid/index/filters')
	@include('platform/operations::extensions/grid/index/no_results')

</section>

@if (config('platform.app.help'))
	@include('platform/operations::extensions/help')
@endif

@stop
