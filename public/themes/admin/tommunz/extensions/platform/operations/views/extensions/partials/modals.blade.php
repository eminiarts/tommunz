<div class="modal fade" id="modal-component" tabindex="-1" role="dialog" aria-labelledby="model-component" aria-hidden="true">

	<div class="modal-dialog">

		<div class="modal-content">

		</div>

	</div>

</div>

<div class="modal fade" id="modal-autoloads" tabindex="-1" role="dialog" aria-hidden="true">

	<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Dumping Autoloads Error</h4>
			</div>

			<div class="modal-body">

				<div class="row">

					<div class="col-md-12">
						<strong>Output</strong>

						<div id="autoloads-output">
							<p></p>
						</div>
					</div>

				</div>

			</div>

		</div>

	</div>

</div>
