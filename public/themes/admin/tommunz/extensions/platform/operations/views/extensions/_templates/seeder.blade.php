<script type="text/template" id="seederTemplate">
	<form id="component-form" data-component="seeder" data-extension="{{ $extension->getSlug() }}" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.seeder.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.seeder.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-7">

					<div class="form-group">

						<label for="table_name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.seeder.table_name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.seeder.table_name') }}}
						</label>

						<input type="text" name="table_name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.seeder.table_name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

				<div class="col-md-5">

					<div class="form-group">

						<label for="records" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.seeder.records_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.seeder.records') }}}
						</label>

						<input type="text" name="records" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.seeder.records_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

			</div>


			<div class="row">

				<div class="col-md-12">

					<p class="lead text-center">{{{ trans('platform/operations::extensions/model.seeder.instruction') }}}</p>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>

