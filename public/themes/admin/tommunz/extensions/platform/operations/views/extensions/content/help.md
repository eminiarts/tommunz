The operations extension allows you to create and manage extensions for your application.
Features include installing/uninstalling, enabling/disabling or scaffolding new resources.

---

### When should I use it?

When you need to manage an extension. Management includes enabling/disabling, installing/uninstalling, scaffolding new resources or adding a single resource such as a model, widget, seeder or migration.

---

### How can I use it?

1. Edit an extension.
2. Hit on Scaffold.
3. Fill out the details.
4. Hit next.
	- Fill out the migration details (if applicable)
	- Hit create.

That's it, now you can start using the newly generated resources.
