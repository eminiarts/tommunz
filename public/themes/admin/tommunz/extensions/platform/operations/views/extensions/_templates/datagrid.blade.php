<script type="text/template" id="datagridTemplate">
	<form id="component-form" data-component="datagrid" data-extension="{{ $extension->getSlug() }}" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.datagrid.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.datagrid.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-8">

					<div class="form-group">

						<label for="name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.datagrid.name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.datagrid.name') }}}
						</label>

						<input type="text" name="name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.datagrid.name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

				<div class="col-md-4">

					<div class="form-group">

						<label for="theme" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.datagrid.theme_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.datagrid.theme') }}}
						</label>

						<select name="theme" class="form-control regular" required>
							<option>{{{ trans('platform/operations::extensions/model.datagrid.theme') }}}</option>
							@foreach($themeLocations as $theme)
								<option value="{{ $theme }}">{{ $theme }}</option>
							@endforeach
						</select>
						<span class="help-block"></span>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-md-12">

					<div class="form-group">

						<label for="view" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.datagrid.view') }}}"></i>
							{{{ trans('platform/operations::extensions/model.datagrid.view') }}}
						</label>

						<input type="text" name="view" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.datagrid.view') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>


			<div class="row">

				<div class="col-md-12">

					<p class="lead text-center">{{{ trans('platform/operations::extensions/model.datagrid.instruction') }}}</p>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>
