<script type="text/template" id="listTemplate">

<% if (items.length == 0) { %>

	<li class="list-group-item list-group-item-warning">
		{{{ trans('platform/operations::extensions/message.components_empty') }}}
	</li>

<% } %>

<% _.each(items, function(item) { %>

	<li class="list-group-item">
		<%= item.class %>
	</li>

<% }); %>

</script>

<script type="text/template" id="listLabelTemplate">

<% if (items.length == 0) { %>

	<li class="list-group-item list-group-item-warning">
		{{{ trans('platform/operations::extensions/message.components_empty') }}}
	</li>

<% } %>

<% _.each(items, function(item) { %>

	<li class="list-group-item">
		<span class="badge"><%= item.location %></span> <%= item.class %>
	</li>

<% }); %>

</script>

<script type="text/template" id="listSeederTemplate">

<% if (items.length == 0) { %>

	<li class="list-group-item list-group-item-warning">
		{{{ trans('platform/operations::extensions/message.components_empty') }}}
	</li>

<% } %>

<% _.each(items, function(item) { %>

	<li class="list-group-item">
			<%= item.class %> <i class="hidden fa fa-gear fa-spin"></i> <button type="button" data-class="<%= item.namespace + item.class %>" class="pull-right btn btn-default btn-xs" data-seed>run</button>
	</li>

<% }); %>

</script>
