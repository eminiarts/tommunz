<script type="text/template" data-grid="main" data-template="results">

	<% _.each(results, function(r) { %>

		<tr>

			<td><%= r.vendor %></td>
			<td>
				<a href="<%= r.edit_uri %>"><%= r.name %> <small>v<%= r.version %></small></a>
			</td>
			<td><%= r.author %></td>


			<% if (r.installed) { %>

				<% if (r.enabled) { %>
					<td><span class="label label-success">{{{ trans('common.enabled') }}}</span></td>
				<% } else{ %>
					<td><span class="label label-warning">{{{ trans('common.disabled') }}}</span></td>
				<% } %>

				<td><span class="label label-success">{{{ trans('common.installed') }}}</span></td>

			<% } else { %>
				<td><span class="label label-danger">{{{ trans('common.uninstalled') }}}</span></td>
				<td></td>
			<% } %>

		</tr>

	<% }); %>

</script>
