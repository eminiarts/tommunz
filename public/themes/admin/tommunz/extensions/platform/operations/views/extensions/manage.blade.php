@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans('platform/operations::extensions/common.manage') }}}
@stop

{{ Asset::queue('extensions', 'platform/operations::css/extensions/manage.scss') }}
{{ Asset::queue('selectize-style', 'selectize/css/selectize.bootstrap3.css', 'style') }}

{{ Asset::queue('underscore', 'underscore/js/underscore.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'jquery') }}

{{ Asset::queue('manage', 'platform/operations::js/extensions/manage.js', 'platform') }}

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

	<header class="panel-heading">

		<nav class="navbar navbar-default navbar-actions">

			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.operations.extensions.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
						<i class="fa fa-reply"></i>  <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
					</a>

					<span class="navbar-brand">

						{{{ $extension->name ?: $extension->getSlug() }}}

						@if ($extension->isInstalled())
						<i class="fa fa-smile-o"></i>
						<span class="text-success">{{{ trans('common.installed') }}}</span>
						@else
						<i class="fa fa-frown-o"></i>
						<span class="text-warning">{{{ trans('common.uninstalled') }}}</span>
						@endif

						&amp;

						@if ($extension->isEnabled())
						<span class="text-success">{{{ trans('common.enabled') }}}</span>
						@else
						<i class="fa fa-flag-o"></i>
						<span class="text-warning">{{{ trans('common.disabled') }}}</span>
						@endif



					</span>
				</div>

				{{-- Form: Actions --}}
				<div class="collapse navbar-collapse" id="actions">

					<ul class="nav navbar-nav navbar-right">

						@if ( ! $errors->any())
						<li>
							<a data-slug="{{ $extension->getSlug() }}" data-dump data-toggle="tooltip" data-original-title="{{{ trans('platform/operations::extensions/action.dump_autoload') }}}">
								<i class="fa fa-truck"></i>	{{{ trans('platform/operations::extensions/action.dump_autoload') }}}
							</a>
						</li>

						<li>
							<a data-slug="{{ $extension->getSlug() }}" data-publish data-toggle="tooltip" data-original-title="{{{ trans('platform/operations::extensions/action.publish_theme') }}}">
								<i class="fa fa-cloud-upload"></i> {{{ trans('platform/operations::extensions/action.publish_theme') }}}
							</a>
						</li>

						@else
						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('platform/operations::extensions/action.dump_autoload') }}}">
								<i class="fa fa-truck"></i> {{{ trans('platform/operations::extensions/action.dump_autoload') }}}
							</a>
						</li>

						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('platform/operations::extensions/action.publish_theme') }}}">
								<i class="fa fa-cloud-upload"></i> {{{ trans('platform/operations::extensions/action.publish_theme') }}}
							</a>
						</li>
						@endif

						{{-- Check if the extension is installed --}}
						@if ($extension->isInstalled())

						{{-- Check if the extension is enabled --}}
						@if ($extension->isEnabled())

						{{-- Allow an extension to be disabled --}}
						@if ($extension->canDisable() && ! $errors->any())
						<li>
							<a href="{{ URL::toAdmin("operations/extensions/{$extension->getSlug()}/disable") }}" data-toggle="tooltip" data-original-title="{{{ trans('action.disable') }}}">
								<i class="fa fa-toggle-on"></i> Disable
							</a>
						</li>
						@else
						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('action.disable') }}}">
								<i class="fa fa-toggle-on"></i> Disable
							</a>
						</li>
						@endif

						{{-- Check if the extension can be enabled --}}
						@else
						@if ($extension->canEnable() && ! $errors->any())
						<li>
							<a href="{{ URL::toAdmin("operations/extensions/{$extension->getSlug()}/enable") }}" data-toggle="tooltip" data-original-title="{{{ trans('action.enable') }}}">
								<i class="fa fa-toggle-off"></i> Enable
							</a>
						</li>
						@else
						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('action.enable') }}}">
								<i class="fa fa-toggle-off"></i> Enable
							</a>
						</li>
						@endif
						@endif

						{{-- Check if the extension can be uninstalled --}}
						@if ($extension->canUninstall() && ! $errors->any())
						<li>
							<a href="{{ URL::toAdmin("operations/extensions/{$extension->getSlug()}/uninstall") }}" data-toggle="tooltip" data-original-title="{{{ trans('action.uninstall') }}}">
								<i class="fa fa-power-off"></i> Uninstall
							</a>
						</li>
						@else
						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('action.uninstall') }}}">
								<i class="fa fa-power-off"></i> Uninstall
							</a>
						</li>
						@endif

						@else

						{{-- Allow an extension to be installed --}}
						@if ($extension->canInstall() && ! $errors->any())
						<li>
							<a href="{{ URL::toAdmin("operations/extensions/{$extension->getSlug()}/install") }}" data-toggle="tooltip" data-original-title="{{{ trans('action.install') }}}">
								<i class="fa fa-power-off"></i> Install
							</a>
						</li>
						@else
						<li class="disabled">
							<a data-toggle="tooltip" data-original-title="{{{ trans('action.install') }}}">
								<i class="fa fa-power-off"></i> Install
							</a>
						</li>
						@endif

						@endif

					</ul>

				</div>

			</div>

		</nav>

	</header>

	<div class="panel-body">

		<div role="tabpanel">

			{{-- Manage: Tabs --}}
			<ul class="nav nav-tabs" role="tablist">
				<li class="active" role="presentation"><a href="#tab-general" aria-controls="tab-general" role="tab" data-toggle="tab">{{{ $extension->name ?: $extension->getSlug() }}} {{{ trans('platform/operations::extensions/common.tabs.general') }}}</a></li>
			</ul>

			<div class="tab-content">

				{{-- Tab: General --}}
				<div role="tabpanel" class="tab-pane fade in active" id="tab-general">

                    @if (count($unresolved) >= 1)
                    <div class="row">

                        <div class="col-sm-12">

                            <div class="alert alert-danger">
                                The following extensions couldn't be resolved:
                                <ul>
                                    @foreach ($unresolved as $non)
                                    <li>{{ $non }}</li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>

                    </div>
                    @endif

					<div class="row">

						<div class="extension-details col-sm-6">

							<div class="extension-type pull-left">
								<i class="fa fa-cube"></i>
								<span>v{{{ $extension->version }}}</span>
							</div>

							<h2>

								{{{ $extension->name ?: $extension->getSlug() }}} <span>by<br>
								{{{ $extension->getVendor() }}}</span><br>
								<small>Authored by {{{ $extension->author }}}</small>

							</h2>

							<p class="lead">
								{{{ $extension->description }}}
							</p>

						</div>

						<div class="col-md-6">

							@if ($canScaffold && ! $isCore)

							<div class="scaffold-components text-right">

								<ul class="scaffold-components-list">

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#scaffoldTemplate">
										<i class="fa fa-cubes"></i>
										<span>{{{ trans('platform/operations::extensions/common.scaffold') }}} All</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#controllerTemplate">
										<i class="fa fa-taxi"></i><span>Controller</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#repositoryTemplate">
										<i class="fa fa-road"></i><span>Repository</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#widgetTemplate">
										<i class="fa fa-puzzle-piece"></i><span>Widget</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="modal-lg" data-component-template="#createMigrationTemplate">
										<i class="fa fa-database"></i><span>Migration</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#seederTemplate">
										<i class="fa fa-leaf"></i><span>Seed</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#modelTemplate">
										<i class="fa fa-truck"></i><span>Model</span>
									</li>

									<li data-toggle="modal" data-target="#modal-component" data-modal-size="" data-component-template="#datagridTemplate">
										<i class="fa fa-list"></i><span>DataGrid</span>
									</li>

								</ul>

							</div>

							@elseif ($isCore)

							<div class="scaffold-components callout">

							<h4>{{ trans('platform/operations::extensions/message.core_extension',  array('extension' => $extension->name ?: $extension->getSlug())) }}</h4>

							<p>{{ trans('platform/operations::extensions/message.core_extension_warning',  array('extension' => $extension->name ?: $extension->getSlug())) }}</p>

							</div>

							@else

							<div class="scaffold-components callout">

							<h4>{{ trans('platform/operations::extensions/message.writable',  array('extension' => $extension->name ?: $extension->getSlug())) }}</h4>

							<p>{{ trans('platform/operations::extensions/message.writable_warning',  array('extension' => $extension->name ?: $extension->getSlug())) }}</p>

							</div>

							@endif

						</div>

					</div>

					<hr>

					<div class="row">

						<div class="col-md-6">

						@include('platform/operations::extensions.partials.components')

						</div>

						<div class="col-md-6">

							{{-- Extension: Dependencies --}}

							<h3>{{{count($dependencies)}}} {{{ trans('platform/operations::extensions/common.dependencies') }}}</h3>

							<p class="lead">{{{ trans('platform/operations::extensions/common.dependencies_help',  array('extension' => $extension->name ?: $extension->getSlug())) }}}</p>

							<hr>

							<ul class="list-group">

								@if (count($dependencies) == 0)

								<li class="list-group-item list-group-item-warning">{{{ trans('platform/operations::extensions/message.dependencies_empty') }}}</li>

								@else

								@foreach ($dependencies as $_extension)

								<li class="list-group-item">
									@if (in_array($_extension->getSlug(), $protectedExtensions))
									{{{ $_extension->name }}} ({{{ $_extension->getSlug() }}})
									@else
									<a href="{{ route('admin.operations.extensions.manage', $_extension->getSlug()) }}">{{{ $_extension->name }}} ({{{ $_extension->getSlug() }}})</a>
									@endif

									<div class="pull-right">

										@if ($_extension->isInstalled())
										<span class="label label-success">{{{ trans('common.installed') }}}</span>

										@if ($_extension->isEnabled())
										<span class="label label-success">{{{ trans('common.enabled') }}}</span>
										@else
										<span class="label label-warning">{{{ trans('common.disabled') }}}</span>
										@endif
										@else
										<span class="label label-danger">{{{ trans('common.uninstalled') }}}</span>
										@endif

									</div>

								</li>

								@endforeach

								@endif

							</ul>

							<hr>

							{{-- Extension: Dependents --}}

							<h3>{{{count($dependents)}}} {{{ trans('platform/operations::extensions/common.dependents') }}}</h3>

							<p class="lead">{{{ trans('platform/operations::extensions/common.dependents_help',  array('extension' => $extension->name ?: $extension->getSlug())) }}}</p>

							<hr>

							<ul class="list-group">

								@if (count($dependents) == 0)

								<li class="list-group-item list-group-item-warning">
									{{{ trans('platform/operations::extensions/message.dependents_empty') }}}
								</li>

								@else

								@foreach ($dependents as $_extension)

								<li class="list-group-item">
									@if (in_array($_extension->getSlug(), $protectedExtensions))
									{{{ $_extension->name }}} ({{{ $_extension->getSlug() }}})
									@else
									<a href="{{ route('admin.operations.extensions.manage', $_extension->getSlug()) }}">{{{ $_extension->name }}} ({{{ $_extension->getSlug() }}})</a>
									@endif

									<div class="pull-right">

										@if ($_extension->isInstalled())
										<span class="label label-success">{{{ trans('common.installed') }}}</span>

										@if ($_extension->isEnabled())
										<span class="label label-success">{{{ trans('common.enabled') }}}</span>
										@else
										<span class="label label-warning">{{{ trans('common.disabled') }}}</span>
										@endif
										@else
										<span class="label label-danger">{{{ trans('common.uninstalled') }}}</span>
										@endif

									</div>

								</li>

								@endforeach

								@endif

							</ul>

							<hr>

							{{-- Extension: Permissions --}}

							@if ($extension->isEnabled())

							<h3>{{{ trans('platform/operations::extensions/common.permissions') }}}</h3>

							<p class="lead">
								{{{ trans('platform/operations::extensions/common.permissions_help',  array('extension' => $extension->name ?: $extension->getSlug(),
								'count' => array_sum(array_map('count', $permissions)))) }}}
							</p>

							<hr>

							<ul class="list-group">

								@if (count($permissions) == 0)

								<li class="list-group-item list-group-item-warning">{{{ trans('platform/operations::extensions/message.permissions_empty') }}}</li>

								@else

								@foreach ($permissions as $group)

									<li class="list-group-item"><h4>{{ $group->name }}</h4></li>

									@foreach ($group->toArray() as $permission)
									<li class="list-group-item">{{ $permission->label }}</li>
									@endforeach

								@endforeach

								@endif

							</ul>

							@endif

						</div>

					</div>

				</div>

			</div>

		</div>

	</div>

</section>

@include('platform/operations::extensions.partials.modals')

@include('platform/operations::extensions._templates.scaffold')
@include('platform/operations::extensions._templates.migration')
@include('platform/operations::extensions._templates.create_migration')
@include('platform/operations::extensions._templates.seeder')
@include('platform/operations::extensions._templates.list')
@include('platform/operations::extensions._templates.controller')
@include('platform/operations::extensions._templates.model')
@include('platform/operations::extensions._templates.repository')
@include('platform/operations::extensions._templates.widget')
@include('platform/operations::extensions._templates.datagrid')

@stop
