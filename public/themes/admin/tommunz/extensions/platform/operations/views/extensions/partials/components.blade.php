{{-- Extension: Components --}}

<h3>{{{ trans('platform/operations::extensions/common.components') }}}</h3>

<p class="lead">{{{ trans('platform/operations::extensions/common.components_help',  array('extension' => $extension->name ?: $extension->getSlug())) }}}</p>


{{-- Component: Controllers --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Controllers</h3>
	</div>

	<table class="table table-hover controllers">
		<tbody></tbody>
	</table>
	<ul class="list-group controllers"></ul>
</div>

{{-- Component: Repositories --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Repositories</h3>
	</div>

	<ul class="list-group repositories"></ul>
</div>

{{-- Component: Widgets --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Widgets</h3>
	</div>

	<ul class="list-group widgets"></ul>
</div>


{{-- Component: Migrations --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Migrations</h3>
	</div>

	<ul class="list-group migrations"></ul>
</div>

{{-- Component: Seeders --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Seeds</h3>
	</div>

	<ul class="list-group seeders"></ul>
</div>

{{-- Component: Models --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">Models</h3>
	</div>

	<ul class="list-group models"></ul>
</div>

{{-- Component: DataGrids --}}
<div class="panel panel-default">

	<div class="panel-heading">
		<h3 class="panel-title">DataGrids</h3>
	</div>

	<ul class="list-group datagrids"></ul>
</div>


