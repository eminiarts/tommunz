@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
 {{{ trans('platform/operations::workshop/common.title') }}}
@stop

{{-- Queue assets --}}
{{ Asset::queue('selectize', 'selectize/css/selectize.bootstrap3.css', 'styles') }}

{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('selectize', 'selectize/js/selectize.js', 'validate') }}
{{ Asset::queue('slugify', 'platform/js/slugify.js', 'selectize') }}
{{ Asset::queue('index', 'platform/operations::js/workshop/index.js', 'platform') }}

@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
@stop

{{-- Page content --}}
@section('page')
<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="workshop-form" action="{{ request()->fullUrl() }}" method="post" accept-char="UTF-8" autocomplete="off" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.operations.extensions.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i>  <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.create") }}}</span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i>  <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('platform/operations::workshop/common.tabs.general') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<div class="row">

							<div class="col-md-6">

								<fieldset>

									<legend>Extension Details</legend>

									<div class="row">

										<div class="col-md-8">
											{{-- Name --}}
											<div class="form-group{{ Alert::onForm('name', ' has-error') }}">

												<label for="name" class="control-label">
													<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.name_help') }}}"></i>
													{{{ trans('platform/operations::workshop/model.general.name') }}}
												</label>

												<input type="text" class="form-control" name="name" id="name" placeholder="{{{ trans('platform/operations::workshop/model.general.name') }}}" value="{{{ Input::old('name') }}}" required>

												<span class="help-block">{{{ Alert::onForm('name') }}}</span>

											</div>
										</div>

										<div class="col-md-4">

											{{-- Version --}}
											<div class="form-group{{ Alert::onForm('version', ' has-error') }}">

												<label for="version" class="control-label">
													<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.version_help') }}}"></i>
													{{{ trans('platform/operations::workshop/model.general.version') }}}
												</label>

												<input type="text" class="form-control" name="version" id="version" placeholder="{{{ trans('platform/operations::workshop/model.general.version') }}}" value="{{{ Input::old('version', '0.1.0') }}}" required>

												<span class="help-block">{{{ Alert::onForm('version') }}}</span>

											</div>

										</div>

									</div>


									{{-- Description --}}
									<div class="form-group{{ Alert::onForm('description', ' has-error') }}">

										<label for="description" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.description_help') }}}"></i>
											{{{ trans('platform/operations::workshop/model.general.description') }}}
										</label>

										<input type="text" class="form-control" name="description" id="description" placeholder="{{{ trans('platform/operations::workshop/model.general.description') }}}" value="{{{ Input::old('name') }}}" required>

										<span class="help-block">{{{ Alert::onForm('description') }}}</span>

									</div>

									{{-- Dependencies --}}
									<div class="form-group{{ Alert::onForm('require', ' has-error') }}">

										<label for="require" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.dependencies_help') }}}"></i>
											{{{ trans('platform/operations::workshop/model.general.dependencies') }}}
										</label>

										<select class="form-control" name="require[]" id="require" multiple>

											@foreach($extensions as $extension)
											<option value="{{ $extension->getSlug()}}">{{ $extension->getSlug()}}</option>
											@endforeach

										</select>

										<span class="help-block">{{{ Alert::onForm('require') }}}</span>

									</div>

								</fieldset>

							</div>

							<div class="col-md-6">

								<fieldset>

									<legend>Vendor Information</legend>

									{{-- Vendor --}}
									<div class="form-group{{ Alert::onForm('vendor', ' has-error') }}">

										<label for="vendor" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.vendor_help') }}}"></i>
											{{{ trans('platform/operations::workshop/model.general.vendor') }}}
										</label>

										<input type="text" class="form-control" name="vendor" id="vendor" placeholder="{{{ trans('platform/operations::workshop/model.general.vendor') }}}" value="{{{ Input::old('vendor', Config::get('platform-operations.vendor')) }}}" required>

										<span class="help-block">{{{ Alert::onForm('vendor') }}}</span>

									</div>

									{{-- Author Name --}}
									<div class="form-group{{ Alert::onForm('author', ' has-error') }}">

										<label for="author" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.author_help') }}}"></i>
											{{{ trans('platform/operations::workshop/model.general.author') }}}
										</label>

										<input type="text" class="form-control" name="author" id="author" placeholder="{{{ trans('platform/operations::workshop/model.general.author') }}}" value="{{{ Input::old('author', Config::get('workbench.name')) }}}" required>

										<span class="help-block">{{{ Alert::onForm('author') }}}</span>

									</div>

									{{-- Author Email --}}
									<div class="form-group{{ Alert::onForm('email', ' has-error') }}">

										<label for="email" class="control-label">
											<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::workshop/model.general.email_help') }}}"></i>
											{{{ trans('platform/operations::workshop/model.general.email') }}}
										</label>

										<input type="email" class="form-control" name="email" id="email" placeholder="{{{ trans('platform/operations::workshop/model.general.email') }}}" value="{{{ Input::old('email', Config::get('workbench.email')) }}}" required>

										<span class="help-block">{{{ Alert::onForm('email') }}}</span>

									</div>

								</fieldset>

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</form>

</section>

@if (config('platform.app.help'))
@include('platform/operations::workshop/help')
@endif

@stop
