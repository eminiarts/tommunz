<script type="text/template" id="controllerTemplate">
	<form id="component-form" data-component="controller" data-extension="{{ $extension->getSlug() }}" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.controller.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.controller.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-8">

					<div class="form-group">

						<label for="name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.controller.name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.controller.name') }}}
						</label>

						<input type="text" name="name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.controller.name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

				<div class="col-md-4">

					<div class="form-group">

						<label for="location" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.controller.location_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.controller.location') }}}
						</label>

						<select name="location" class="form-control create" required>
							<option value="" disabled selected>{{{ trans('platform/operations::extensions/model.controller.location') }}}</option>
							@foreach($controllerLocations as $location)
							<option value="{{ $location }}">{{ $location }}</option>
							@endforeach
						</select>

						<span class="help-block"></span>

					</div>

				</div>

			</div>


			<div class="row">

				<div class="col-md-12">

					<p class="lead text-center">{{{ trans('platform/operations::extensions/model.controller.instruction') }}}</p>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>
