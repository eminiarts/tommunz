<script type="text/template" id="widgetTemplate">
	<form id="component-form" data-component="widget" data-extension="{{ $extension->getSlug() }}" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.widget.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.widget.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-12">

					<div class="form-group">

						<label for="name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.widget.name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.widget.name') }}}
						</label>

						<input type="text" name="name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.widget.name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col-md-12">

					<p class="lead text-center">{{{ trans('platform/operations::extensions/model.widget.instruction') }}}</p>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>

