The operations extension allows you to create and manage extensions for your application.
Features include installing/uninstalling, enabling/disabling or scaffolding new resources.

---

### When should I use it?

The workshop allows you to create new extensions.

---

### How can I use it?

1. Create an extension.
2. Fill out name, description, dependencies, vendor, author name and author email.
3. Hit save.

That's it, now you can start scaffolding or adding new resources to your extension.
