<script type="text/template" id="scaffoldTemplate">
	<form id="component-form" data-component="scaffold" data-extension="{{ $extension->getSlug() }}" parsley-validate-form>

		<div class="modal-header">

			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			<h4 class="modal-title">

				<i class="fa fa-cubes"></i>

				<span>{{{ trans('platform/operations::extensions/model.scaffold.legend') }}}</span>

				<small>{{{ trans('platform/operations::extensions/model.scaffold.help') }}}</small>

			</h4>

		</div>

		<div class="modal-body">

			<div class="row">

				<div class="col-md-12">

					<div class="form-group">

						<label for="name" class="control-label">
							<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('platform/operations::extensions/model.controller.name_help') }}}"></i>
							{{{ trans('platform/operations::extensions/model.scaffold.name') }}}
						</label>

						<input type="text" name="name" class="form-control" placeholder="{{{ trans('platform/operations::extensions/model.scaffold.name_placeholder') }}}" required data-parsley-trigger="change">

						<span class="help-block"></span>

					</div>

				</div>

			</div>

			<br>

			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-default">

						<div class="panel-heading">

							<div class="checkbox">
								<label>
									<input class="checkAll" type="checkbox"> Scaffold All <span class="small">( Recommended )</span>
								</label>
								<a class="components pull-right" data-toggle="collapse" data-target="#components" aria-expanded="false" aria-controls="components">
									<i class="fa fa-flask"></i> Advanced
								</a>
							</div>

						</div>

						<ul class="list-group collapse" id="components">

							<li class="list-group-item">
								<strong>Controllers</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="admin_controller"> Admin Controller
									</label>
								</div>
							</li>

							<li class="list-group-item">

								<div class="checkbox">
									<label>
										<input type="checkbox" name="frontend_controller"> Frontend Controller
									</label>
								</div>
							</li>


							<li class="list-group-item">
								<strong>Routes</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="routes"> Routes
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Resources</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="model"> Model
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="widget"> Widget
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Create/Update Form</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="form"> Form
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Language Files</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="lang"> Language Files
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Repository</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="repository"> Interface &amp; Repository
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Data Grid</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="datagrid"> Data Grid
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Database</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="migration"> Migration &amp; Seeder
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<strong>Configure Extension</strong>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="service_provider"> Service Provider
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="permissions"> Permissions
									</label>
								</div>
							</li>

							<li class="list-group-item">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="menus"> Menu Item
									</label>
								</div>
							</li>

						</ul>

					</div>

				</div>

			</div>

		</div>

		<div class="modal-footer">

			<button type="submit" class="btn btn-primary btn-block">
				<span class="scaffold">{{{ trans('platform/operations::extensions/action.scaffold') }}}</span>
				<span class="scaffolding hide"><i class="fa fa-gear fa-spin"></i> {{{ trans('platform/operations::extensions/action.scaffolding') }}}</span>
			</button>

		</div>

	</form>

</script>


