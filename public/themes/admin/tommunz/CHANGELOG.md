# Default Admin Theme Change Log

This project follows [Semantic Versioning](CONTRIBUTING.md).

## Proposals

We do not give estimated times for completion on `Accepted` Proposals.

- [Accepted](https://github.com/cartalyst/theme-default-admin/labels/Accepted)
- [Rejected](https://github.com/cartalyst/theme-default-admin/labels/Rejected)

---

### v3.0.0 - 2016-08-03

`UPDATED`

- Font-Awesome 4.3.0 > 4.6.3
- Moment.js 2.13.0 > 2.14.1
- Modernizr 2.8.3 > 3.3.1
- jQuery 2.1.3 > 2.2.4
- Redactor 10.2.1 > 10.2.5
- Selectize 0.11.2 > 0.12.2
- Underscore 1.7.0 > 1.8.3

### v2.0.5 - 2016-06-27

`UPDATED`

- Redactor.js 10.0.6 -> 10.2.1.
- Moment.js 2.9.0 -> 2.13.0.

`FIXED`

- Deprecation warnings with moment.js on the date range picker.

### v2.0.4 - 2015-06-15

`UPDATED`

- Pace.js to v1.0.2.

### v2.0.3 - 2015-06-10

`REVISED`

- Slugifier to only slugify it's value if the input to store the slugified value is empty.

### v2.0.2 - 2015-06-02

`FIXED`

- Minor issue on the modal confirm "submit" button that was pushing the page to the top, on longer pages.

### v2.0.1 - 2015-05-12

`FIXED`

- Use a translated string for the error message on the alerts view.

### v2.0.0 - 2015-03-05

- Updated for Platform 3.

### v1.0.6 - 2015-06-15

`UPDATED`

- Pace.js to v1.0.2.

### v1.0.5 - 2015-06-10

`REVISED`

- Slugifier to only slugify it's value if the input to store the slugified value is empty.

### v1.0.4 - 2015-06-02

`FIXED`

- Minor issue on the modal confirm "submit" button that was pushing the page to the top, on longer pages.

### v1.0.3 - 2015-05-12

`FIXED`

- Use a translated string for the error message on the alerts view.

### v1.0.2 - 2015-02-09

`REVISED`

- Popover z-index override.

### v1.0.1 - 2015-01-28

`REVISED`

- Fixed version on `theme.json` file.

### v1.0.0 - 2015-01-26

- Initial stable release with:
    - Twitter Bootstrap 3.3.2
    - Font Awesome 4.3.0
    - jQuery 2.1.3
    - Modernizr 2.8.3
    - Parsleyjs 2.0.7
    - Underscore.js 1.7.0
    - Moment.js 2.9.0
