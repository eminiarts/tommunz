var Vue = require('vue');
var VueRouter = require('vue-router');
var VueResource = require('vue-resource');
var VueHead = require('vue-head');


import Site from './components/Site.vue';
import Home from './components/Home.vue';
import Fest from './components/Fest.vue';
import Danke from './components/Danke.vue';
import Project from './components/Project.vue';
import About from './components/About.vue';


Vue.use(VueRouter);
Vue.use(VueHead);


Vue.component('site', Site)
Vue.component('home', Home)
Vue.component('fest', Fest)
Vue.component('danke', Danke)
Vue.component('project', Project)
Vue.component('about', About)

Vue.use(VueResource);

export
default {}


// The router needs a root component to render.
// For demo purposes, we will just use an empty one
// because we are using the HTML as the app template.
// !! Note that the App is not a Vue instance.
var App = Vue.extend({

  data() {
    return {}
  },

  ready() {
    // console.log('App ready');
  },

  methods: {}
})


// Create a router instance.
// You can pass in additional options here, but let's
// keep it simple for now.
var router = new VueRouter({
  history: true,
  hashbang: false,
  transitionOnLoad: false,
});


// Redirect certain routes

// Define some routes.
router.map({

  '/': {
    name: 'site',
    component: Site,
    subRoutes: {
      '': {
        name: 'home',
        component: Home
      },
      '/about': {
        name: 'about',
        component: About,
      },
      '404': {
        name: 'notfound',
        component: {
          template: '<div class="container">' +
          '<h1>Die gesuchte Seite wurde leider nicht gefunden.</h1>' +
          '<a href="/">Zurück zur Startseite</a>' +
          '</div>'
        }
      },
      'portfolio/:projectSlug': {
        name: 'project-show',
        component: Project,
      },
    }
  },
})

// Vue.config.warnExpressionErrors = false;
// Vue.config.debug = false;

// Now we can start the app!
// The router will create an instance of App and mount to
// the element matching the selector #app.
router.start(App, '#app')
