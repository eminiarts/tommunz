<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <title>Tom Munz Architekt St.Gallen</title>
  <meta name="viewport"
  content="width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=no">

  <style>
    body{
      padding: 0 0 30px 0;
      margin: 0;
      background: #fff;
      color: #000;
      font-family: "TheinhardtRegular", "Theinhardt Regular", "Theinhardt", "Helvetica Neue", arial, sans-serif;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;

      font-size: 16px;
      line-height: 1.2em;
    }
    *{
      box-sizing: border-box;
    }
    a {
      color: #000;
      font-weight: normal;
      text-decoration: none;
    }
    table{
      width: 100%;
    }
    .container{
      width: 100%;
      max-width: 600px;
      padding: 15px;
      margin: 0 auto;
    }
    .content{
      background: #fff;
      padding: 30px;
      width: 100%;
      margin-top: 0;
      border-radius: 0px;
      overflow: hidden;
      /*box-shadow: 0 3px 2px -2px rgba(0,0,0,.04), 0 2px 3px 0 rgba(0,0,0,.098),0 1px 8px 0 rgba(0,0,0,.084); */
    }
    .content-header{
      background: #fff;
      border-radius: 8px 8px 0 0;
      overflow: hidden;
    }
    @media screen and (max-width: 570px){
      .container{
        padding: 0;
      }
      .content{
        border-radius: 0;
        box-shadow: none;
        padding: 20px;
      }
    }
    h1, .h1{
      font-size: 26px;
      font-weight: 500;
      line-height: 1.45em;
    }
    h2{
      margin-top: 0;
      margin-bottom: 25px;
    }
    h3{
      margin-top: 10px;
      margin-bottom: 0px;
    }
    p{
      margin-top: 0;
    }

    .center-text{
      text-align: center;
    }
    .mail-logo{
      height: 100px;
      padding: 20px;
    }

    .btn{
      display: inline-block;
      text-decoration: none;
      padding: 0 16px;
      margin: 6px 8px 16px 0px;
      line-height: 36px;
      text-align: center;
      font-weight: 600;
      text-transform: uppercase;
      font-size: 14px;
      min-width: 84px;
      border: none;
      border-radius: 99px;
      letter-spacing: 1px;
      vertical-align: middle;
      cursor: pointer;
      transition: .2s all ease-in-out;

      background: #000;
      color: #fff;
    }
    .btn:hover, .btn:active, .btn:focus{
      text-decoration: none;
    }
    .btn:active{
      box-shadow: 1px 5px 10px rgba(0, 0, 0, .2);
    }
    a{
      word-break:     break-word;
      -webkit-hyphens: auto;
      -moz-hyphens:    auto;
      hyphens:         auto;
    }
    .span-info{
      font-size: 12px;
      color: #828489;
    }
    .footer{
      padding-top: 15px;
    }
    .footer-link{
      color: #12D789;
      text-decoration: none;
      font-weight: bold;
    }
    .product-row{
    }
    .product-row > td{
      margin: 0;
      border: none;
      border-bottom: 1px solid #888;
    }
    .mt30{
      margin-top: 30px;
    }

  </style>
</head>
<body>

  <table class="container" width="100%" border="0" cellpadding="0" cellspacing="0" style="width:100%;max-width:600px;margin:30px auto 0;padding:0px;">
    <tbody>

      <tr>
        <td class="content">

          <h1>{{$invitation['name']}}. Herzlichen Dank für die Anmeldung.</h1>

          <p class="h1">
            6. Oktober <br>
            ab 17.30 Uhr <br>
            Hintere Poststr. 18 <br>
            9000 St.Gallen <br>
          </p>

          <p>
            {{ $invitation['people'] }} Personen.
          </p>

          <p>
            <a traget="_blank" href="{{ url('Tom-Munz-Trois-Raisons.ics') }}">Veranstaltung zum Kalender hinzufügen</a>
          </p>

          <p class="mt30">
            Tom Munz Architekt <br>
            Hintere Poststrasse 18<br>
            9000 St.Gallen <br>
            +41 (0)71 220 41 41<br>
            <a href="http://tommunz.com/">www.tommunz.com</a><br>

          </p>

        </td>
      </tr>

      <tr>
        <td class="footer center-text">
        </td>
      </tr>

    </tbody>

  </table>

</body>
</html>
