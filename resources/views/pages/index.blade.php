<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000000">
  <meta name="theme-color" content="#000000">

  <title>Tom Munz Architekt St. Gallen</title>
  <meta name="description" content="Architekt St.Gallen, junger Architekt St.Gallen, Architekturen, Architekten St.Gallen, Atelier, Studio, kreativer Architekt, Handwerk und Architektur, Holzbauarchitektur, Holzhaus, Vorarlberg, Wohnbau, öffentliche Bauten, Schulbauten, Kanton St.Gallen, Archithese, BSA, SIA, FSAI, Architekurwettbewerbe Nachwuchsbüro Architektur, ETH, FH, Fachhochschule St.Gallen, Design, Möbelbau, Innenarchitektur, Raumgestaltung, Architekturtheorie, Schweiz, Paris, Hans Kollhoff, Gähler Architekten, Barao Hutter, Stadt St.Gallen, Bauleitung St.Gallen, Baumanagement St.Gallen, Kunst am Bau, Alterswohnungen, Objektgestaltung, Städtebau. Raumplanung, Jury, Hochparterre, Werk Bauen Wohnen, Bauwerk, Finger Architekten, Jeannette Geissmann Architektin, Jens Eggel">

  <!-- Verify ownership for Google Search Console -->
  <meta name="google-site-verification" content="verification_token">
  <!-- Short description of your site's subject -->
  <meta name="subject" content="Architekt St.Gallen, Innenarchitektur, Raumgestaltung, Raumplanung">

  <!-- <meta name="url" content="http://bamboo.ch/"> -->
  <base href="/">

  <link rel="alternate" hreflang="de" href="http://tommunz.com/" />

  <!-- Full domain name or web address -->
  <meta name="url" content="https://tommunz.com/">

  <!-- <meta property="fb:app_id" content="123456789"> -->
  <meta property="og:url" content="https://tommunz.com/">
  <meta property="og:type" content="website">
  <meta property="og:title" content="Tom Munz Architekt St. Gallen">
  <!-- <meta property="og:image" content="https://example.com/image.jpg"> -->
  <meta property="og:description" content="Architekt St.Gallen, junger Architekt St.Gallen, Architekturen, Architekten St.Gallen, Atelier, Studio, kreativer Architekt, Handwerk und Architektur, Holzbauarchitektur, Holzhaus, Vorarlberg, Wohnbau, öffentliche Bauten, Schulbauten, Kanton St.Gallen, Archithese, BSA, SIA, FSAI, Architekurwettbewerbe Nachwuchsbüro Architektur, ETH, FH, Fachhochschule St.Gallen, Design, Möbelbau, Innenarchitektur, Raumgestaltung, Architekturtheorie, Schweiz, Paris, Hans Kollhoff, Gähler Architekten, Barao Hutter, Stadt St.Gallen, Bauleitung St.Gallen, Baumanagement St.Gallen, Kunst am Bau, Alterswohnungen, Objektgestaltung, Städtebau. Raumplanung, Jury, Hochparterre, Werk Bauen Wohnen, Bauwerk, Finger Architekten, Jeannette Geissmann Architektin, Jens Eggel">
  <meta property="og:site_name" content="Tom Munz Architekt St. Gallen">
  <meta property="og:locale" content="de_DE">


  <!-- Links to the author of the document -->
  <link rel="author" content="Emini Arts">

  <link rel="stylesheet" href="fonts/TheinhardtRegular-Regular.css">
  <link rel="stylesheet" href="css/app.css">

</head>
<body id="app">

  <!-- route outlet -->
  <router-view :csrf="['{{ csrf_token() }}']"></router-view>

  <!-- Scripts -->
  <script src="js/jquery.min.js"></script>
  <!-- <script src="js/bootstrap.min.js"></script> -->

  <script src="js/slick.min.js"></script>

  <script async src="js/app.js"></script>

  <script>
    $(document).ready(function(){

      function randomSelect(){
        $('.portfolio__item').removeClass('portfolio__item--active');
        for (var i = $('.portfolio__item').length - 1; i >= 0; i--) {
          var max = 100;
          var min = 0;
          // x% Wahrscheilichkeit, dass das Element aktiv wird
          var prob = 14;
          // Gibt eine Zufallszahl zwischen min (inklusive) und max (exklusive) zurück
          var rand = Math.random() * (max - min) + min;
          if(rand <= prob){
            $($('.portfolio__item')[i]).addClass('portfolio__item--active');
          }
        }
      }

      randomSelect();
      window.setInterval(function(){
        randomSelect();
      }, 4500);

    });
  </script>

</body>
</html>
