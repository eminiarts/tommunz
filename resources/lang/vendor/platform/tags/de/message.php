<?php

return [

    // General messages
    'not_found' => 'Tag [:id] ist nicht vorhanden.',

    // Success messages
    'success' => [
        'create' => 'Tag wurde erfolgreich erstellt.',
        'update' => 'Tag wurde erfolgreich bearbeitet.',
        'delete' => 'Tag wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen des Tags. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten des Tags. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen des Tags. Bitte versuchen Sie es erneut.',
    ],

];
