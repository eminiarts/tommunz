<?php

return [

    'general' => [

        'legend' => 'Details',

        'name'      => 'Name',
        'name_help' => 'Geben Sie den Tag Namen ein',

        'slug'      => 'Slug',
        'slug_help' => 'Geben Sie den Tag Slug ein. Dieser sollte alphanumerisch sein (Unterstriche und Bindestriche sind zulässig).',

        'namespace'      => 'Namespace',
        'namespace_help' => 'Geben Sie den Tag-Namespace ein. Dieser sollte die vollständige Model-Namespace enthalten.',

    ],

    'tag' => [

        'legend'    => 'Tags',

        'tags'      => 'Tags',
        'tags_help' => 'Erstellen oder Tag zuweisen.',

    ],

];
