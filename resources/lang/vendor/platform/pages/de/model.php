<?php

return [

    'general' => [

        'legend'        => 'Details',

        'name'          => 'Name',
        'name_help'     => 'Geben Sie einen beschreibenden Namen für Ihre Seite an.',

        'slug'          => 'Slug',
        'slug_help'     => 'Einzelwort, keine Leerzeichen, keine Sonderwörtern. Bindestriche sind erlaubt.',

        'uri'           => 'Uri',
        'uri_help'      => 'Die Uri der Seite.',

        'https'         => 'Https',
        'https_help'    => 'Soll die Seite über HTTPS präsentiert werden?',

        'enabled'       => 'Status',
        'enabled_help'  => 'Wie ist der Status der Seite?',

        'type'          => 'Speichertyp',
        'type_help'     => 'Wie möchten Sie die Seite speichern bearbeiten?',

        'database'      => 'Datenbank',
        'filesystem'    => 'Filesystem',

        'template'      => 'Template',
        'template_help' => 'Seitenvorlage, die verwendet werden soll.',

        'section'       => 'Section',
        'section_help'  => 'In welche @section() wird die Seite eingebunden?',

        'value'         => 'Markup',
        'value_help'    => "Markup der Seite. @content ist erlaubt.",

        'file'          => 'Datei',
        'file_help'     => 'Wählen Sie die Datei, die beim Rendern angezeigt werden soll.',

    ],

    'access' => [

        'legend'          => 'Zugriffsverwaltung',

        'visibility'      => 'Sichtbarkeit',
        'visibility_help' => 'Wählen Sie, wann auf diese Seite zugegriffen werden kann.',

        'always'          => 'Immer anzeigen',
        'logged_in'       => 'Eingeloggter Benutzer',
        'logged_out'      => 'Ausgeloggter Benutzer',
        'admin'           => 'Nur Admin',

        'roles'           => 'Rollen',
        'roles_help'      => 'Beschränken Sie den Zugriff auf Benutzerrollen.',

    ],

    'navigation' => [

        'legend'      => 'Navigation',

        'menu'        => 'Menü',
        'menu_help'   => 'Fügen Sie diese Seite zur Navigation hinzu.',

        'select_menu' => '-- Wählen Sie ein Menü --',
        'top_level'   => '-- Top Level --',

    ],

];
