<?php

return [

    // General messages
    'not_found' => 'Seite [:id] ist nicht vorhanden.',

    // Success messages
    'success' => [
        'create' => 'Seite wurde erfolgreich erstellt.',
        'update' => 'Seite wurde erfolgreich bearbeitet.',
        'delete' => 'Seite wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen der Seite. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten der Seite. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen der Seite. Bitte versuchen Sie es erneut.',
    ],

];
