<?php

return [

    'title'  => 'Seiten',

    'tabs' => [
        'general'     => 'Seite',
        'access'      => 'Zugriff',
        'navigation'  => 'Navigation',
        'tags'        => 'Tags',
        'attributes'  => 'Attribute',
    ],

];
