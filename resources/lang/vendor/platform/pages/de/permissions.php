<?php

return [

    'index'  => 'Seiten anzeigen',
    'create' => 'Neue Seiten erstellen',
    'copy'   => 'Seiten kopieren',
    'edit'   => 'Seiten bearbeiten',
    'delete' => 'Seiten löschen',

];