<?php

return [

    'title' => 'Rollen',

    'tabs' => [
        'general'     => 'Rolle',
        'permissions' => 'Berechtigungen',
        'attributes'  => 'Attribute',
    ],

];
