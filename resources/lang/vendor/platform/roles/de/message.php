<?php

return [

    // General messages
    'not_found' => 'Rolle [:id] ist nicht vorhanden.',

    // Success messages
    'success' => [
        'create' => 'Rolle wurde erfolgreich erstellt.',
        'update' => 'Rolle wurde erfolgreich bearbeitet.',
        'delete' => 'Rolle wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen der Rolle. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten der Rolle. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen der Rolle. Bitte versuchen Sie es erneut.',
    ],

];
