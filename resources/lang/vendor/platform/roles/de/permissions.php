<?php

return [

    'index'  => 'Rollen anzeigen',
    'create' => 'Neue Rollen erstellen',
    'edit'   => 'Rollen ansehen/bearbeiten',
    'delete' => 'Rollen löschen',

];
