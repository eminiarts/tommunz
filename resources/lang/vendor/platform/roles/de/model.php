<?php

return [

    'general' => [

        'legend'    => 'Details',

        'name'      => 'Name',
        'name_help' => 'Geben Sie den Namen der Rolle an.',

        'slug'      => 'Slug',
        'slug_help' => 'Geben Sie den Slug der Rolle an. Dieser sollte alphanumerisch sein (Unterstriche und Bindestriche sind zulässig).',

    ],

    'permissions' => [

        'legend' => 'Berechtigungen',

    ],

];
