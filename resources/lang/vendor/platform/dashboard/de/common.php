<?php

return [

    'title'        => 'Dashboard',
    'introduction' => 'Willkommen zum Dashboard von Bamboo. Hier werden Statistiken und die wichtigsten Links angezeigt.',

];
