<?php

return [

    'no_access'    => "Sie haben keinen Zugang zu diesem Bereich.",
    'no_access_to' => "Sie haben keinen Zugang zu [:permission].",

];
