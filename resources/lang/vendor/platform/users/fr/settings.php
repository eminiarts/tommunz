<?php

/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    3.1.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    'section' => [
        'general' => 'General',
    ],

    'registration' => [
        'label' => 'Allow User Registration',
        'info'  => 'Enable or disable the user registration completely.',
    ],

    'activation' => [
        'label' => 'Account activation',
        'info'  => 'Change the activation behavior.',

        // Activation types
        'automatic' => 'No activation (immediate access)',
        'email'     => 'Activation by Email',
        'admin'     => 'Admin activation',
    ],

    'default_role' => [
        'label' => 'Default User Role',
        'info'  => "Define here the default role that users get assigned to when they create a new account.",
    ],

];
