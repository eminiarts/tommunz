<?php

/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    3.1.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    'general' => [

        'legend'                     => 'Details',

        'name'                       => 'Name',

        'first_name'                 => 'First Name',
        'first_name_help'            => 'Type the user\'s first name.',

        'last_name'                  => 'Last Name',
        'last_name_help'             => 'Type the user\'s last name.',

        'email'                      => 'Email',
        'email_help'                 => 'Type the user\'s email address.',
    ],

    'authorization' => [

        'legend'                     => 'Authorization',

        'roles'                      => 'Roles',
        'roles_help'                 => 'Select the roles to assign to the user, remember that a user takes on the permissions of the roles they are assigned.',

        'activated'                  => 'Activated',
        'activated_help'             => 'Select if you want the user to be automatically activated.',
    ],

    'authentication' => [

        'legend'                     => 'Authentication',

        'password_confirmation'      => 'Password Confirmation',
        'password_confirmation_help' => 'Confirm the user\'s password.',

        'create' => [
            'password'      => 'Password',
            'password_help' => 'Type the user\'s password.',
        ],

        'update' => [
            'password'      => 'Change Password',
            'password_help' => 'Type the user\'s new password or leave empty to remain unchanged.',
        ],

    ],

    'permissions' => [

        'legend'    => 'Permissions',

    ],

];
