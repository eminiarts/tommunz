<?php

/**
 * Part of the Platform Users extension.
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Cartalyst PSL License.
 *
 * This source file is subject to the Cartalyst PSL License that is
 * bundled with this package in the LICENSE file.
 *
 * @package    Platform Users extension
 * @version    3.1.0
 * @author     Cartalyst LLC
 * @license    Cartalyst PSL
 * @copyright  (c) 2011-2015, Cartalyst LLC
 * @link       http://cartalyst.com
 */

return [

    'subject' => [
        'welcome'               => 'Welcome to :sitename',
        'activate-account'      => 'Activate your Account',
        'activate-user-account' => 'Activate user account',
        'account-activated'     => 'Account activated',
        'account-deactivated'   => 'Account deactivated',
        'reset-password'        => 'Account Password Reset',
    ],

];
