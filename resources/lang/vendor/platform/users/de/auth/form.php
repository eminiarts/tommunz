<?php

return [

    'first_name' => 'Vorname',
    'first_name_help' => 'Geben Sie Ihren Vornamen an.',
    'first_name_error' => 'Eine Vorname ist erforderlich.',
    'last_name' => 'Nachname',
    'last_name_help' => 'Geben Sie Ihren Nachnamen an.',
    'last_name_error' => 'Ein Nachname ist erforderlich.',
    'email' => 'E-Mail',
    'email_placeholder' => 'meine@email.ch',
    'email_help' => 'Geben Sie Ihre E-Mail-Adresse ein.',
    'email_error' => 'Eine gültige E-Mail-Adresse ist erforderlich.',
    'password' => 'Passwort',
    'password_placeholder' => '&#9679;&#9679;&#9679;&#9679;&#9679;',
    'password_help' => 'Geben Sie Ihr Passwort ein.',
    'password_error' => 'Das Passwort muss mindestens 6 Zeichen lang sein.',
    'password_confirmation' => 'Kennwort bestätigen',
    'password_confirmation_help' => 'Bestätigen Sie Ihr Passwort.',
    'password_confirmation_error' => 'Passwort stimmt nicht überein.',
    'required' => 'Erforderlich',

    'login' => [
      'legend' => 'Anmelden',
      'legend_social' => 'Anmelden mit',
      'remember-me' => 'Angemeldet bleiben',
      'submit' => 'Anmelden',
      'forgot-password' => 'Passwort vergessen?',
      'no_account' => 'Haben Sie kein Konto?',
      'register' => 'Registrieren',
      'or_recover' => 'oder',
    ],

    'login-social' => [
        'legend' => 'Social Login',
    ],

    'register' => [
        'legend' => 'Registrieren',
        'submit' => 'Neues Konto erstellen',
        'disabled' => 'Die Registrierung ist deaktiviert.',
    ],

    'profile' => [
        'legend' => 'Profil',
        'submit' => 'Speichern',
    ],

    'forgot-password' => [
        'legend' => 'Passwort vergessen',
        'submit' => 'Passwort zurücksetzen',
    ],

    'reset-password' => [
        'legend' => 'Ändern Sie Ihr Passwort',
        'password' => 'Neues Passwort',
        'password_help' => 'Geben Sie Ihr neues Passwort ein.',
        'password_confirmation' => 'Passwort bestätigen.',
        'password_confirmation_help' => 'Bestätigen Sie Ihr neues Passwort.',
        'submit' => 'Passwort ändern',
    ],

];
