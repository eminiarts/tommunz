<?php

return [

    // General messages
    'user_not_found'      => 'Falsche E-Mail oder falsches Passwort.',
    'user_already_exists' => 'Benutzer ist bereits vorhanden.',
    'user_is_activated'   => 'Ihr Konto wurde bereits aktiviert.',
    'user_not_activated'  => 'hr Konto ist nicht aktiviert.',
    'user_is_banned'      => 'Ihr Konto wurde gesperrt.',
    'invalid_email'       => 'E-Mail ist ungültig oder wurde nicht gefunden.',
    'invalid_password'    => 'Passwort ist nicht korrekt.',

    'throttling' => [
        'global' => 'Zu viele erfolglose Anmeldungen wurden unternommen. Die Anmeldungen ist für :delay Sekunde(n) gesperrt.',
        'ip' => 'Verdächtige Aktivitäten sind von Ihrer IP-Adresse aufgetreten. Ihnen wird ein weiterer Zugang für :delay Sekdune(n) verweigert.',
        'user' => 'Zu viele erfolglose Login-Versuche. Bitte versuchen Sie es nach :delay Sekunde(n) erneut.',
    ],

    // Success messages
    'success' => [
        'login'    => 'Erfolgreich eingeloggt.',
        'register' => 'Konto erfolgreich erstellt.',
        'update'   => 'Ihr Konto wurde aktualisiert.',
        'activate' => 'Ihr Konto ist jetzt aktiviert. Danke für die Registrierung.',

        'reset-password-confirm' => 'Ihr Passwort wurde erfolgreich zurückgesetzt. Jetzt können Sie sich mit Ihrem neuen Passwort anmelden.',
    ],

    // Error messages
    'error' => [
        'login'    => 'Es gab ein Problem bei der Anmeldung. Bitte versuchen Sie es erneut',
        'register' => 'Es gab ein Problem der Erstellung Ihres Kontos. Bitte versuche es erneut.',
        'activate' => 'Ihr Konto wurde bereits aktiviert.',

        'reset-password'         => 'Ihr Passwort konnte nicht zurückgesetzt werden. Bitte stellen Sie sicher, dass Sie eine registrierte E-Mail-Adresse verwenden.',
        'reset-password-confirm' => 'Es gab ein Problem der Bestätigung Ihres Passwort-Reset-Codes. Bitte versuchen Sie es erneut.',
    ],

];
