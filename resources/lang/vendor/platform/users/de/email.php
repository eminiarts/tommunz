<?php

return [

    'subject' => [
        'welcome'               => 'Willkommen bei :sitename',
        'activate-account'      => 'Aktivieren Sie Ihr Konto',
        'activate-user-account' => 'Aktivieren Sie Ihr Benutzerkonto',
        'account-activated'     => 'Konto aktiviert',
        'account-deactivated'   => 'Konto deaktiviert',
        'reset-password'        => 'Passwort zurücksetzen',
    ],

];
