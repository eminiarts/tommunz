<?php

return [

    'general' => [

        'legend'                     => 'Details',

        'name'                       => 'Name',

        'first_name'                 => 'Vormame',
        'first_name_help'            => 'Geben Sie Ihren Vornamen an.',

        'last_name'                  => 'Nachname',
        'last_name_help'             => 'Geben Sie Ihren Nachnamen an.',

        'email'                      => 'E-Mail',
        'email_help'                 => 'Geben Sie Ihre E-Mail-Adresse an.',
    ],

    'authorization' => [

        'legend'                     => 'Autorisierung',

        'roles'                      => 'Rollen',
        'roles_help'                 => 'Wählen Sie die Rollen, die dem Benutzer zugeordnet werden. Denken Sie daran, dass ein Benutzer über die Berechtigungen der Rollen verfügt',

        'activated'                  => 'Aktiviert',
        'activated_help'             => 'Wählen Sie, dass der Benutzer automatisch aktiviert werden soll.',
    ],

    'authentication' => [

        'legend'                     => 'Authentifizierung',

        'password_confirmation'      => 'Passwort-Bestätigung',
        'password_confirmation_help' => 'Bestätigen Sie das Passwort.',

        'create' => [
            'password'      => 'Passwort',
            'password_help' => 'Geben Sie das Passwort ein.',
        ],

        'update' => [
            'password'      => 'Passwort ändern',
            'password_help' => 'Geben Sie ein neues Passwort ein, oder lassen Sie dieses Feld leer, damit es unverändert bleibt.',
        ],

    ],

    'permissions' => [

        'legend'    => 'Berechtigungen',

    ],

];
