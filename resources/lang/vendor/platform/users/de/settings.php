<?php

return [

    'section' => [
        'general' => 'General',
    ],

    'registration'  => [
        'label'     => 'Benutzerregistrierung zulassen',
        'info'      => 'Aktivieren oder deaktivieren die Benutzerregistrierung.',
    ],

    'activation'    => [
        'label'     => 'Konto-Aktivierung',
        'info'      => 'Ändern Sie das Aktivierungsverhalten.',

        // Activation types
        'automatic' => 'Keine Aktivierung (unmittelbarer Zugang)',
        'email'     => 'Aktivierung per E-Mail',
        'admin'     => 'Aktivierung durch Administrator',
    ],

    'default_role'  => [
        'label'     => 'Benutzerrolle',
        'info'      => "Hier definieren Sie die Standardrolle, welche dem Benutzer zugewiesen wird, wenn er sich registriert.",
    ],

];
