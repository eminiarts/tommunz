<?php

return [

    'title' => 'Benutzer',

    'tabs' => [
        'general'     => 'Benutzer',
        'permissions' => 'Berechtigungen',
        'attributes'  => 'Attribute',
    ],

];
