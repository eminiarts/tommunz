<?php

return [

    // General messages
    'not_found' => 'Benutzer [:id] ist nicht vorhanden',

    // Success messages
    'success' => [
        'create' => 'Benutzer wurde erfolgreich erstellt.',
        'update' => 'Benutzer wurde erfolgreich bearbeitet.',
        'delete' => 'Benutzer wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen des Benutzers. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten des Benutzers. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen des Benutzers. Bitte versuchen Sie es erneut.',
    ],

];
