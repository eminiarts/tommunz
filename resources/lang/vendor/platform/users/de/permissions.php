<?php

return [

    'index'  => 'Benutzer anzeigen',
    'create' => 'Neue Benutzer erstellen',
    'edit'   => 'Benutzer anzeigen/bearbeiten',
    'delete' => 'Benutzer löschen',

];
