<?php

return [

    'file_exists' => 'Datei bereits vorhanden.',

    'file_size_exceeded' => 'Dateigröße überschritten',

    'invalid_file' => 'Ungültige hochgeladenen Datei.',

    'invalid_mime' => 'Ungültiger Dateityp.',

    'not_found' => 'Datei [:id] ist nicht vorhanden.',

    // Success messages
    'success' => [
        'update' => 'Die Datei wurde erfolgreich aktualisiert.',
        'delete' => 'Die Datei wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'update'   => 'Es gab ein Problem bei der Aktualisierung der Medien. Bitte versuchen Sie es erneut.',
        'delete'   => 'Es gab ein Problem beim Löschen der Medien. Bitte versuchen Sie es erneut.',
        'multiple' => '{1} 1 Element wurde erfolgreich entfernt.|[2,Inf] :items Elemente wurden erfolgreich entfernt.',
    ],

];
