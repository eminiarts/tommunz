<?php

return [

    'index'  => 'Medien anzeigen',
    'upload' => 'Medien hochladen',
    'edit'   => 'Medien bearbeiten',
    'delete' => 'Medien löschen',

];
