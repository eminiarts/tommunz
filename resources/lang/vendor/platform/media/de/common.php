<?php

return [

    'title'  => 'Medien',

    'tabs' => [
        'general' => 'Medien',
        'tags'    => 'Tags',
        'email'   => 'Medien E-Mail',
    ],

];
