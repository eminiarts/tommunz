<?php

return [

    'general' => [

        'legend'       => 'Details',

        'name'         => 'Name',
        'name_help'    => 'Geben Sie einen beschreibenden Namen für Ihre Datei ein.',

        'file'         => 'Datei',
        'size'         => 'Grösse',

        'roles'        => 'Rollen',
        'roles_help'   => 'Wählen Sie die Rollen, die auf die Datei zugreifen können.',

        'status'       => 'Status',
        'status_help ' => '',

        'share'        => 'Teilen',
        'download'     => 'Download',
        'email'        => 'E-Mail',
        'public'       => 'Öffentlich',
        'private'      => 'Privat',

    ],

];
