<?php

return [

    'send_email' => 'E-Mail senden',
    'share'      => 'Teilen',
    'download'   => 'Download',

    'bulk' => [
        'email'   => 'Ausgewählte E-Mail',
        'private' => 'Privat einstellen',
        'public'  => 'Öffentlich einstellen',
    ],

    'filter' => [
        'private' => 'Alle private',
        'public'  => 'Alle öffentliche',
    ]

];
