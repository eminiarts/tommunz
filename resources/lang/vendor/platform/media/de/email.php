<?php

return [

    'attachments' => 'Anhänge',
    'total_bytes' => 'Total',

    'users'       => 'Benutzer',
    'users_help'  => 'Wählen Sie die Empfänger aus.',

    'roles'       => 'Rollen',
    'roles_help'  => 'Rollen als Empfänger auswählen.',

];
