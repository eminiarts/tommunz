<?php

return [

    'general' => [

        'legend'         => 'Details',

        'name'           => 'Name',
        'name_help'      => 'Geben Sie einen beschreibenden Namen für das Attribut ein.',

        'description'      => 'Beschreibung',
        'description_help' => 'Geben Sie eine informative Beschreibung des Attributs ein.',

        'slug'           => 'Slug',
        'slug_help'      => 'Einzelwort, keine Leerzeichen, keine Sonderwörtern. Bindestriche sind erlaubt.',

        'namespace'      => 'Namespace',
        'namespace_help' => "Wählen Sie das Namespace des Attributs.",

        'enabled'        => 'Status',
        'enabled_help'   => 'Wählen Sie den Status des Attributs.',

    ],

    'types' => [

        'legend'       => 'Attribut-Typ',

        'type'         => 'Typ',
        'type_help'    => 'Wählen Sie den Typ des Attributs',

        'option_value' => 'Option Value',

        'option_label' => 'Option Label',

        'option' => [
            'checkbox'    => 'Checkbox',
            'input'       => 'Input',
            'multiselect' => 'Multi Select',
            'radio'       => 'Radio',
            'select'      => 'Select',
            'textarea'    => 'Textarea',
        ],

    ],

];
