<?php

return [

    // General messages
    'not_found' => 'Attribut [:id] ist nicht vorhanden.',

    'options_not_allowed' => "Der ausgewählte Typ erlaubt keine erweiterte Optionen.",

    // Success messages
    'success' => [
        'create' => 'Attribut wurde erfolgreich erstellt.',
        'update' => 'Attribut wurde erfolgreich bearbeitet.',
        'delete' => 'Attribut wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen der Attribut. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten der Attribut. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen der Attribut. Bitte versuchen Sie es erneut.',
    ],

];
