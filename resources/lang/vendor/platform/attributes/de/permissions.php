<?php

return [

    'index'  => 'Attribute anzeigen',
    'create' => 'Attribut erstellen',
    'edit'   => 'Attribut anzeigen/bearbeiten',
    'delete' => 'Attribut löschen',

];
