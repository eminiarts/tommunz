<?php

return [

    // Section title
    'title' => 'Themes',

    // Group
    'general' => [

        // Group title
        'title' => 'Allgemein',

        // Fields
        'field' => [
            'area' => ':area Theme',
        ],

    ],

    // Group
    'settings' => [

        // Group title
        'title' => 'Einstellungen',

        // Fields
        'field' => [
            'debug'           => 'Debug Status',
            'force_recompile' => 'Asset Kompilierung erzwingen',
            'auto_clear'      => 'Kompilierte Assets erneuern',
        ],

    ],

];
