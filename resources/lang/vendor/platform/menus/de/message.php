<?php

return [

    'already_exists' => 'Menü bereits vorhanden!',
    'not_found'      => 'Menü [:id] ist entweder kein Grund-Eintrag oder es existiert nicht.',
    'no_children'    => 'Es gibt noch keine Einträge für dieses Menü, warum erstellen Sie nicht einen Eintrag?',

    // Success messages
    'success' => [
        'create' => 'Menü wurde erfolgreich erstellt.',
        'update' => 'Menü wurde erfolgreich bearbeitet.',
        'delete' => 'Menü wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen des Menüs. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten des Menüs. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen des Menüs. Bitte versuchen Sie es erneut.',
    ],

];
