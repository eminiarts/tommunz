<?php

return [

    'general' => [

        'create' => [
            'legend'      => 'Neuen Link erstellen',
            'description' => 'Fügen Sie einen Navigationspunkt hinzu.',
        ],

        'update' => [
            'legend'      => 'Element bearbeiten',
            'description' => 'Navigationspunkt bearbeiten.',
        ],

        'name'      => 'Menü-Name',
        'name_help' => 'Geben Sie den Namen des Menüs ein.',

        'slug'      => 'Slug',
        'slug_help' => 'Einzelwort, keine Leerzeichen und keine besonderen Zeichen. Bindestriche sind erlaubt.',

        'name_item'      => 'Link Name',
        'name_item_help' => 'Geben Sie den Namen des Navigationspunktes ein.',

        'slug_item'      => 'Slug',
        'slug_item_help' => 'Einzelwort, keine Leerzeichen und keine besonderen Zeichen. Bindestriche sind erlaubt.',

        'enabled'      => 'Status',
        'enabled_help' => 'Wie ist der Status des Menüpunktes?',

        'parent'      => 'Parent',
        'parent_help' => 'Chose the parent that this item belongs to or leave the default option selected for it to be a "root" menu item of this menu.',

        'type'      => 'Type',
        'type_help' => 'Select the item url type.',

        'type_static' => 'Statisch',

        'uri'      => 'Uri',
        'uri_help' => 'Geben Sie die URI ein.',

        'secure'      => 'HTTPS',
        'secure_help' => 'Soll die Seite über HTTPS präsentiert werden?',

        'visibility'      => 'Sichtbarkeit',
        'visibility_help' => 'Wann sollte dieser Menüpunkt gesehen werden?',

        'visibilities' => [
            'always'        => 'Immer anzeigen',
            'logged_in'     => 'Eingeloggter Benutzer',
            'logged_out'    => 'Ausgeloggter Benutzer',
            'admin'         => 'Nur Admin',
        ],

        'roles'      => 'Rollen',
        'roles_help' => 'Welche Benutzerrollen sollten in der Lage, diesen Menüpunkt zu sehen?',

        'class'      => 'Klassen',
        'class_help' => 'Die Klasse, die an das <li> Element zugewiesen wird.',

        'target'      => 'Ziel',
        'target_help' => 'Das Zielattribut gibt an, wie Sie den Menüpunkt öffnen.',

        'targets' => [
            'self'   => 'Gleiches Fenster',
            'blank'  => 'Neues Fenster',
            'parent' => 'Übergeordneter Frame',
            'top'    => 'Oberer Rahmen (Hauptdokument)',
        ],

        'regex'      => 'Regular Expressions',
        'regex_help' => 'Regex-Muster für Fortgeschrittene ausgewählte Elemente',

        'created_at' => 'Erstellt am',

        'items_count' => '# Elemente',
        'items'       => '{0} Keine Elemente|{1} 1 Element|[2,Inf] :count Elemente',

        'item_details' => 'Details',
        'advanced_settings' => 'Erweiterte Einstellungen',

    ],

];
