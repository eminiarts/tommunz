<?php

return [

    'index'  => 'Menüs anzeigen',
    'create' => 'Neue Menüs erstellen',
    'edit'   => 'Menüs anzeigen/bearbeiten',
    'delete' => 'Menüs löschen',

];