<?php

return [

    // Section title
    'title' => 'Allgemeine Einstellungen',

    // Group
    'admin' => [

        // Group title
        'title' => 'Admin',

        // Fields
        'field' => [
            'help' => 'App Hilfe',
        ],

    ],

    // Group
    'application' => [

        // Group title
        'title' => 'Anwendungsgrundlagen',

        // Fields
        'field' => [
            'title'     => 'Titel',
            'tagline'   => 'Tagline',
            'copyright' => 'Copyright',
        ],

    ],

    // Group
    'email' => [

        // Group title
        'title' => 'Email Settings',

        // Fields
        'field' => [
            'driver'        => 'Mail Driver',
            'host'          => 'Host Addrese',
            'port'          => 'Host Port',
            'encryption'    => 'Encryption Protokol',
            'from_address'  => 'Von Adresse',
            'from_name'     => 'Von Name',
            'username'      => 'Server Username',
            'password'      => 'Server Passwort',
            'sendmail_path' => 'Sendmail System Path',
            'pretend'       => 'Main "Pretend"',
        ],

    ],

];
