<?php

return [

    'title' => 'Einstellungen',

    'no_fieldsets' => 'Kein Feld für [:section]',

    'no_fields' => 'Keine Felder für [:fieldset] im Abschnitt [:section]',

];
