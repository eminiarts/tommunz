<?php

return [

    'title' => 'Workshop',

    'autoload' => 'Composer konnte nicht gefunden werden. Du musst manuell dies manuell angeben: \'composer dump-autoload\'.',

    'writable' => 'Workshop wurde deaktiviert, weil das Verzeichnis ist nicht beschreibbar ist.',

    'tabs' => [
        'general' => 'Extension',
    ],

];
