<?php

return [

    'general' => [

        'author'      => 'Name des Erstellers',
        'author_help' => 'Der Name des Erstellers der Erweiterung.',

        'email'      => 'E-Mail des Erstellers',
        'email_help' => 'Die E-Mail-Adresse des Erstellers.',

        'vendor'      => 'Vendor',
        'vendor_help' => 'Dies sollte nur aus Kleinbuchstaben und Bindestrichen bestehen. Dies wird bei Bedarf (wie Namespaces und Klassennamen) überschrieben.',

        'name'      => 'Name',
        'name_help' => 'Dies sollte nur aus Kleinbuchstaben und Bindestrichen bestehen. Dies wird bei Bedarf (wie Namespaces und Klassennamen) überschrieben.',

        'description'      => 'Beschreibung',
        'description_help' => 'Geben Sie eine kurze Beschreibung über Ihre Erweiterung an.',

        'version'      => 'Version',
        'version_help' => 'Geben Sie die Version der Erweiterung an.',

        'uri'      => 'URI',
        'uri_help' => 'Geben Sie die relative URI an, die zur automatischen Steuerung (Routing) innerhalb Ihrer Erweiterung verwendet wird.',

        'dependencies'      => 'Abhängigkeiten',
        'dependencies_help' => 'Geben Sie die Slugs der Erweiterungen an, welche diese Erweiterung erfordert. Einen Eintrag pro Zeile.',

        'components' => 'Komponenten',

        'install' => 'Installierung - Methode',

    ],

];
