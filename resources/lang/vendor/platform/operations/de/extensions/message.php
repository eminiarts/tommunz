<?php

return [

    'install'   => 'Erweiterung :extension erfolgreich installiert.',
    'uninstall' => 'Erweiterung :extension erfolgreich deinstalliert.',
    'enable'    => 'Erweiterung :extension erfolgreich aktiviert.',
    'disable'   => 'Erweiterung :extension erfolgreich deaktiviert.',
    'upgrade'   => 'Erweiterung :extension erfolgreich aktualisiert.',

    'permissions_empty'  => 'Es gibt keine Berechtigungen für diese Erweiterung.',
    'dependencies_empty' => 'Es gibt keine Abhängigkeiten für diese Erweiterung.',
    'dependents_empty' => 'Es gibt keine untergeordnete Abhängigkeiten für diese Erweiterung.',
    'components_empty' => 'Es gibt keine Scaffolds für diese Erweiterung.',

    'core_extension' => ':extension ist eine grundlegende Erweiterung.',
    'core_extension_warning' => 'Scaffold in einer grundlegenden Erweiterung ist nicht möglich. Sollten Sie die :extension erweitern, empfehlen wir, die Erweiterung zu duplizieren oder selber zu erweitern.',

    'writable' => ':extension ist nicht beschreibbar..',
    'writable_warning' => 'Scaffolding benötigt Berechtigungen, die Erweiterung ins Verzeichnis zu schreiben.',

];
