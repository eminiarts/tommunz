<?php

return [

    'title'  => 'Erweiterungen',
    'manage' => 'Erweiterungen verwalten',

    'extension_status' => ':installation und :status.',

    'all_installed'   => 'Alle installiert',
    'all_uninstalled' => 'Alle deinstalliert',

    'show_installed'   => 'Zeige installierte',
    'show_uninstalled' => 'Zeige uninstallierte',

    'scaffold' => 'Scaffold',

    'autoload' => 'Composer konnte nicht gefunden werden. Du musst manuell dies manuell angeben: \'composer dump-autoload\'.',

    'writable' => 'Workshop kann den Ordner der Erweiterungen nicht beschreiben.',

    'tabs' => [
        'general'           => 'Extension',
        'components'        => 'Komponenten',
    ],

    'components'      => 'Komponenten',
    'components_help' => ':extension has the following components.',

    'permissions'      => 'Berechtigungen',
    'permissions_help' => ':extension has :count permissions.',

    'dependencies'      => 'Abhängigkeiten',
    'dependencies_help' => ':extension (übergeordnet) ist abhängig von diesen Erweiterungen.',

    'dependents'      => 'Untergeordnet',
    'dependents_help' => 'Diese Erweiterungen sind :extension untergeordnet.',

];
