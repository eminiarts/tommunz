<?php

return [

    'publish_theme' => 'Publish Theme',
    'dump_autoload' => 'Dump Autoload',
    'scaffold'      => 'Scaffold',
    'scaffolding'   => 'Scaffolding',

];
