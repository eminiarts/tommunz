<?php

return [

    'autoloaded' => 'Die Erweiterung wird nicht automatisch geladen und funktioniert nicht richtig. Bitte "dump autoload" verwenden.',

    'composer' => 'Composer konnte nicht gefunden werden. Stellen Sie sicher, dass Composer global installiert und verfügbar ist.',

    'permissions' => 'Kann nicht in das Workbench-Verzeichnis schreiben. Stellen Sie sicher, dass das Verzeichnis vom Webserver beschreibbar ist.',

];
