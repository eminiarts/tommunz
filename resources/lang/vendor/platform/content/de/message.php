<?php

return [

    // General messages
    'not_found' => 'Inhalt [:id] ist nicht vorhanden.',

    // Success messages
    'success' => [
        'create' => 'Inhalt wurde erfolgreich erstellt.',
        'update' => 'Inhalt wurde erfolgreich bearbeitet.',
        'delete' => 'Inhalt wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error' => [
        'create' => 'Es gab ein Problem beim Erstellen der Inhalt. Bitte versuchen Sie es erneut.',
        'update' => 'Es gab ein Problem beim Bearbeiten der Inhalt. Bitte versuchen Sie es erneut.',
        'delete' => 'Es gab ein Problem beim Löschen der Inhalt. Bitte versuchen Sie es erneut.',
    ],

];
