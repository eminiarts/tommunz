<?php

return [

    'index'  => 'Inhalt anzeigen',
    'create' => 'Inhalt erstellen',
    'copy'   => 'Inhalt kopieren',
    'edit'   => 'Inhalt bearbeiten',
    'delete' => 'Inhalt löschen',

];
