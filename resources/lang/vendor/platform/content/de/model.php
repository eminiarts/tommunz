<?php

return [

    'general' => [

        'legend'       => 'Details',

        'name'         => 'Name',
        'name_help'    => 'Name des Inhalts eingeben.',

        'slug'         => 'Slug',
        'slug_help'    => 'Einzelwort, keine Leerzeichen, keine Sonderwörtern. Bindestriche sind erlaubt.',

        'enabled'      => 'Status',
        'enabled_help' => 'Ist der Inhalt aktiv?',

        'type'         => 'Speichertyp',
        'type_help'    => 'Wie wollen Sie den Inhalt speicher und bearbeiten?',

        'database'     => 'Datenbank',
        'filesystem'   => 'Filesystem',

        'value'        => 'Inhalt',
        'value_help'   => 'Inhalt. @content ist erlaubt.',

        'file'         => 'Datei',
        'file_help'    => 'Die Datei, die verwendet wird, um den Inhalt anzuzeigen.',

    ],

];
