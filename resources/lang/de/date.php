<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Date Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used throughout the whole application.
    | You can change them to anything you want to customize your views to
    | better match your application.
    |
     */

    'day'        => 'Tag',
    'week'       => 'Woche',
    'month'      => 'Monat',
    'today'      => 'Heute',
    'tomorrow'   => 'Morgen',
    'yesterday'  => 'Gestern',
    'this_month' => 'Diesen Monat',
    'last_month' => 'Letzten Monat',
    'next_month' => 'Nächsten Monat',

    'months'     => [
        'january'   => 'Januar',
        'february'  => 'Februar',
        'march'     => 'März',
        'april'     => 'April',
        'may'       => 'Mai',
        'june'      => 'Juni',
        'july'      => 'Juli',
        'august'    => 'August',
        'september' => 'September',
        'october'   => 'Oktober',
        'november'  => 'November',
        'december'  => 'Dezember',
    ],

    'days'       => [
        'monday'    => 'Montag',
        'tuesday'   => 'Dienstag',
        'wednesday' => 'Mittwoch',
        'thursday'  => 'Donnerstag',
        'friday'    => 'Freitag',
        'saturday'  => 'Samstag',
        'sunday'    => 'Sonntag',
    ],

];
