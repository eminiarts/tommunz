<?php

return [

    'error'         => 'Fehler',
    'success'       => 'Erfolg',
    'info'          => 'Info',
    'warning'       => 'Warnung',
    'danger'        => 'Gefahr',
    'yes'           => 'Ja',
    'no'            => 'Nein',
    'all'           => 'Alle',
    'enabled'       => 'Aktiviert',
    'disabled'      => 'Deaktiviert',
    'installed'     => 'Installiert',
    'uninstalled'   => 'Deinstalliert',
    'actions'       => 'Aktionen',
    'bulk_actions'  => 'Bulk-Aktionen',
    'allow'         => 'Zulassen',
    'deny'          => 'Ablehnen',
    'inherit'       => 'Erben',
    'reset'         => 'Zurücksetzen',
    'refresh'       => 'Aktualisieren',

    // Search, Filter & Pagination
    'no_results'    => 'Keine Ergebnisse',
    'showing'       => 'Zeige',
    'to'            => 'bis',
    'in'            => 'in',
    'of'            => 'von',
    'entries'       => 'Einträge',
    'loading'       => 'Laden',
    'search'        => 'Suchen',
    'filters'       => 'Filter',
    'remove_filter' => 'Filter entfernen',
    'show_all'      => 'Alle anzeigen',
    'show_enabled'  => 'Aktivierte anzeigen',
    'show_disabled' => 'Deaktivierte anzeigen',
    'all_enabled'   => 'Alle aktiviert',
    'all_disabled'  => 'Alle deaktiviert',

    'help'          => [

        'title'         => 'Hilfe',
        'documentation' => 'Read Manual',
        'setting'       => 'Kann in Einstellungen > Allgemein deaktiviert werden',

    ],

];
