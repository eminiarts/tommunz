<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
     */

    'password' => 'Das Passwort muss mindestens sechs Zeichen lang sein und bestätigt werden.',
    'user'     => "Wir können keinen Benutzer mit dieser E-Mail Adresse finden.",
    'token'    => 'Dieser Passwort-Reset-Token ist ungültig.',
    'sent'     => 'Wir haben Ihnen per E-Mail geschickt, damit Sie Ihr Passwort zurückgesetzen können.',
    'reset'    => 'Ihr Passwort wurde zurückgesetzt!',

];
