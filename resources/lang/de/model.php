<?php

return [

    'id'         => 'Id',
    'name'       => 'Name',
    'slug'       => 'Slug',
    'created_at' => 'Erstellt am',
    'updated_at' => 'Bearbeitet am',
    'status'     => 'Status',
    'namespace'  => 'Namespace',

];
