<?php

return [

    'delete_record' => 'Sie sind dabei, diesen Eintrag zu löschen, möchten Sie weitermachen?',
    'not_logged_in' => 'Sie sind nicht eingeloggt.',
    'loading'       => 'Laden',
    'check_form'    => 'Prüfen Sie das Formular auf Fehler.',

];
