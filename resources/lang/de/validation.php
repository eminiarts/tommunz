<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
     */

    'accepted'             => 'Das :attribute muss akzeptiert werden.',
    'active_url'           => 'Das :attribute ist kein gültiger Link.',
    'after'                => 'Das :attribute muss ein Datum nach :date sein.',
    'alpha'                => 'Das :attribute darf nur Buchstaben enthalten.',
    'alpha_dash'           => 'Das :attribute darf nur Buchstaben, Zahlen und Bindestriche enthalten.',
    'alpha_num'            => 'Das :attribute darf nur Buchstaben und Zahlen enthalen.',
    'array'                => 'Das :attribute darf nur ein Array sein.',
    'before'               => 'Das :attribute muss ein Datum vor :date sein.',
    'between'              => [
        'numeric' => 'Das :attribute muss zwischen :min und :max sein.',
        'file'    => 'Das :attribute muss zwischen :min und :max kilobytes sein.',
        'string'  => 'Das :attribute muss zwischen :min und :max characters sein.',
        'array'   => 'Das :attribute muss zwischen :min und :max Elemente enthalten.',
    ],
    'boolean'              => 'Das :attribute Feld muss wahr oder falsch sein.',
    'confirmed'            => 'Die :attribute Bestätigung stimmt nicht überein.',
    'date'                 => ':attribute ist kein gültiges Datum.',
    'date_format'          => ':attribute stimmt nicht mit dem Format :format überein.',
    'different'            => ':attribute und :other müssen unterschiedlich sein.',
    'digits'               => ':attribute muss diese :digits Ziffern haben.',
    'digits_between'       => ':attribute muss zwischen :min und :max sein.',
    'email'                => ':attribute muss eine gültige E-Mail-Adresse sein.',
    'filled'               => ':attribute ist erforderlich.',
    'exists'               => 'Ausgewählte :attribute ist ungültig.',
    'image'                => ':attribute muss ein Bild sein.',
    'in'                   => 'Ausgewählte :attribute ist ungültig.',
    'integer'              => ':attribute muss eine Zahl sein.',
    'ip'                   => ':attribute muss eine IP-Adresse sein.',
    'max'                  => [
        'numeric' => ':attribute kann nicht grösser sein als :max.',
        'file'    => ':attribute kann nicht grösser sein als :max kilobytes.',
        'string'  => ':attribute kann nicht grösser sein als :max Zeichen.',
        'array'   => ':attribute kann nicht mehr als :max Einträge haben.',
    ],
    'mimes'                => 'Das :attribute muss folgendes Format vorweisen: :values.',
    'min'                  => [
        'numeric' => ':attribute muss mindestens :min sein.',
        'file'    => ':attribute muss mindestens :min Kilobytes sein.',
        'string'  => ':attribute muss mindestens :min Zeichen sein.',
        'array'   => ':attribute muss mindestend :min Einträge haben.',
    ],
    'not_in'               => 'Das ausgewählte :attribute ist ungültig.',
    'numeric'              => ':attribute muss eine Nummer sein.',
    'regex'                => ':attribute - Format ist ungültig.',
    'required'             => ':attribute ist erforderlich.',
    'required_if'          => ':attribute ist erforderlich, wenn :other ist :value.',
    'required_with'        => ':attribute ist erforderlich wenn :values vorhanden ist.',
    'required_with_all'    => ':attribute ist erforderlich wenn :values vorhanden ist.',
    'required_without'     => ':attribute ist erforderlich wenn :values nicht vorhanden ist.',
    'required_without_all' => ':attribute ist erforderlich wenn keine von :values vorhanden sind.',
    'same'                 => ':attribute und :other müssen übereinstimmen.',
    'size'                 => [
        'numeric' => ':attribute muss :size sein.',
        'file'    => ':attribute muss be :size Kilobytes sein.',
        'string'  => ':attribute muss be :size Zeichen sein.',
        'array'   => ':attribute muss :size Einträge enhalten..',
    ],
    'string'               => ':attribute muss ein String sein.',
    'timezone'             => ':attribute muss eine gültige Zone sein.',
    'unique'               => ':attribute wird bereits verwendet.',
    'url'                  => ':attribute - Format ist ungültig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
     */

    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'eigene-nachricht',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
     */

    'attributes'           => [],

];
