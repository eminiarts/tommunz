<?php

return [

    'add'       => 'Hinzufügen',
    'create'    => 'Erstellen',
    'remove'    => 'Entfernen',
    'cancel'    => 'Abbrechen',
    'delete'    => 'Löschen',
    'edit'      => 'Bearbeiten',
    'update'    => 'Änderungen speichern',
    'submit'    => 'Speichern',
    'save'      => 'Speichern',
    'copy'      => 'Kopieren',
    'update'    => 'Aktualisieren',
    'enable'    => 'Aktivieren',
    'disable'   => 'Deaktivieren',
    'install'   => 'Installieren',
    'uninstall' => 'Deinstallieren',
    'open'      => 'Öffnen',
    'close'     => 'Schliessen',
    'collapse'  => 'Öffnen/Schliessen',
    'upload'    => 'Hochladen',
    'export'    => 'Exportieren',

    'bulk'      => [
        'delete'  => 'Ausgewählte löschen',
        'enable'  => 'Ausgewählte aktivieren',
        'disable' => 'Ausgewählte deaktivieren',
    ],

];
