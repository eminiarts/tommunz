<?php namespace Tommunz\Support\Providers;

use Cartalyst\Support\ServiceProvider;

class InvitationServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Tommunz\Support\Models\Invitation']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('tommunz.support.invitation.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('tommunz.support.invitation', 'Tommunz\Support\Repositories\Invitation\InvitationRepository');

		// Register the data handler
		$this->bindIf('tommunz.support.invitation.handler.data', 'Tommunz\Support\Handlers\Invitation\InvitationDataHandler');

		// Register the event handler
		$this->bindIf('tommunz.support.invitation.handler.event', 'Tommunz\Support\Handlers\Invitation\InvitationEventHandler');

		// Register the validator
		$this->bindIf('tommunz.support.invitation.validator', 'Tommunz\Support\Validator\Invitation\InvitationValidator');
	}

}
