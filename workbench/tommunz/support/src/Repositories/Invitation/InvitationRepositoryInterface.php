<?php namespace Tommunz\Support\Repositories\Invitation;

interface InvitationRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Tommunz\Support\Models\Invitation
	 */
	public function grid();

	/**
	 * Returns all the support entries.
	 *
	 * @return \Tommunz\Support\Models\Invitation
	 */
	public function findAll();

	/**
	 * Returns a support entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Tommunz\Support\Models\Invitation
	 */
	public function find($id);

	/**
	 * Determines if the given support is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given support is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given support.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a support entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Tommunz\Support\Models\Invitation
	 */
	public function create(array $data);

	/**
	 * Updates the support entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Tommunz\Support\Models\Invitation
	 */
	public function update($id, array $data);

	/**
	 * Deletes the support entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
