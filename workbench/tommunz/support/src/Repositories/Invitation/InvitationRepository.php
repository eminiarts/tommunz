<?php namespace Tommunz\Support\Repositories\Invitation;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;
use Symfony\Component\Finder\Finder;

class InvitationRepository implements InvitationRepositoryInterface {

	use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

	/**
	 * The Data handler.
	 *
	 * @var \Tommunz\Support\Handlers\Invitation\InvitationDataHandlerInterface
	 */
	protected $data;

	/**
	 * The Eloquent support model.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * Constructor.
	 *
	 * @param  \Illuminate\Container\Container  $app
	 * @return void
	 */
	public function __construct(Container $app)
	{
		$this->setContainer($app);

		$this->setDispatcher($app['events']);

		$this->data = $app['tommunz.support.invitation.handler.data'];

		$this->setValidator($app['tommunz.support.invitation.validator']);

		$this->setModel(get_class($app['Tommunz\Support\Models\Invitation']));
	}

	/**
	 * {@inheritDoc}
	 */
	public function grid()
	{
		return $this
			->createModel();
	}

	/**
	 * {@inheritDoc}
	 */
	public function findAll()
	{
		return $this->container['cache']->rememberForever('tommunz.support.invitation.all', function()
		{
			return $this->createModel()->get();
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function find($id)
	{
		return $this->container['cache']->rememberForever('tommunz.support.invitation.'.$id, function() use ($id)
		{
			return $this->createModel()->find($id);
		});
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForCreation(array $input)
	{
		return $this->validator->on('create')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function validForUpdate($id, array $input)
	{
		return $this->validator->on('update')->validate($input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function store($id, array $input)
	{
		return ! $id ? $this->create($input) : $this->update($id, $input);
	}

	/**
	 * {@inheritDoc}
	 */
	public function create(array $input)
	{
		// Create a new invitation
		$invitation = $this->createModel();

		// Fire the 'tommunz.support.invitation.creating' event
		if ($this->fireEvent('tommunz.support.invitation.creating', [ $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForCreation($data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Save the invitation
			$invitation->fill($data)->save();

			// Fire the 'tommunz.support.invitation.created' event
			$this->fireEvent('tommunz.support.invitation.created', [ $invitation ]);
		}

		return [ $messages, $invitation ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function update($id, array $input)
	{
		// Get the invitation object
		$invitation = $this->find($id);

		// Fire the 'tommunz.support.invitation.updating' event
		if ($this->fireEvent('tommunz.support.invitation.updating', [ $invitation, $input ]) === false)
		{
			return false;
		}

		// Prepare the submitted data
		$data = $this->data->prepare($input);

		// Validate the submitted data
		$messages = $this->validForUpdate($invitation, $data);

		// Check if the validation returned any errors
		if ($messages->isEmpty())
		{
			// Update the invitation
			$invitation->fill($data)->save();

			// Fire the 'tommunz.support.invitation.updated' event
			$this->fireEvent('tommunz.support.invitation.updated', [ $invitation ]);
		}

		return [ $messages, $invitation ];
	}

	/**
	 * {@inheritDoc}
	 */
	public function delete($id)
	{
		// Check if the invitation exists
		if ($invitation = $this->find($id))
		{
			// Fire the 'tommunz.support.invitation.deleting' event
			$this->fireEvent('tommunz.support.invitation.deleting', [ $invitation ]);

			// Delete the invitation entry
			$invitation->delete();

			// Fire the 'tommunz.support.invitation.deleted' event
			$this->fireEvent('tommunz.support.invitation.deleted', [ $invitation ]);

			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public function enable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => true ]);
	}

	/**
	 * {@inheritDoc}
	 */
	public function disable($id)
	{
		$this->validator->bypass();

		return $this->update($id, [ 'enabled' => false ]);
	}

}
