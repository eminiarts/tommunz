<?php namespace Tommunz\Support\Validator\Invitation;

interface InvitationValidatorInterface {

	/**
	 * Updating a invitation scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
