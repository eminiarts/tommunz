<?php
namespace Tommunz\Support\Validator\Invitation;

use Cartalyst\Support\Validator;

class InvitationValidator extends Validator implements InvitationValidatorInterface
{
    /**
     * {@inheritDoc}
     */
    protected $rules = [
        'name'   => 'required',
        'email'  => 'required',
        'people' => 'required',
    ];

    /**
     * {@inheritDoc}
     */
    public function onUpdate() {}
}
