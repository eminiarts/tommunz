<?php namespace Tommunz\Support\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;

class InvitationsController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('tommunz/support::index');
	}

}
