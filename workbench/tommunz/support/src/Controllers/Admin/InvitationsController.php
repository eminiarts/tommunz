<?php namespace Tommunz\Support\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Tommunz\Support\Repositories\Invitation\InvitationRepositoryInterface;

class InvitationsController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Support repository.
	 *
	 * @var \Tommunz\Support\Repositories\Invitation\InvitationRepositoryInterface
	 */
	protected $invitations;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Tommunz\Support\Repositories\Invitation\InvitationRepositoryInterface  $invitations
	 * @return void
	 */
	public function __construct(InvitationRepositoryInterface $invitations)
	{
		parent::__construct();

		$this->invitations = $invitations;
	}

	/**
	 * Display a listing of invitation.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('tommunz/support::invitations.index');
	}

	/**
	 * Datasource for the invitation Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->invitations->grid();

		$columns = [
			'id',
			'name',
			'email',
			'people',
			'created_at',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.tommunz.support.invitations.edit', $element->id);

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new invitation.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new invitation.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating invitation.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating invitation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified invitation.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->invitations->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("tommunz/support::invitations/message.{$type}.delete")
		);

		return redirect()->route('admin.tommunz.support.invitations.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->invitations->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a invitation identifier?
		if (isset($id))
		{
			if ( ! $invitation = $this->invitations->find($id))
			{
				$this->alerts->error(trans('tommunz/support::invitations/message.not_found', compact('id')));

				return redirect()->route('admin.tommunz.support.invitations.all');
			}
		}
		else
		{
			$invitation = $this->invitations->createModel();
		}

		// Show the page
		return view('tommunz/support::invitations.form', compact('mode', 'invitation'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the invitation
		list($messages) = $this->invitations->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("tommunz/support::invitations/message.success.{$mode}"));

			return redirect()->route('admin.tommunz.support.invitations.all');
		}

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}

}
