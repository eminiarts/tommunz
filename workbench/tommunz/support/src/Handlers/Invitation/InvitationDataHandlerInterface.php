<?php namespace Tommunz\Support\Handlers\Invitation;

interface InvitationDataHandlerInterface {

	/**
	 * Prepares the given data for being stored.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function prepare(array $data);

}
