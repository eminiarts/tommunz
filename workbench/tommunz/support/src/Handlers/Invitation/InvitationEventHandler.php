<?php
namespace Tommunz\Support\Handlers\Invitation;

use Mail;
use Illuminate\Events\Dispatcher;
use Tommunz\Support\Models\Invitation;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class InvitationEventHandler extends BaseEventHandler implements InvitationEventHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function created(Invitation $invitation)
    {
        $this->flushCache($invitation);

        // Send Mail to User
        Mail::send('emails.anmeldung', compact('invitation'), function ($message) use ($invitation) {
            $message->to($invitation->email)->subject($invitation->id . ' – trois raisons Anmeldung auf tommunz.com!');
            $message->attach(asset('Tom-Munz-Trois-Raisons.ics'));
        });

        // Send Mail to Tom
        Mail::send('emails.anmeldung', compact('invitation'), function ($message) use ($invitation) {
            $message->to('fest@tommunz.com')->subject($invitation->id . ' – trois raisons Anmeldung auf tommunz.com!');
            $message->attach(asset('Tom-Munz-Trois-Raisons.ics'));
        });
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {}

    /**
     * {@inheritDoc}
     */
    public function deleted(Invitation $invitation)
    {
        $this->flushCache($invitation);
    }

    /**
     * {@inheritDoc}
     */
    public function deleting(Invitation $invitation) {}

    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('tommunz.support.invitation.creating', __CLASS__ . '@creating');
        $dispatcher->listen('tommunz.support.invitation.created', __CLASS__ . '@created');

        $dispatcher->listen('tommunz.support.invitation.updating', __CLASS__ . '@updating');
        $dispatcher->listen('tommunz.support.invitation.updated', __CLASS__ . '@updated');

        $dispatcher->listen('tommunz.support.invitation.deleted', __CLASS__ . '@deleting');
        $dispatcher->listen('tommunz.support.invitation.deleted', __CLASS__ . '@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function updated(Invitation $invitation)
    {
        $this->flushCache($invitation);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(
        Invitation $invitation,
        array      $data
    ) {}

    /**
     * Flush the cache.
     *
     * @param  \Tommunz\Support\Models\Invitation $invitation
     * @return void
     */
    protected function flushCache(Invitation $invitation)
    {
        $this->app['cache']->forget('tommunz.support.invitation.all');

        $this->app['cache']->forget('tommunz.support.invitation.' . $invitation->id);
    }
}
