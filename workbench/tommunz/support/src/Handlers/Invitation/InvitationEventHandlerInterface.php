<?php namespace Tommunz\Support\Handlers\Invitation;

use Tommunz\Support\Models\Invitation;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface InvitationEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a invitation is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a invitation is created.
	 *
	 * @param  \Tommunz\Support\Models\Invitation  $invitation
	 * @return mixed
	 */
	public function created(Invitation $invitation);

	/**
	 * When a invitation is being updated.
	 *
	 * @param  \Tommunz\Support\Models\Invitation  $invitation
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Invitation $invitation, array $data);

	/**
	 * When a invitation is updated.
	 *
	 * @param  \Tommunz\Support\Models\Invitation  $invitation
	 * @return mixed
	 */
	public function updated(Invitation $invitation);

	/**
	 * When a invitation is being deleted.
	 *
	 * @param  \Tommunz\Support\Models\Invitation  $invitation
	 * @return mixed
	 */
	public function deleting(Invitation $invitation);

	/**
	 * When a invitation is deleted.
	 *
	 * @param  \Tommunz\Support\Models\Invitation  $invitation
	 * @return mixed
	 */
	public function deleted(Invitation $invitation);

}
