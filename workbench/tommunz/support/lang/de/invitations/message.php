<?php

return [

    // General messages
    'not_found' => 'Einladung [:id] gibt es nicht.',

    // Success messages
    'success'   => [
        'create' => 'Einladung wurde erfolgreich erstellt.',
        'update' => 'Einladung wurde erfolgreich geändert.',
        'delete' => 'Einladung wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error'     => [
        'create' => 'Es gab einen Fehler beim Erstellen. Bitte versuche es erneut.',
        'update' => 'Es gab einen Fehler beim Ändern. Bitte versuche es erneut.',
        'delete' => 'Es gab einen Fehler beim Löschen. Bitte versuche es erneut.',
    ],

];
