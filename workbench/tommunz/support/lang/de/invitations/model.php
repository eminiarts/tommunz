<?php

return [

    'general' => [

        'id'          => 'Id',
        'name'        => 'Name',
        'email'       => 'E-Mail',
        'people'      => 'Personen',
        'created_at'  => 'Erstellt am',
        'name_help'   => 'Name eingeben',
        'email_help'  => 'E-Mail eingeben',
        'people_help' => 'Personen eingeben',

    ],

];
