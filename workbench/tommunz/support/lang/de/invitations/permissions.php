<?php

return [

	'index'  => 'List Invitations',
	'create' => 'Create new Invitation',
	'edit'   => 'Edit Invitation',
	'delete' => 'Delete Invitation',

];
