<?php

return [

	// General messages
	'not_found' => 'Invitation [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Invitation was successfully created.',
		'update' => 'Invitation was successfully updated.',
		'delete' => 'Invitation was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the invitation. Please try again.',
		'update' => 'There was an issue updating the invitation. Please try again.',
		'delete' => 'There was an issue deleting the invitation. Please try again.',
	],

];
