<?php

return [

	'general' => [

		'id' => 'Id',
		'name' => 'Name',
		'email' => 'Email',
		'people' => 'People',
		'created_at' => 'Created At',
		'name_help' => 'Enter the Name here',
		'email_help' => 'Enter the Email here',
		'people_help' => 'Enter the People here',

	],

];
