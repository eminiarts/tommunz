<?php

return [

	'general' => [

		'id' => 'Id',
		'name' => 'Name',
		'email' => 'Email',
		'created_at' => 'Created At',
		'name_help' => 'Enter the Name here',
		'email_help' => 'Enter the Email here',

	],

];
