<?php

return [

	// General messages
	'not_found' => 'Employee [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Employee was successfully created.',
		'update' => 'Employee was successfully updated.',
		'delete' => 'Employee was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the employee. Please try again.',
		'update' => 'There was an issue updating the employee. Please try again.',
		'delete' => 'There was an issue deleting the employee. Please try again.',
	],

];
