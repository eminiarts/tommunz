<?php

return [

    // General messages
    'not_found' => 'Mitarbeiter [:id] gibt es nicht.',

    // Success messages
    'success'   => [
        'create' => 'Mitarbeiter wurde erfolgreich erstellt.',
        'update' => 'Mitarbeiter wurde erfolgreich geändert.',
        'delete' => 'Mitarbeiter wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error'     => [
        'create' => 'Es gab einen Fehler beim Erstellen. Bitte versuche es erneut.',
        'update' => 'Es gab einen Fehler beim Ändern. Bitte versuche es erneut.',
        'delete' => 'Es gab einen Fehler beim Löschen. Bitte versuche es erneut.',
    ],

];
