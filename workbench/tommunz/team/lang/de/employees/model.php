<?php

return [

    'general' => [

        'id'         => 'Id',
        'name'       => 'Name',
        'email'      => 'E-Mail',
        'created_at' => 'Erstellt am',
        'name_help'  => 'Name eingeben',
        'email_help' => 'E-Mail eingeben',

    ],

];
