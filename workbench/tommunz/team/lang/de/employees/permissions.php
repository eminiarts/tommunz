<?php

return [

	'index'  => 'List Employees',
	'create' => 'Create new Employee',
	'edit'   => 'Edit Employee',
	'delete' => 'Delete Employee',

];
