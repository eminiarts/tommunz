@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{ trans('tommunz/team::employees/common.title') }}
@stop

{{-- Queue assets --}}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/css/daterangepicker-bs3.css', 'style') }}

{{ Asset::queue('moment', 'moment/js/moment.js', 'jquery') }}
{{ Asset::queue('data-grid', 'cartalyst/js/data-grid.js', 'jquery') }}
{{ Asset::queue('underscore', 'underscore/js/underscore.js', 'jquery') }}
{{ Asset::queue('index', 'tommunz/team::employees/js/index.js', 'platform') }}
{{ Asset::queue('bootstrap-daterange', 'bootstrap/js/daterangepicker.js', 'jquery') }}

{{ Asset::queue('sortable', 'tommunz/team::js/jquery.sortable.js', 'jquery') }}

{{-- Inline scripts --}}
@section('scripts')
@parent
<script>
  var group = $("ol.simple_with_drop").sortable({
      group: 'simple_with_drop',
      handle: 'i.fa',
      nested: false,

      onDrop: function($item, container, _super) {
        _super($item, container);
          serialize();
      }
  });

  function serialize() {
      var data = group.sortable("serialize").get();
      var jsonString = JSON.stringify(data, null, ' ');

      console.log(data);
      console.log(jsonString);

      $.post( "/team/employees/sort", {data: jsonString});
  }

</script>
@stop

{{-- Inline styles --}}
@section('styles')
@parent

<style>
  body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.simple_with_drop li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.simple_with_drop li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}

ol.vertical li {
    display: block;
    margin: 5px;
    padding: 5px;
    border: 1px solid #cccccc;
    color: #0088cc;
    background: #eeeeee;
}

body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  top: 0;
  opacity: 0.5;
  z-index: 2000;
}

ol.vertical {
  padding-left: 0;
  margin: 0 0 9px 0;
  min-height: 10px;
}
ol.vertical li {
  display: block;
  margin: 5px;
  padding: 5px;
  border: 1px solid #cccccc;
  color: #333;
  background: #fff;
  border-radius: 4px;
}
ol.vertical li.placeholder {
  position: relative;
  height: 32px;
  border: 2px dashed #337ab7;
  border-radius: 4px;
}
</style>
@stop

{{-- Page content --}}
@section('page')

{{-- Page content --}}
@section('page')

<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Reihenfolge</h3>
  </div>
  <div class="collapse" id="collapseExample">
    <div class="panel-body">
      <ol class="simple_with_drop vertical">


      @foreach(app('tommunz.team.employee')->findAll() as $team)
          <li data-id="{{ $team->id }}">
            <i class="fa fa-arrows"></i>
            {{ $team->name }}
          </li>
      @endforeach

      {{-- var_dump(app('tommunz.team.employee')->find(1)->findChildren(1)) --}}

      </ol>
    </div>
  </div>
</div>

{{-- Grid --}}
<section class="panel panel-default panel-grid">

	{{-- Grid: Header --}}
	<header class="panel-heading">

		<nav class="navbar navbar-default navbar-actions">

			<div class="container-fluid">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>

					<span class="navbar-brand">{{{ trans('tommunz/team::employees/common.title') }}}</span>

				</div>

				{{-- Grid: Actions --}}
				<div class="collapse navbar-collapse" id="actions">

					<ul class="nav navbar-nav navbar-left">

						<li class="disabled">
							<a class="disabled" data-grid-bulk-action="disable" data-toggle="tooltip" data-original-title="{{{ trans('action.bulk.disable') }}}">
								<i class="fa fa-eye-slash"></i> <span class="visible-xs-inline">{{{ trans('action.bulk.disable') }}}</span>
							</a>
						</li>

						<li class="disabled">
							<a data-grid-bulk-action="enable" data-toggle="tooltip" data-original-title="{{{ trans('action.bulk.enable') }}}">
								<i class="fa fa-eye"></i> <span class="visible-xs-inline">{{{ trans('action.bulk.enable') }}}</span>
							</a>
						</li>

						<li class="danger disabled">
							<a data-grid-bulk-action="delete" data-toggle="tooltip" data-target="modal-confirm" data-original-title="{{{ trans('action.bulk.delete') }}}">
								<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.bulk.delete') }}}</span>
							</a>
						</li>

						<li class="dropdown">
							<a href="#" class="dropdown-toggle tip" data-toggle="dropdown" role="button" aria-expanded="false" data-original-title="{{{ trans('action.export') }}}">
								<i class="fa fa-download"></i> <span class="visible-xs-inline">{{{ trans('action.export') }}}</span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a data-download="json"><i class="fa fa-file-code-o"></i> JSON</a></li>
								<li><a data-download="csv"><i class="fa fa-file-excel-o"></i> CSV</a></li>
								<li><a data-download="pdf"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
							</ul>
						</li>

						<li class="primary">
							<a href="{{ route('admin.tommunz.team.employees.create') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.create') }}}">
								<i class="fa fa-plus"></i> <span class="visible-xs-inline">{{{ trans('action.create') }}}</span>
							</a>
						</li>

					</ul>

					{{-- Grid: Filters --}}
					<form class="navbar-form navbar-right" method="post" accept-charset="utf-8" data-search data-grid="employee" role="form">

						<div class="input-group">

							<span class="input-group-btn">

								<button class="btn btn-default" type="button" disabled>
									{{{ trans('common.filters') }}}
								</button>

								<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
									<span class="caret"></span>
									<span class="sr-only">Toggle Dropdown</span>
								</button>

								<ul class="dropdown-menu" role="menu">

									<li>
										<a data-grid="employee" data-filter="enabled:1" data-label="enabled::{{{ trans('common.all_enabled') }}}" data-reset>
											<i class="fa fa-eye"></i> {{{ trans('common.show_enabled') }}}
										</a>
									</li>

									<li>
										<a data-toggle="tooltip" data-placement="top" data-original-title="" data-grid="employee" data-filter="enabled:0" data-label="enabled::{{{ trans('common.all_disabled') }}}" data-reset>
											<i class="fa fa-eye-slash"></i> {{{ trans('common.show_disabled') }}}
										</a>
									</li>

									<li class="divider"></li>

									<li>
										<a data-grid-calendar-preset="day">
											<i class="fa fa-calendar"></i> {{{ trans('date.day') }}}
										</a>
									</li>

									<li>
										<a data-grid-calendar-preset="week">
											<i class="fa fa-calendar"></i> {{{ trans('date.week') }}}
										</a>
									</li>

									<li>
										<a data-grid-calendar-preset="month">
											<i class="fa fa-calendar"></i> {{{ trans('date.month') }}}
										</a>
									</li>

								</ul>

								<button class="btn btn-default hidden-xs" type="button" data-grid-calendar data-range-filter="created_at">
									<i class="fa fa-calendar"></i>
								</button>

							</span>

							<input class="form-control" name="filter" type="text" placeholder="{{{ trans('common.search') }}}">

							<span class="input-group-btn">

								<button class="btn btn-default" type="submit">
									<span class="fa fa-search"></span>
								</button>

								<button class="btn btn-default" data-grid="employee" data-reset>
									<i class="fa fa-refresh fa-sm"></i>
								</button>

							</span>

						</div>

					</form>

				</div>

			</div>

		</nav>

	</header>

	<div class="panel-body">

		{{-- Grid: Applied Filters --}}
		<div class="btn-toolbar" role="toolbar" aria-label="data-grid-applied-filters">

			<div id="data-grid_applied" class="btn-group" data-grid="employee"></div>

		</div>

	</div>

	{{-- Grid: Table --}}
	<div class="table-responsive">

		<table id="data-grid" class="table table-hover" data-source="{{ route('admin.tommunz.team.employees.grid') }}" data-grid="employee">
			<thead>
				<tr>
					<th><input data-grid-checkbox="all" type="checkbox"></th>
					<th class="sortable" data-sort="id">{{{ trans('tommunz/team::employees/model.general.id') }}}</th>
					<th class="sortable" data-sort="name">{{{ trans('tommunz/team::employees/model.general.name') }}}</th>
					<th class="sortable" data-sort="email">{{{ trans('tommunz/team::employees/model.general.email') }}}</th>
					<th class="sortable" data-sort="created_at">{{{ trans('tommunz/team::employees/model.general.created_at') }}}</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>

	</div>

	<footer class="panel-footer clearfix">

		{{-- Grid: Pagination --}}
		<div id="data-grid_pagination" data-grid="employee"></div>

	</footer>

	{{-- Grid: templates --}}
	@include('tommunz/team::employees/grid/index/results')
	@include('tommunz/team::employees/grid/index/pagination')
	@include('tommunz/team::employees/grid/index/filters')
	@include('tommunz/team::employees/grid/index/no_results')

</section>

@if (config('platform.app.help'))
	@include('tommunz/team::employees/help')
@endif

@stop
