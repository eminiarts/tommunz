<?php namespace Tommunz\Team\Controllers\Admin;

use Platform\Access\Controllers\AdminController;
use Tommunz\Team\Repositories\Employee\EmployeeRepositoryInterface;

class EmployeesController extends AdminController {

	/**
	 * {@inheritDoc}
	 */
	protected $csrfWhitelist = [
		'executeAction',
	];

	/**
	 * The Team repository.
	 *
	 * @var \Tommunz\Team\Repositories\Employee\EmployeeRepositoryInterface
	 */
	protected $employees;

	/**
	 * Holds all the mass actions we can execute.
	 *
	 * @var array
	 */
	protected $actions = [
		'delete',
		'enable',
		'disable',
	];

	/**
	 * Constructor.
	 *
	 * @param  \Tommunz\Team\Repositories\Employee\EmployeeRepositoryInterface  $employees
	 * @return void
	 */
	public function __construct(EmployeeRepositoryInterface $employees)
	{
		parent::__construct();

		$this->employees = $employees;
	}

	/**
	 * Display a listing of employee.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('tommunz/team::employees.index');
	}

	/**
	 * Datasource for the employee Data Grid.
	 *
	 * @return \Cartalyst\DataGrid\DataGrid
	 */
	public function grid()
	{
		$data = $this->employees->grid();

		$columns = [
			'id',
			'name',
			'email',
			'created_at',
		];

		$settings = [
			'sort'      => 'created_at',
			'direction' => 'desc',
		];

		$transformer = function($element)
		{
			$element->edit_uri = route('admin.tommunz.team.employees.edit', $element->id);

			return $element;
		};

		return datagrid($data, $columns, $settings, $transformer);
	}

	/**
	 * Show the form for creating new employee.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new employee.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating employee.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified employee.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		$type = $this->employees->delete($id) ? 'success' : 'error';

		$this->alerts->{$type}(
			trans("tommunz/team::employees/message.{$type}.delete")
		);

		return redirect()->route('admin.tommunz.team.employees.all');
	}

	/**
	 * Executes the mass action.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function executeAction()
	{
		$action = request()->input('action');

		if (in_array($action, $this->actions))
		{
			foreach (request()->input('rows', []) as $row)
			{
				$this->employees->{$action}($row);
			}

			return response('Success');
		}

		return response('Failed', 500);
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		// Do we have a employee identifier?
		if (isset($id))
		{
			if ( ! $employee = $this->employees->find($id))
			{
				$this->alerts->error(trans('tommunz/team::employees/message.not_found', compact('id')));

				return redirect()->route('admin.tommunz.team.employees.all');
			}
		}
		else
		{
			$employee = $this->employees->createModel();
		}

		// Show the page
		return view('tommunz/team::employees.form', compact('mode', 'employee'));
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		// Store the employee
		list($messages) = $this->employees->store($id, request()->all());

		// Do we have any errors?
		if ($messages->isEmpty())
		{
			$this->alerts->success(trans("tommunz/team::employees/message.success.{$mode}"));

			return redirect()->route('admin.tommunz.team.employees.all');
		}

		$this->alerts->error($messages, 'form');

		return redirect()->back()->withInput();
	}

}
