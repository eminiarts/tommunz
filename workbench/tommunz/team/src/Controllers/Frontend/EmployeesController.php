<?php
namespace Tommunz\Team\Controllers\Frontend;

use Tommunz\Team\Models\Employee;
use Platform\Foundation\Controllers\Controller;

class EmployeesController extends Controller
{
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('tommunz/team::index');
    }

    public function sort()
    {
        app('cache')->forget('tommunz.team.employee.all');
        // This gets an Array of IDs... ['2', '4', '3'] for example
        $mediaIds = json_decode(request()->get('data'), true);

        $mediaIds = collect($mediaIds)->flatten();

        $preparedMediaIds = [];
        foreach ($mediaIds as $key => $id) {
            $preparedMediaIds[$id] = ['sort' => $key];
        }

        foreach ($preparedMediaIds as $key => $value) {
            Employee::find($key)->update($value);
        }

        return response([
            'success' => true,
        ]);

    }
}
