<?php namespace Tommunz\Team\Validator\Employee;

use Cartalyst\Support\Validator;

class EmployeeValidator extends Validator implements EmployeeValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [

	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
