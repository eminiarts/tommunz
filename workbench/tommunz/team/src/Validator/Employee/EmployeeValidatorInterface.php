<?php namespace Tommunz\Team\Validator\Employee;

interface EmployeeValidatorInterface {

	/**
	 * Updating a employee scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
