<?php namespace Tommunz\Team\Repositories\Employee;

interface EmployeeRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Tommunz\Team\Models\Employee
	 */
	public function grid();

	/**
	 * Returns all the team entries.
	 *
	 * @return \Tommunz\Team\Models\Employee
	 */
	public function findAll();

	/**
	 * Returns a team entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Tommunz\Team\Models\Employee
	 */
	public function find($id);

	/**
	 * Determines if the given team is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given team is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given team.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a team entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Tommunz\Team\Models\Employee
	 */
	public function create(array $data);

	/**
	 * Updates the team entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Tommunz\Team\Models\Employee
	 */
	public function update($id, array $data);

	/**
	 * Deletes the team entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
