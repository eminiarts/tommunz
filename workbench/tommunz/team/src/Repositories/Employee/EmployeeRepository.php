<?php
namespace Tommunz\Team\Repositories\Employee;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class EmployeeRepository implements EmployeeRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Tommunz\Team\Handlers\Employee\EmployeeDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent team model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['tommunz.team.employee.handler.data'];

        $this->setValidator($app['tommunz.team.employee.validator']);

        $this->setModel(get_class($app['Tommunz\Team\Models\Employee']));
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input)
    {
        // Create a new employee
        $employee = $this->createModel();

        // Fire the 'tommunz.team.employee.creating' event
        if ($this->fireEvent('tommunz.team.employee.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the employee
            $employee->fill($data)->save();

            // Fire the 'tommunz.team.employee.created' event
            $this->fireEvent('tommunz.team.employee.created', [$employee]);
        }

        return [$messages, $employee];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        // Check if the employee exists
        if ($employee = $this->find($id)) {
            // Fire the 'tommunz.team.employee.deleting' event
            $this->fireEvent('tommunz.team.employee.deleting', [$employee]);

            // Delete the employee entry
            $employee->delete();

            // Fire the 'tommunz.team.employee.deleted' event
            $this->fireEvent('tommunz.team.employee.deleted', [$employee]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function disable($id)
    {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => false]);
    }

    /**
     * {@inheritDoc}
     */
    public function enable($id)
    {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => true]);
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('tommunz.team.employee.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('tommunz.team.employee.all', function () {
            return $this->createModel()->orderBy('sort', 'asc')->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this
            ->createModel();
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input)
    {
        // Get the employee object
        $employee = $this->find($id);

        // Fire the 'tommunz.team.employee.updating' event
        if ($this->fireEvent('tommunz.team.employee.updating', [$employee, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($employee, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the employee
            $employee->fill($data)->save();

            // Fire the 'tommunz.team.employee.updated' event
            $this->fireEvent('tommunz.team.employee.updated', [$employee]);
        }

        return [$messages, $employee];
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $input)
    {
        return $this->validator->on('update')->validate($input);
    }
}
