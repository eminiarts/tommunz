<?php namespace Tommunz\Team\Providers;

use Cartalyst\Support\ServiceProvider;

class EmployeeServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Register the attributes namespace
		$this->app['platform.attributes.manager']->registerNamespace(
			$this->app['Tommunz\Team\Models\Employee']
		);

		// Subscribe the registered event handler
		$this->app['events']->subscribe('tommunz.team.employee.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('tommunz.team.employee', 'Tommunz\Team\Repositories\Employee\EmployeeRepository');

		// Register the data handler
		$this->bindIf('tommunz.team.employee.handler.data', 'Tommunz\Team\Handlers\Employee\EmployeeDataHandler');

		// Register the event handler
		$this->bindIf('tommunz.team.employee.handler.event', 'Tommunz\Team\Handlers\Employee\EmployeeEventHandler');

		// Register the validator
		$this->bindIf('tommunz.team.employee.validator', 'Tommunz\Team\Validator\Employee\EmployeeValidator');
	}

}
