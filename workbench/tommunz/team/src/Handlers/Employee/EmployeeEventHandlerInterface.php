<?php namespace Tommunz\Team\Handlers\Employee;

use Tommunz\Team\Models\Employee;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface EmployeeEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a employee is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a employee is created.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @return mixed
	 */
	public function created(Employee $employee);

	/**
	 * When a employee is being updated.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Employee $employee, array $data);

	/**
	 * When a employee is updated.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @return mixed
	 */
	public function updated(Employee $employee);

	/**
	 * When a employee is being deleted.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @return mixed
	 */
	public function deleting(Employee $employee);

	/**
	 * When a employee is deleted.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @return mixed
	 */
	public function deleted(Employee $employee);

}
