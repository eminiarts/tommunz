<?php namespace Tommunz\Team\Handlers\Employee;

use Illuminate\Events\Dispatcher;
use Tommunz\Team\Models\Employee;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class EmployeeEventHandler extends BaseEventHandler implements EmployeeEventHandlerInterface {

	/**
	 * {@inheritDoc}
	 */
	public function subscribe(Dispatcher $dispatcher)
	{
		$dispatcher->listen('tommunz.team.employee.creating', __CLASS__.'@creating');
		$dispatcher->listen('tommunz.team.employee.created', __CLASS__.'@created');

		$dispatcher->listen('tommunz.team.employee.updating', __CLASS__.'@updating');
		$dispatcher->listen('tommunz.team.employee.updated', __CLASS__.'@updated');

		$dispatcher->listen('tommunz.team.employee.deleted', __CLASS__.'@deleting');
		$dispatcher->listen('tommunz.team.employee.deleted', __CLASS__.'@deleted');
	}

	/**
	 * {@inheritDoc}
	 */
	public function creating(array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function created(Employee $employee)
	{
		$this->flushCache($employee);
	}

	/**
	 * {@inheritDoc}
	 */
	public function updating(Employee $employee, array $data)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function updated(Employee $employee)
	{
		$this->flushCache($employee);
	}

	/**
	 * {@inheritDoc}
	 */
	public function deleting(Employee $employee)
	{

	}

	/**
	 * {@inheritDoc}
	 */
	public function deleted(Employee $employee)
	{
		$this->flushCache($employee);
	}

	/**
	 * Flush the cache.
	 *
	 * @param  \Tommunz\Team\Models\Employee  $employee
	 * @return void
	 */
	protected function flushCache(Employee $employee)
	{
		$this->app['cache']->forget('tommunz.team.employee.all');

		$this->app['cache']->forget('tommunz.team.employee.'.$employee->id);
	}

}
