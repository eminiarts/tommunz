<?php

return [

	'general' => [

		'id' => 'Id',
		'name' => 'Name',
		'slug' => 'Slug',
		'number' => 'Number',
		'phase' => 'Phase',
		'description' => 'Description',
		'details' => 'Details',
		'enabled' => 'Enabled',
		'from' => 'From',
		'until' => 'Until',
		'created_at' => 'Created At',
		'name_help' => 'Enter the Name here',
		'slug_help' => 'Enter the Slug here',
		'number_help' => 'Enter the Number here',
		'phase_help' => 'Enter the Phase here',
		'description_help' => 'Enter the Description here',
		'details_help' => 'Enter the Details here',
		'enabled_help' => 'Enter the Enabled here',
		'from_help' => 'Enter the From here',
		'until_help' => 'Enter the Until here',

	],

];
