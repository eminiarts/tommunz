<?php

return [

	// General messages
	'not_found' => 'Project [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Project was successfully created.',
		'update' => 'Project was successfully updated.',
		'delete' => 'Project was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the project. Please try again.',
		'update' => 'There was an issue updating the project. Please try again.',
		'delete' => 'There was an issue deleting the project. Please try again.',
	],

];
