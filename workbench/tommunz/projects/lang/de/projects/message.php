<?php

return [

    // General messages
    'not_found' => 'Projekt [:id] gibt es nicht.',

    // Success messages
    'success'   => [
        'create' => 'Projekt wurde erfolgreich erstellt.',
        'update' => 'Projekt wurde erfolgreich geändert.',
        'delete' => 'Projekt wurde erfolgreich gelöscht.',
    ],

    // Error messages
    'error'     => [
        'create' => 'Es gab einen Fehler beim Erstellen. Bitte versuche es erneut.',
        'update' => 'Es gab einen Fehler beim Ändern. Bitte versuche es erneut.',
        'delete' => 'Es gab einen Fehler beim Löschen. Bitte versuche es erneut.',
    ],

];
