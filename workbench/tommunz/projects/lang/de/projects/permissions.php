<?php

return [

	'index'  => 'List Projects',
	'create' => 'Create new Project',
	'edit'   => 'Edit Project',
	'delete' => 'Delete Project',

];
