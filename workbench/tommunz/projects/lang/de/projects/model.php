<?php

return [

    'general' => [

        'id'               => 'Id',
        'name'             => 'Name',
        'slug'             => 'Slug',
        'number'           => 'Nummer',
        'phase'            => 'Phase',
        'description'      => 'Beschreibung',
        'details'          => 'Details',
        'enabled'          => 'Aktiv',
        'from'             => 'Von',
        'until'            => 'Bis',
        'created_at'       => 'Erstellt am',
        'name_help'        => 'Name eingeben',
        'slug_help'        => 'Slug eingeben',
        'number_help'      => 'Nummer eingeben',
        'phase_help'       => 'Phase eingeben',
        'description_help' => 'Beschreibung eingeben',
        'details_help'     => 'Details eingeben',
        'enabled_help'     => 'Aktiv eingeben',
        'from_help'        => 'Von eingeben',
        'until_help'       => 'Bis eingeben',

    ],

];
