<?php namespace Tommunz\Projects\Handlers\Project;

use Tommunz\Projects\Models\Project;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface ProjectEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a project is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a project is created.
	 *
	 * @param  \Tommunz\Projects\Models\Project  $project
	 * @return mixed
	 */
	public function created(Project $project);

	/**
	 * When a project is being updated.
	 *
	 * @param  \Tommunz\Projects\Models\Project  $project
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Project $project, array $data);

	/**
	 * When a project is updated.
	 *
	 * @param  \Tommunz\Projects\Models\Project  $project
	 * @return mixed
	 */
	public function updated(Project $project);

	/**
	 * When a project is being deleted.
	 *
	 * @param  \Tommunz\Projects\Models\Project  $project
	 * @return mixed
	 */
	public function deleting(Project $project);

	/**
	 * When a project is deleted.
	 *
	 * @param  \Tommunz\Projects\Models\Project  $project
	 * @return mixed
	 */
	public function deleted(Project $project);

}
