<?php
namespace Tommunz\Projects\Handlers\Project;

use Illuminate\Events\Dispatcher;
use Tommunz\Projects\Models\Project;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class ProjectEventHandler extends BaseEventHandler implements ProjectEventHandlerInterface
{
    /**
     * {@inheritDoc}
     */
    public function created(Project $project)
    {
        $this->flushCache($project);
    }

    /**
     * {@inheritDoc}
     */
    public function creating(array $data)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function deleted(Project $project)
    {
        $this->flushCache($project);
    }

    /**
     * {@inheritDoc}
     */
    public function deleting(Project $project)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher)
    {
        $dispatcher->listen('tommunz.projects.project.creating', __CLASS__ . '@creating');
        $dispatcher->listen('tommunz.projects.project.created', __CLASS__ . '@created');

        $dispatcher->listen('tommunz.projects.project.updating', __CLASS__ . '@updating');
        $dispatcher->listen('tommunz.projects.project.updated', __CLASS__ . '@updated');

        $dispatcher->listen('tommunz.projects.project.deleted', __CLASS__ . '@deleting');
        $dispatcher->listen('tommunz.projects.project.deleted', __CLASS__ . '@deleted');
    }

    /**
     * {@inheritDoc}
     */
    public function updated(Project $project)
    {
        $this->flushCache($project);
    }

    /**
     * {@inheritDoc}
     */
    public function updating(Project $project, array $data)
    {
    }

    /**
     * Flush the cache.
     *
     * @param  \Tommunz\Projects\Models\Project  $project
     * @return void
     */
    protected function flushCache(Project $project)
    {
        $this->app['cache']->forget('tommunz.projects.project.all');

        $this->app['cache']->forget('tommunz.projects.project.homepage');

        $this->app['cache']->forget('tommunz.projects.project.' . $project->id);
    }
}
