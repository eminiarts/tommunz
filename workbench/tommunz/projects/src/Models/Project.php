<?php
namespace Tommunz\Projects\Models;

use Platform\Media\Support\MediaTrait;
use Illuminate\Database\Eloquent\Model;
use Cartalyst\Attributes\EntityInterface;
use Platform\Attributes\Traits\EntityTrait;
use Cartalyst\Support\Traits\NamespacedEntityTrait;
use Cartalyst\Support\Contracts\NamespacedEntityInterface;

class Project extends Model implements EntityInterface, NamespacedEntityInterface
{
    use EntityTrait, NamespacedEntityTrait, MediaTrait;

    /**
     * @var array
     */
    protected $appends = [
        'images',
    ];

    /**
     * {@inheritDoc}
     */
    protected static $entityNamespace = 'tommunz/projects.project';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'number',
        'phase',
        'description',
        'details',
        'enabled',
        'from',
        'until',
        'sort',
    ];

    /**
     * {@inheritDoc}
     */
    protected $guarded = [
        'id',
    ];

    /**
     * {@inheritDoc}
     */
    protected $table = 'projects';

    /**
     * {@inheritDoc}
     */
    protected $with = [
        //'values.attribute',
        'media',
    ];

    /**
     * Images Accessor.
     *
     * @param  string   $value
     * @return string
     */
    public function getImagesAttribute($value)
    {
        // $images = [];

        // foreach ($this->media as $image) {
        //     $images['images'][] = Widget::make('platform/media::media.path', [$image->id, 'optimized']);
        //     $images['thumbs'][] = Widget::make('platform/media::media.path', [$image->id, 'thumb']);
        //     $images['medium'][] = Widget::make('platform/media::media.path', [$image->id, 'medium']);
        // }

        // if (count($images)) {
        //     return $images;
        // }

        // return [
        //     'images' => [asset('img/product.jpg')],
        //     'thumbs' => [asset('img/product-thumbnail.jpg')],
        //     'medium' => [asset('img/product.jpg')],
        // ];
    }
}
