<?php namespace Tommunz\Projects\Widgets;

class Project {

	public function show()
	{
    $projects = app('tommunz.projects.project')->homepage();

		return view('tommunz/projects::dashboard', compact('projects'));
	}

}
