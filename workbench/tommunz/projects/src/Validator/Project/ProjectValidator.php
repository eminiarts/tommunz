<?php namespace Tommunz\Projects\Validator\Project;

use Cartalyst\Support\Validator;

class ProjectValidator extends Validator implements ProjectValidatorInterface {

	/**
	 * {@inheritDoc}
	 */
	protected $rules = [

	];

	/**
	 * {@inheritDoc}
	 */
	public function onUpdate()
	{

	}

}
