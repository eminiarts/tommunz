<?php namespace Tommunz\Projects\Validator\Project;

interface ProjectValidatorInterface {

	/**
	 * Updating a project scenario.
	 *
	 * @return void
	 */
	public function onUpdate();

}
