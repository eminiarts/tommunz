<?php
namespace Tommunz\Projects\Providers;

use Cartalyst\Support\ServiceProvider;

class ProjectServiceProvider extends ServiceProvider
{
    /**
     * {@inheritDoc}
     */
    public function boot()
    {
        // Register the attributes namespace
        $this->app['platform.attributes.manager']->registerNamespace(
            $this->app['Tommunz\Projects\Models\Project']
        );

        // Subscribe the registered event handler
        $this->app['events']->subscribe('tommunz.projects.project.handler.event');

        // $tommunz = [
        //     'projects' => app('tommunz.projects.project')->homepage(),
        //     'about'    => [
        //         'address'   => app('platform.content')->findBySlug('adresse'),
        //         'content'   => app('platform.content')->findBySlug('content'),
        //         'impressum' => app('platform.content')->findBySlug('impressum'),
        //         'werkliste' => Widget::make('platform/media::media.path', [241]),
        //     ],
        //     'team'     => app('tommunz.team.employee')->findAll(),
        // ];

        // //dd($tommunz);

        // View::share('tommunz', $tommunz);
    }

    /**
     * {@inheritDoc}
     */
    public function register()
    {
        // Register the repository
        $this->bindIf('tommunz.projects.project', 'Tommunz\Projects\Repositories\Project\ProjectRepository');

        // Register the data handler
        $this->bindIf('tommunz.projects.project.handler.data', 'Tommunz\Projects\Handlers\Project\ProjectDataHandler');

        // Register the event handler
        $this->bindIf('tommunz.projects.project.handler.event', 'Tommunz\Projects\Handlers\Project\ProjectEventHandler');

        // Register the validator
        $this->bindIf('tommunz.projects.project.validator', 'Tommunz\Projects\Validator\Project\ProjectValidator');
    }
}
