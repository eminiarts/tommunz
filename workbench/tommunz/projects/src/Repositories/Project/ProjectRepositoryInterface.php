<?php namespace Tommunz\Projects\Repositories\Project;

interface ProjectRepositoryInterface {

	/**
	 * Returns a dataset compatible with data grid.
	 *
	 * @return \Tommunz\Projects\Models\Project
	 */
	public function grid();

	/**
	 * Returns all the projects entries.
	 *
	 * @return \Tommunz\Projects\Models\Project
	 */
	public function findAll();

	/**
	 * Returns a projects entry by its primary key.
	 *
	 * @param  int  $id
	 * @return \Tommunz\Projects\Models\Project
	 */
	public function find($id);

	/**
	 * Determines if the given projects is valid for creation.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForCreation(array $data);

	/**
	 * Determines if the given projects is valid for update.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Illuminate\Support\MessageBag
	 */
	public function validForUpdate($id, array $data);

	/**
	 * Creates or updates the given projects.
	 *
	 * @param  int  $id
	 * @param  array  $input
	 * @return bool|array
	 */
	public function store($id, array $input);

	/**
	 * Creates a projects entry with the given data.
	 *
	 * @param  array  $data
	 * @return \Tommunz\Projects\Models\Project
	 */
	public function create(array $data);

	/**
	 * Updates the projects entry with the given data.
	 *
	 * @param  int  $id
	 * @param  array  $data
	 * @return \Tommunz\Projects\Models\Project
	 */
	public function update($id, array $data);

	/**
	 * Deletes the projects entry.
	 *
	 * @param  int  $id
	 * @return bool
	 */
	public function delete($id);

}
