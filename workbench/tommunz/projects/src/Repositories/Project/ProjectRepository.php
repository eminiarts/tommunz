<?php
namespace Tommunz\Projects\Repositories\Project;

use Cartalyst\Support\Traits;
use Illuminate\Container\Container;

class ProjectRepository implements ProjectRepositoryInterface
{
    use Traits\ContainerTrait, Traits\EventTrait, Traits\RepositoryTrait, Traits\ValidatorTrait;

    /**
     * The Data handler.
     *
     * @var \Tommunz\Projects\Handlers\Project\ProjectDataHandlerInterface
     */
    protected $data;

    /**
     * The Eloquent projects model.
     *
     * @var string
     */
    protected $model;

    /**
     * Constructor.
     *
     * @param  \Illuminate\Container\Container  $app
     * @return void
     */
    public function __construct(Container $app)
    {
        $this->setContainer($app);

        $this->setDispatcher($app['events']);

        $this->data = $app['tommunz.projects.project.handler.data'];

        $this->setValidator($app['tommunz.projects.project.validator']);

        $this->setModel(get_class($app['Tommunz\Projects\Models\Project']));
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input)
    {
        // Create a new project
        $project = $this->createModel();

        // Fire the 'tommunz.projects.project.creating' event
        if ($this->fireEvent('tommunz.projects.project.creating', [$input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForCreation($data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Save the project
            $project->fill($data)->save();

            // Fire the 'tommunz.projects.project.created' event
            $this->fireEvent('tommunz.projects.project.created', [$project]);
        }

        return [$messages, $project];
    }

    /**
     * {@inheritDoc}
     */
    public function delete($id)
    {
        // Check if the project exists
        if ($project = $this->find($id)) {
            // Fire the 'tommunz.projects.project.deleting' event
            $this->fireEvent('tommunz.projects.project.deleting', [$project]);

            // Delete the project entry
            $project->delete();

            // Fire the 'tommunz.projects.project.deleted' event
            $this->fireEvent('tommunz.projects.project.deleted', [$project]);

            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function disable($id)
    {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => false]);
    }

    /**
     * {@inheritDoc}
     */
    public function enable($id)
    {
        $this->validator->bypass();

        return $this->update($id, ['enabled' => true]);
    }

    /**
     * {@inheritDoc}
     */
    public function find($id)
    {
        return $this->container['cache']->rememberForever('tommunz.projects.project.' . $id, function () use ($id) {
            return $this->createModel()->find($id);
        });
    }

    /**
     * {@inheritDoc}
     */
    public function findAll()
    {
        return $this->container['cache']->rememberForever('tommunz.projects.project.all', function () {
            return $this->createModel()->get();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function grid()
    {
        return $this
            ->createModel();
    }

    /**
     * @return mixed
     */
    public function homepage()
    {
        return $this->container['cache']->rememberForever('tommunz.projects.project.homepage', function () {
            return $this->createModel()->orderBy('sort', 'asc')->whereEnabled(1)->take(16)->get()->each(function ($item, $key) {
                $item->meta_description = strip_tags($item['description']);
            });
        });
    }

    /**
     * {@inheritDoc}
     */
    public function store($id, array $input)
    {
        return !$id ? $this->create($input) : $this->update($id, $input);
    }

    /**
     * {@inheritDoc}
     */
    public function update($id, array $input)
    {
        // Get the project object
        $project = $this->find($id);

        // Fire the 'tommunz.projects.project.updating' event
        if ($this->fireEvent('tommunz.projects.project.updating', [$project, $input]) === false) {
            return false;
        }

        // Prepare the submitted data
        $data = $this->data->prepare($input);

        // Validate the submitted data
        $messages = $this->validForUpdate($project, $data);

        // Check if the validation returned any errors
        if ($messages->isEmpty()) {
            // Update the project
            $project->fill($data)->save();

            // Fire the 'tommunz.projects.project.updated' event
            $this->fireEvent('tommunz.projects.project.updated', [$project]);
        }

        return [$messages, $project];
    }

    /**
     * {@inheritDoc}
     */
    public function validForCreation(array $input)
    {
        return $this->validator->on('create')->validate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function validForUpdate($id, array $input)
    {
        return $this->validator->on('update')->validate($input);
    }
}
