<?php namespace Tommunz\Projects\Controllers\Frontend;

use Platform\Foundation\Controllers\Controller;
use Tommunz\Projects\Models\Project;
use Cache;

class ProjectsController extends Controller {

	/**
	 * Return the main view.
	 *
	 * @return \Illuminate\View\View
	 */
	public function sort()
	{
    $sort = Request()->get('projectsArray');

    $preparedMediaIds = [];
    foreach ($sort as $key => $id) {
        $preparedMediaIds[$id] = ['sort' => $key];
    }

    foreach ($preparedMediaIds as $key => $value) {
      $projects[] = Project::find($key)->update($value);
    }

    Cache::forget('tommunz.projects.project.homepage');

    return $preparedMediaIds;
	}

}
