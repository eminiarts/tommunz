<?php
namespace Tommunz\Projects\Controllers\Admin;

use Widget;
use Platform\Access\Controllers\AdminController;
use Tommunz\Projects\Repositories\Project\ProjectRepositoryInterface;

class ProjectsController extends AdminController
{
    /**
     * Holds all the mass actions we can execute.
     *
     * @var array
     */
    protected $actions = [
        'delete',
        'enable',
        'disable',
    ];

    /**
     * {@inheritDoc}
     */
    protected $csrfWhitelist = [
        'executeAction',
    ];

    /**
     * The Projects repository.
     *
     * @var \Tommunz\Projects\Repositories\Project\ProjectRepositoryInterface
     */
    protected $projects;

    /**
     * Constructor.
     *
     * @param  \Tommunz\Projects\Repositories\Project\ProjectRepositoryInterface  $projects
     * @return void
     */
    public function __construct(ProjectRepositoryInterface $projects)
    {
        parent::__construct();

        $this->projects = $projects;
    }

    /**
     * Show the form for creating new project.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return $this->showForm('create');
    }

    /**
     * Remove the specified project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $type = $this->projects->delete($id) ? 'success' : 'error';

        $this->alerts->{$type}(
            trans("tommunz/projects::projects/message.{$type}.delete")
        );

        return redirect()->route('admin.tommunz.projects.projects.all');
    }

    /**
     * Show the form for updating project.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id)
    {
        return $this->showForm('update', $id);
    }

    /**
     * Executes the mass action.
     *
     * @return \Illuminate\Http\Response
     */
    public function executeAction()
    {
        $action = request()->input('action');

        if (in_array($action, $this->actions)) {
            foreach (request()->input('rows', []) as $row) {
                $this->projects->{$action}($row);
            }

            return response('Success');
        }

        return response('Failed', 500);
    }

    /**
     * Datasource for the project Data Grid.
     *
     * @return \Cartalyst\DataGrid\DataGrid
     */
    public function grid()
    {
        $data = $this->projects->grid();

        $columns = [
            'id',
            'name',
            'slug',
            'number',
            'phase',
            'description',
            'details',
            'enabled',
            'from',
            'until',
            'created_at',
        ];

        $settings = [
            'sort'      => 'created_at',
            'direction' => 'desc',
        ];

        $transformer = function ($element) {
            $element->edit_uri = route('admin.tommunz.projects.projects.edit', $element->id);

            $element->thumb = count($element->media) ? Widget::make('platform/media::media.path', [$element->media->first()->id, 'thumb']) : null;

            return $element;
        };

        return datagrid($data, $columns, $settings, $transformer);
    }

    /**
     * Display a listing of project.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('tommunz/projects::projects.index');
    }

    /**
     * Handle posting of the form for creating new project.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        return $this->processForm('create');
    }

    /**
     * Handle posting of the form for updating project.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        return $this->processForm('update', $id);
    }

    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null)
    {
        // Store the project
        list($messages) = $this->projects->store($id, request()->all());

        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("tommunz/projects::projects/message.success.{$mode}"));

            return redirect()->route('admin.tommunz.projects.projects.all');
        }

        $this->alerts->error($messages, 'form');

        return redirect()->back()->withInput();
    }

    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null)
    {
        // Do we have a project identifier?
        if (isset($id)) {
            if (!$project = $this->projects->find($id)) {
                $this->alerts->error(trans('tommunz/projects::projects/message.not_found', compact('id')));

                return redirect()->route('admin.tommunz.projects.projects.all');
            }
        } else {
            $project = $this->projects->createModel();
        }

        // Show the page

        return view('tommunz/projects::projects.form', compact('mode', 'project'));
    }
}
