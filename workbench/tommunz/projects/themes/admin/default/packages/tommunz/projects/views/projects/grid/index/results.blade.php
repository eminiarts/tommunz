<script type="text/template" data-grid="project" data-template="results">

	<% _.each(results, function(r) { %>

	<tr data-grid-row>
		<td><input content="id" input data-grid-checkbox="" name="entries[]" type="checkbox" value="<%= r.id %>"></td>
		<td>
			<a href="<%= r.edit_uri %>">
				<img src="<%= r.thumb %>" style="height: 80px;">
			</a>
		</td>
		<td><%= r.name %></td>
		<td><%= r.slug %></td>
		<td><%= r.number %></td>
		<td><%= r.phase %></td>
		<td><%= r.enabled %></td>
	</tr>

	<% }); %>

</script>
