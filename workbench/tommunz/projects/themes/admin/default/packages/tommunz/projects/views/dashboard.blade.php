
<style>
  #simpleList{
    background: #000;
    padding: 50px 0;
  }
  .square{
    width: 80%;
    margin: 0 auto 20px;
    content: '';
    transition: .3s all ease;
    cursor: move;

    font-size: 48px;
    color: white;
    text-align: center;
    line-height: 100px;
    font-weight: bold;

    box-shadow: 0px 2px 3px rgba(0,0,0,.0);
  }

  .square img{
    width: 100%;
  }

  .sortable-ghost .square{
    opacity: .3;
  }
  .sortable-chosen:not(.sortable-ghost) .square{
    box-shadow: 0px 6px 15px rgba(0,0,0,.5);
  }
  .black-bg{
    background: #000;
    color: #fff;
    margin-top: -16px;
    height: calc(100vh - 55px);
  }

  .page{
    min-height: 100vh;
  }
  .page__content{
    min-height: calc(100vh - 71px);
  }
</style>


<div class="row black-bg">
  <div class="col-xs-12">
    <div class="row" style="margin-bottom: 50px;">
      <div class="col-xs-12">
        <h3>Verschiebe die Portfolio-Einträge un die Reihenfolge zu ändern.</h3>
      </div>
    </div>

    <div class="row" id="simpleList">

      @foreach ($projects as $project)
      <div class="col-sm-3" id="project_{{$project->id}}" data-id="{{$project->id}}">
        <div class="square">
          <img src="{{ $project->media[0]['preset_paths']['medium'] }}">
        </div>
      </div>
      @endforeach

    </div>
  </div>
</div>
