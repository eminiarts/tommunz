@extends('layouts/default')

{{-- Page title --}}
@section('title')
@parent
{{{ trans("action.{$mode}") }}} {{ trans('tommunz/projects::projects/common.title') }}
@stop


{{ Asset::queue('redactor', 'redactor/css/redactor.css', 'styles') }}

{{-- Queue assets --}}
{{ Asset::queue('validate', 'platform/js/validate.js', 'jquery') }}
{{ Asset::queue('slugify', 'platform/js/slugify.js', 'jquery') }}
{{ Asset::queue('redactor', 'redactor/js/redactor.min.js', 'jquery') }}


{{-- Inline scripts --}}
@section('scripts')
@parent
@stop

{{-- Inline styles --}}
@section('styles')
@parent
<style>
  .list-group-item:first-child .list-group-item-center span:after{
    content: ' (Titelbild)';
  }
</style>
@stop

{{-- Page content --}}
@section('page')

<section class="panel panel-default panel-tabs">

	{{-- Form --}}
	<form id="projects-form" action="{{ request()->fullUrl() }}" role="form" method="post" data-parsley-validate>

		{{-- Form: CSRF Token --}}
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<header class="panel-heading">

			<nav class="navbar navbar-default navbar-actions">

				<div class="container-fluid">

					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#actions">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						<a class="btn btn-navbar-cancel navbar-btn pull-left tip" href="{{ route('admin.tommunz.projects.projects.all') }}" data-toggle="tooltip" data-original-title="{{{ trans('action.cancel') }}}">
							<i class="fa fa-reply"></i> <span class="visible-xs-inline">{{{ trans('action.cancel') }}}</span>
						</a>

						<span class="navbar-brand">{{{ trans("action.{$mode}") }}} <small>{{{ $project->exists ? $project->id : null }}}</small></span>
					</div>

					{{-- Form: Actions --}}
					<div class="collapse navbar-collapse" id="actions">

						<ul class="nav navbar-nav navbar-right">

							@if ($project->exists)
							<li>
								<a href="{{ route('admin.tommunz.projects.projects.delete', $project->id) }}" class="tip" data-action-delete data-toggle="tooltip" data-original-title="{{{ trans('action.delete') }}}" type="delete">
									<i class="fa fa-trash-o"></i> <span class="visible-xs-inline">{{{ trans('action.delete') }}}</span>
								</a>
							</li>
							@endif

							<li>
								<button class="btn btn-primary navbar-btn" data-toggle="tooltip" data-original-title="{{{ trans('action.save') }}}">
									<i class="fa fa-save"></i> <span class="visible-xs-inline">{{{ trans('action.save') }}}</span>
								</button>
							</li>

						</ul>

					</div>

				</div>

			</nav>

		</header>

		<div class="panel-body">

			<div role="tabpanel">

				{{-- Form: Tabs --}}
				<ul class="nav nav-tabs" role="tablist">
					<li class="active" role="presentation"><a href="#general-tab" aria-controls="general-tab" role="tab" data-toggle="tab">{{{ trans('tommunz/projects::projects/common.tabs.general') }}}</a></li>
          <li></li>
					<li role="presentation"><a href="#mediaTab" aria-controls="attributes" role="tab" data-toggle="tab">Bilder</a></li>
          <li role="presentation"><a href="#attributes" aria-controls="attributes" role="tab" data-toggle="tab">{{{ trans('tommunz/projects::projects/common.tabs.attributes') }}}</a></li>
				</ul>

				<div class="tab-content">

					{{-- Tab: General --}}
					<div role="tabpanel" class="tab-pane fade in active" id="general-tab">

						<fieldset>

							<div class="row">

								<div class="col-sm-6">
                  <div class="form-group{{ Alert::onForm('name', ' has-error') }}">

                    <label for="name" class="control-label">
                      <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.name_help') }}}"></i>
                      {{{ trans('tommunz/projects::projects/model.general.name') }}}
                    </label>

                    <input type="text" data-slugify="#slug" class="form-control" name="name" id="name" placeholder="{{{ trans('tommunz/projects::projects/model.general.name') }}}" value="{{{ input()->old('name', $project->name) }}}">

                    <span class="help-block">{{{ Alert::onForm('name') }}}</span>

                  </div>
                </div>

  							<div class="col-sm-6">
                  <div class="form-group{{ Alert::onForm('slug', ' has-error') }}">

                    <label for="slug" class="control-label">
                      <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.slug_help') }}}"></i>
                      {{{ trans('tommunz/projects::projects/model.general.slug') }}}
                    </label>

                    <input type="text" class="form-control" name="slug" id="slug" placeholder="{{{ trans('tommunz/projects::projects/model.general.slug') }}}" value="{{{ input()->old('slug', $project->slug) }}}">

                    <span class="help-block">{{{ Alert::onForm('slug') }}}</span>

                  </div>
                </div>

  							<div class="col-sm-6">
                  <div class="form-group{{ Alert::onForm('number', ' has-error') }}">

                    <label for="number" class="control-label">
                      <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.number_help') }}}"></i>
                      {{{ trans('tommunz/projects::projects/model.general.number') }}}
                    </label>

                    <input type="text" class="form-control" name="number" id="number" placeholder="{{{ trans('tommunz/projects::projects/model.general.number') }}}" value="{{{ input()->old('number', $project->number) }}}">

                    <span class="help-block">{{{ Alert::onForm('number') }}}</span>

                  </div>
                </div>

  							<div class="col-sm-6">
                  <div class="form-group{{ Alert::onForm('phase', ' has-error') }}">

                    <label for="phase" class="control-label">
                      <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.phase_help') }}}"></i>
                      {{{ trans('tommunz/projects::projects/model.general.phase') }}}
                    </label>

                    <input type="text" class="form-control" name="phase" id="phase" placeholder="{{{ trans('tommunz/projects::projects/model.general.phase') }}}" value="{{{ input()->old('phase', $project->phase) }}}">

                    <span class="help-block">{{{ Alert::onForm('phase') }}}</span>

                  </div>
                </div>

  							<div class="col-sm-12">
                  <div class="form-group{{ Alert::onForm('description', ' has-error') }}">

                    <label for="description" class="control-label">
                      <i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.description_help') }}}"></i>
                      {{{ trans('tommunz/projects::projects/model.general.description') }}}
                    </label>

                    <textarea class="redactor form-control" name="description" id="description" placeholder="{{{ trans('tommunz/projects::projects/model.general.description') }}}">{{{ input()->old('description', $project->description) }}}</textarea>

                    <span class="help-block">{{{ Alert::onForm('description') }}}</span>

                  </div>
                </div>

                <div class="col-sm-12">
  								<div class="form-group{{ Alert::onForm('details', ' has-error') }}">

  									<label for="details" class="control-label">
  										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.details_help') }}}"></i>
  										{{{ trans('tommunz/projects::projects/model.general.details') }}}
  									</label>

  									<textarea class="redactor form-control" name="details" id="details" placeholder="{{{ trans('tommunz/projects::projects/model.general.details') }}}">{{{ input()->old('details', $project->details) }}}</textarea>

  									<span class="help-block">{{{ Alert::onForm('details') }}}</span>

  								</div>
                </div>

                <div class="col-sm-12">
  								<div class="form-group{{ Alert::onForm('enabled', ' has-error') }}">

  									<label for="enabled" class="control-label">
  										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.enabled_help') }}}"></i>
  										{{{ trans('tommunz/projects::projects/model.general.enabled') }}}
  									</label>

  									<div class="checkbox">
  										<label>
  											<input type="hidden" name="enabled" id="enabled" value="0" checked>
  											<input type="checkbox" name="enabled" id="enabled" @if($project->enabled) checked @endif value="1"> {{ ucfirst('enabled') }}
  										</label>
  									</div>

  									<span class="help-block">{{{ Alert::onForm('enabled') }}}</span>

  								</div>
                </div>
                {{--
                <div class="col-sm-12">
  								<div class="form-group{{ Alert::onForm('from', ' has-error') }}">

  									<label for="from" class="control-label">
  										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.from_help') }}}"></i>
  										{{{ trans('tommunz/projects::projects/model.general.from') }}}
  									</label>

  									<input type="text" class="form-control" name="from" id="from" placeholder="{{{ trans('tommunz/projects::projects/model.general.from') }}}" value="{{{ input()->old('from', $project->from) }}}">

  									<span class="help-block">{{{ Alert::onForm('from') }}}</span>

  								</div>

  								<div class="form-group{{ Alert::onForm('until', ' has-error') }}">

  									<label for="until" class="control-label">
  										<i class="fa fa-info-circle" data-toggle="popover" data-content="{{{ trans('tommunz/projects::projects/model.general.until_help') }}}"></i>
  										{{{ trans('tommunz/projects::projects/model.general.until') }}}
  									</label>

  									<input type="text" class="form-control" name="until" id="until" placeholder="{{{ trans('tommunz/projects::projects/model.general.until') }}}" value="{{{ input()->old('until', $project->until) }}}">

  									<span class="help-block">{{{ Alert::onForm('until') }}}</span>

  								</div>

                </div>
                --}}


							</div>

						</fieldset>

					</div>

          {{-- Tab: Media --}}
					<div role="tabpanel" class="tab-pane fade" id="mediaTab">
            @mediaUpload($project)
          </div>

          {{-- Tab: Attributes --}}
					<div role="tabpanel" class="tab-pane fade" id="attributes">
						@attributes($project)
					</div>

				</div>

			</div>

		</div>

	</form>

</section>
@stop
