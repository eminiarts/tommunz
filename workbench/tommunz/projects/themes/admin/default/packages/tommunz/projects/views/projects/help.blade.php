<section class="panel panel-default panel-help">

	<header class="panel-heading">

		<h4>
			<i class="fa fa-life-ring" data-toggle="popover" data-content="{{{ trans('common.help.setting') }}}"></i> Anleitung

			<a class="panel-close small pull-right collapsed tip" data-original-title="{{{ trans('action.collapse') }}}" data-toggle="collapse" href="#help-body" aria-expanded="false" aria-controls="help-body"></a>

		</h4>

	</header>

	<div class="panel-body collapse" id="help-body">

		<div class="row">

			<div class="col-md-10 col-md-offset-1 help">

				@content('tommunz-projects-projects-help', 'tommunz/projects::projects/content/help.md')



				<!-- 16:9 aspect ratio -->
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/qyT84onANfg" allowfullscreen></iframe>
				</div>

			</div>

		</div>

	</div>

</section>
