<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('slug')->nullable()->unique();
			$table->string('number')->nullable();
			$table->string('phase')->nullable();
			$table->text('description')->nullable();
			$table->text('details')->nullable();
			$table->boolean('enabled')->nullable();
			$table->timestamp('from')->nullable();
			$table->timestamp('until')->nullable();
      $table->integer('sort')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
